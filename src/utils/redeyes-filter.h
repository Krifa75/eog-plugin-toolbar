/* EogToolbar - Plugin.

   Copyright (C) 2019 Yahiaoui Fakhri

   This file is part of the plugin for Eog.

   EogToolbar is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License as published by the Free
   Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   EogToolbar is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
   for more details.

   You should have received a copy of the GNU General Public License
   along with EogImagesManager; see the file COPYING.  If not, write to the Free
   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   This file implements a toolbar for eog.  */

#ifndef __YEUX_ROUGES_H__
#define __YEUX_ROUGES_H__

#include <gtk/gtk.h>

typedef struct _CoordRedEyes{
    gint center_x, center_y;
    gint radius;
}CoordRedEyes;

int ImageRedEyes2(GdkPixbuf* pix, int rayon, int x, int y);
int image_red_eyes(GdkPixbuf *pix, GSList *l);

#endif // __YEUX_ROUGES_H__