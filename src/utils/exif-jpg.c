/**************************************************************/
/**************************************************************/
/**  titi@debian:~$ gcc -o test2 *.c -lexif -I.. -lIL -lILU  **/
/**************************************************************/
/**************************************************************/
#include "exif-jpg.h"
#include <glib/gi18n.h>

static char FORMATDATE[PATH_MAX];

typedef enum {
    EXIF_JPG_MARKER_SOF0        = 0xc0,
    EXIF_JPG_MARKER_SOF1        = 0xc1,
    EXIF_JPG_MARKER_SOF2        = 0xc2,
    EXIF_JPG_MARKER_SOF3        = 0xc3,
    EXIF_JPG_MARKER_DHT	    = 0xc4,
    EXIF_JPG_MARKER_SOF5        = 0xc5,
    EXIF_JPG_MARKER_SOF6        = 0xc6,
    EXIF_JPG_MARKER_SOF7        = 0xc7,
    EXIF_JPG_MARKER_JPG	    = 0xc8,
    EXIF_JPG_MARKER_SOF9        = 0xc9,
    EXIF_JPG_MARKER_SOF10       = 0xca,
    EXIF_JPG_MARKER_SOF11       = 0xcb,
    EXIF_JPG_MARKER_DAC	    = 0xcc,
    EXIF_JPG_MARKER_SOF13       = 0xcd,
    EXIF_JPG_MARKER_SOF14       = 0xce,
    EXIF_JPG_MARKER_SOF15       = 0xcf,
    EXIF_JPG_MARKER_RST0	= 0xd0,
    EXIF_JPG_MARKER_RST1	= 0xd1,
    EXIF_JPG_MARKER_RST2	= 0xd2,
    EXIF_JPG_MARKER_RST3	= 0xd3,
    EXIF_JPG_MARKER_RST4	= 0xd4,
    EXIF_JPG_MARKER_RST5	= 0xd5,
    EXIF_JPG_MARKER_RST6	= 0xd6,
    EXIF_JPG_MARKER_RST7	= 0xd7,
    EXIF_JPG_MARKER_SOI         = 0xd8,
    EXIF_JPG_MARKER_EOI         = 0xd9,
    EXIF_JPG_MARKER_SOS         = 0xda,
    EXIF_JPG_MARKER_DQT		= 0xdb,
    EXIF_JPG_MARKER_DNL		= 0xdc,
    EXIF_JPG_MARKER_DRI		= 0xdd,
    EXIF_JPG_MARKER_DHP		= 0xde,
    EXIF_JPG_MARKER_EXP		= 0xdf,
    EXIF_JPG_MARKER_APP0	= 0xe0,
    EXIF_JPG_MARKER_APP1        = 0xe1,
    EXIF_JPG_MARKER_APP2        = 0xe2,
    EXIF_JPG_MARKER_APP3        = 0xe3,
    EXIF_JPG_MARKER_APP4        = 0xe4,
    EXIF_JPG_MARKER_APP5        = 0xe5,
    EXIF_JPG_MARKER_APP6        = 0xe6,
    EXIF_JPG_MARKER_APP7        = 0xe7,
    EXIF_JPG_MARKER_APP8        = 0xe8,
    EXIF_JPG_MARKER_APP9        = 0xe9,
    EXIF_JPG_MARKER_APP10       = 0xea,
    EXIF_JPG_MARKER_APP11       = 0xeb,
    EXIF_JPG_MARKER_APP12       = 0xec,
    EXIF_JPG_MARKER_APP13       = 0xed,
    EXIF_JPG_MARKER_APP14       = 0xee,
    EXIF_JPG_MARKER_APP15       = 0xef,
    EXIF_JPG_MARKER_JPG0	= 0xf0,
    EXIF_JPG_MARKER_JPG1	= 0xf1,
    EXIF_JPG_MARKER_JPG2	= 0xf2,
    EXIF_JPG_MARKER_JPG3	= 0xf3,
    EXIF_JPG_MARKER_JPG4	= 0xf4,
    EXIF_JPG_MARKER_JPG5	= 0xf5,
    EXIF_JPG_MARKER_JPG6	= 0xf6,
    EXIF_JPG_MARKER_JPG7	= 0xf7,
    EXIF_JPG_MARKER_JPG8	= 0xf8,
    EXIF_JPG_MARKER_JPG9	= 0xf9,
    EXIF_JPG_MARKER_JPG10	= 0xfa,
    EXIF_JPG_MARKER_JPG11	= 0xfb,
    EXIF_JPG_MARKER_JPG12	= 0xfc,
    EXIF_JPG_MARKER_JPG13	= 0xfd,
    EXIF_JPG_MARKER_COM         = 0xfe
} MarkerJPG;

#define EXIF_JPG_IS_MARKER(m) (((m) >= EXIF_JPG_MARKER_SOF0) &&		\
			   ((m) <= EXIF_JPG_MARKER_COM))

typedef ExifData * JPEGContentAPP1;

typedef struct _ContentGenericJPG ContentGenericJPG;
struct _ContentGenericJPG
{
    unsigned char *data;
    unsigned int size;
};

typedef union _ContentJPG ContentJPG;
union _ContentJPG
{
    ContentGenericJPG generic;
    JPEGContentAPP1    app1;
};

typedef struct _SectionJPG SectionJPG;
struct _SectionJPG
{
    MarkerJPG marker;
    ContentJPG content;
};

typedef struct _DataJPG        DataJPG;
typedef struct _DataPrivateJPG DataPrivateJPG;

struct _DataPrivateJPG
{
    unsigned int ref_count;
};

struct _DataJPG
{
    SectionJPG *sections;
    unsigned int count;

    unsigned char *data;
    unsigned int size;

    DataPrivateJPG *priv;
};

char*
getFormat(void)
{
    if(*FORMATDATE == 0)
    {
        snprintf(FORMATDATE, sizeof (FORMATDATE), "%s %%e %%b %%Y %s %%Hh%%M", FE_LE, FE_A);
    }
    return FORMATDATE;
}

char*
localize_date(char* date, char* format)
{

    if(format && date)
    {
        struct tm tm;
        char MASKDATE[PATH_MAX];
        char* date_tmp = strdup(date);
        char d = (char)0;

        char* dtmp = date_tmp;
        while (*dtmp != 0)
        {
            if (*dtmp != ' ' && !isdigit (*dtmp))
                d = *dtmp;
            dtmp++;
        }

        if (!d)
            return NULL;
        snprintf (MASKDATE, sizeof (MASKDATE), "%%d %c %%d %c %%d %%d %c %%d %c %%d", d, d, d, d);
        if (sscanf (date_tmp, MASKDATE, &tm.tm_year, &tm.tm_mon, &tm.tm_mday, &tm.tm_hour, &tm.tm_min, &tm.tm_sec) != 6)
        {
            return NULL;
        }

        tm.tm_mon -= 1;

        tm.tm_year -= 1900;

        if (tm.tm_mon < 0 || tm.tm_mon > 11)
            tm.tm_mon %= 12;

        if (tm.tm_mday < 1 || tm.tm_mday > 31)
            tm.tm_mday %= 31;

        if (tm.tm_hour < 0 || tm.tm_hour > 23)
            tm.tm_hour %= 24;

        if (tm.tm_min < 0 || tm.tm_min > 59)
            tm.tm_min %= 60;

        if (tm.tm_sec < 0 || tm.tm_sec > 59)
            tm.tm_sec %= 60;

        int lg = strftime(NULL, SSIZE_MAX, format, &tm);
        if(lg > 0)
        {
            char* buffer = (char*)calloc(1, (lg + 1));
            strftime(buffer, lg+1, format, &tm);
            free (date_tmp);
            return buffer;
        }
        return date_tmp;
    }
    return NULL;
}

static char*
create_date(const char* nomfichier)
{

    struct stat etat;
    struct tm tm;
    char buffer[PATH_MAX];

    stat(nomfichier, &etat);

    stat(nomfichier, &etat);
    localtime_r(&(etat.st_mtime), &tm);
    snprintf(buffer,sizeof (buffer), "%04i:%02i:%02i %02i:%02i:%02i",
             (tm.tm_year + 1900),
             tm.tm_mon + 1,
             tm.tm_mday,
             tm.tm_hour,
             tm.tm_min,
             tm.tm_sec);
    return strdup (buffer);
}

// Rotation                   : 0 90 180 270 (1->6->3->8) 
// Rotation par transposition : 0 90 180 270 (2->7->4->5)
Eog_Orient
rotation_90_CW_get(Eog_Orient orient)
{
    switch (orient)
    {
        case EOG_ORIENT_NONE : return EOG_ROTATE_90_CW;
        case EOG_ROTATE_90_CW : return EOG_ROTATE_180_CW;
        case EOG_ROTATE_180_CW : return EOG_ROTATE_90_CCW;
        case EOG_ROTATE_90_CCW : return EOG_ORIENT_NONE;
        case EOG_FLIP_HORIZONTAL : return EOG_FLIP_TRANSVERSE;
        case EOG_FLIP_TRANSVERSE : return EOG_FLIP_VERTICAL;
        case EOG_FLIP_VERTICAL : return EOG_FLIP_TRANSPOSE;
        case EOG_FLIP_TRANSPOSE : return EOG_FLIP_HORIZONTAL;
        default : return EOG_ORIENT_NONE;
    }
}

int
get_rotation_exif (const char *file)
{
    ExifData  *exif = NULL;
    ExifEntry *entry = NULL;
    ExifByteOrder bo;
    int o = 1;

    exif = exif_data_new_from_file(file);

    if (exif)
    {
        entry = exif_data_get_entry(exif, EXIF_TAG_ORIENTATION);
        if (entry)
        {
            bo = exif_data_get_byte_order(exif);
            o = exif_get_short(entry->data, bo);
        }
        exif_data_free(exif);
    }
    return o;
}

Eog_Orient
rotation_exif_set (const char *file)
{
    ExifData  *exif = NULL;
    ExifEntry *entry = NULL;
    ExifByteOrder bo;
    int o = 0;

    exif = exif_data_new_from_file(file);

    if (exif)
    {
        entry = exif_data_get_entry(exif, EXIF_TAG_ORIENTATION);
        if (entry)
        {
            bo = exif_data_get_byte_order(exif);
            o = exif_get_short(entry->data, bo);
        }
        exif_data_free(exif);

        return o;
    }
    return EOG_ORIENT_NONE;
}

static ExifEntry *initialize_tag(ExifData *exif, ExifTag tag)
{
    ExifEntry *entry = NULL;

    if (tag != EXIF_TAG_ORIENTATION) {
        entry = exif_content_get_entry(exif->ifd[EXIF_IFD_EXIF], EXIF_TAG_DATE_TIME_ORIGINAL);
        if (entry)
            return entry;

        entry = exif_content_get_entry(exif->ifd[EXIF_IFD_EXIF], EXIF_TAG_DATE_TIME_DIGITIZED);
        if (entry)
            return entry;
    }

    if (!((entry = exif_content_get_entry (exif->ifd[EXIF_IFD_0], tag)))) {
        entry = exif_entry_new ();
        assert(entry != NULL);
        entry->tag = tag;

        exif_content_add_entry (exif->ifd[EXIF_IFD_0], entry);
        exif_entry_initialize (entry, tag);
        exif_entry_unref(entry);
    }
    return entry;
}

static void
data_append_section (DataJPG *data)
{

    SectionJPG *s;

    if (!data->count)
        s = (SectionJPG*)calloc (1, sizeof (SectionJPG));
    else
        s = (SectionJPG*)realloc (data->sections,
                                  sizeof (SectionJPG) * (data->count + 1));
    if (!s)
    {

        return;
    }

    data->sections = s;
    data->count++;

}

static DataJPG *
data_new (void)
{
    DataJPG *data;

    data = (DataJPG*)calloc (1, sizeof (DataJPG));
    if (!data)
        return (NULL);
    memset (data, 0, sizeof (DataJPG));
    data->priv = (DataPrivateJPG*)calloc (1, sizeof (DataPrivateJPG));
    if (!data->priv) {
        free (data);
        return (NULL);
    }
    data->priv->ref_count = 1;

    return (data);
}

static void
data_free (DataJPG *data)
{
    if (data)
    {
        if (data->priv)
            free (data->priv);
        free (data);
        data = NULL;
    }
}

static void
data_load_data (DataJPG *data, const unsigned char *d,
                unsigned int size)
{

    unsigned int i, o, len;
    SectionJPG *s;
    MarkerJPG marker;

    if (!data)
        return;
    if (!d)
        return;

    for (o = 0; o < size;) {
        /*
         * JPEG sections start with 0xff. The first byte that is
         * not 0xff is a marker (hopefully).
         */
        for (i = 0; i < 7; i++)
            if (d[o + i] != 0xff)
                break;
        if (!EXIF_JPG_IS_MARKER (d[o + i]))
        {

            return;
        }
        marker = (MarkerJPG)d[o + i];

        /* Append this section */
        data_append_section (data);
        s = &data->sections[data->count - 1];
        s->marker = marker;
        s->content.generic.data = NULL;
        o += i + 1;

        switch (s->marker) {
            case EXIF_JPG_MARKER_SOI:
            case EXIF_JPG_MARKER_EOI:
                break;
            default:

                /* Read the length of the section */
                len = ((d[o] << 8) | d[o + 1]) - 2;
                if (len > size) { o = size; break; }
                o += 2;
                if (o + len > size) { o = size; break; }

                switch (s->marker) {
                    case EXIF_JPG_MARKER_APP1:
                        s->content.app1 = exif_data_new_from_data (d + o - 4, len + 4);
                        break;
                    default:
                        s->content.generic.size = len;
                        s->content.generic.data = (unsigned char*) calloc (1, sizeof (unsigned char) * len);
                        memcpy (s->content.generic.data, &d[o], len);

                        /* In case of SOS, image data will follow. */
                        if (s->marker == EXIF_JPG_MARKER_SOS) {
                            data->size = size - 2 - o - len;
                            data->data = (unsigned char*) calloc (1, sizeof (unsigned char) * data->size);
                            memcpy (data->data, d + o + len, data->size);
                            o += data->size;
                        }
                        break;
                }
                o += len;
                break;
        }
    }

}

static void
data_load_file (DataJPG *data, const char *path)
{

    FILE *f;
    unsigned char *d;
    unsigned int size;

    if (!data)
        return;
    if (!path)
        return;

    f = fopen (path, "rb");
    if (!f)
        return;

    fseek (f, 0, SEEK_END);
    size = ftell (f);
    fseek (f, 0, SEEK_SET);
    d = (unsigned char*) calloc (1, sizeof (unsigned char) * size);
    if (!d) {
        fclose (f);
        return;
    }
    if (fread (d, 1, size, f) != size) {
        free (d);
        fclose (f);

        return;
    }
    fclose (f);

    data_load_data (data, d, size);
    free (d);

}

static DataJPG *
data_new_from_file (const char *path)
{

    DataJPG *data;

    data = data_new ();
    data_load_file (data, path);

    return data;
}


static SectionJPG *
data_get_section (DataJPG *data, MarkerJPG marker)
{

    unsigned int i;

    if (!data)
        return (NULL);

    for (i = 0; i < data->count; i++)
        if (data->sections[i].marker == marker)
            return (&data->sections[i]);

    return (NULL);
}

static void
data_set_exif_data (DataJPG *data, ExifData *exif_data)
{

    SectionJPG *section;

    section = data_get_section (data, EXIF_JPG_MARKER_APP1);
    if (!section) {
        data_append_section (data);
        memmove (&data->sections[2], &data->sections[1],
                 sizeof (SectionJPG) * (data->count - 2));
        section = &data->sections[1];
    } else {
        exif_data_unref (section->content.app1);
    }
    section->marker = EXIF_JPG_MARKER_APP1;
    section->content.app1 = exif_data;
    exif_data_ref (exif_data);

}

static void
data_save_data (DataJPG *data, unsigned char **d, unsigned int *ds)
{

    unsigned int i, eds = 0;
    SectionJPG s;
    unsigned char *ed = NULL;

    if (!data)
        return;
    if (!d)
        return;
    if (!ds)
        return;

    for (*ds = i = 0; i < data->count; i++) {
        s = data->sections[i];

        /* Write the marker */
        *d = (unsigned char *)realloc (*d, sizeof (unsigned char) * (*ds + 2));
        (*d)[*ds + 0] = 0xff;
        (*d)[*ds + 1] = s.marker;
        *ds += 2;

        switch (s.marker) {
            case EXIF_JPG_MARKER_SOI:
            case EXIF_JPG_MARKER_EOI:
                break;
            case EXIF_JPG_MARKER_APP1:
                exif_data_save_data (s.content.app1, &ed, &eds);
                if (!ed) break;
                *d = (unsigned char *)realloc (*d, sizeof (unsigned char) * (*ds + 2));
                (*d)[*ds + 0] = (eds + 2) >> 8;
                (*d)[*ds + 1] = (eds + 2) >> 0;
                *ds += 2;
                *d = (unsigned char *)realloc (*d, sizeof (unsigned char) * (*ds + eds));
                memcpy (*d + *ds, ed, eds);
                *ds += eds;
                free (ed);
                break;
            default:
                *d = (unsigned char *)realloc (*d, sizeof (unsigned char) *
                                                   (*ds + s.content.generic.size + 2));
                (*d)[*ds + 0] = (s.content.generic.size + 2) >> 8;
                (*d)[*ds + 1] = (s.content.generic.size + 2) >> 0;
                *ds += 2;
                memcpy (*d + *ds, s.content.generic.data,
                        s.content.generic.size);
                *ds += s.content.generic.size;

                /* In case of SOS, we need to write the data. */
                if (s.marker == EXIF_JPG_MARKER_SOS) {
                    *d = (unsigned char *)realloc (*d, *ds + data->size);
                    memcpy (*d + *ds, data->data, data->size);
                    *ds += data->size;
                }
                break;
        }
    }

}

static int
data_save_file (DataJPG *data, const char *path)
{

    FILE *f;
    unsigned char *d = NULL;
    unsigned int size = 0, written;

    data_save_data (data, &d, &size);
    if (!d)
        return 0;
    remove (path);
    f = fopen (path, "wb");
    if (!f) {
        free (d);
        return 0;
    }
    written = fwrite (d, 1, size, f);
    fclose (f);
    free (d);
    if (written == size)
        return 1;
    remove(path);
    return 0;
}

static char*
show_entry (ExifEntry *entry)
{

    char* val = (char*)calloc(1, TAG_VALUE_BUF);
    exif_entry_get_value (entry, val, TAG_VALUE_BUF);

    return val;

}

static void
internal_error (void)
{

    fprintf (stderr, "Internal error. Please "
                     "contact <libexif-devel@"
                     "lists.sourceforge.net>.");
    fputc ('\n', stderr);

    exit (1);
}

static void
convert_arg_to_entry (const char *set_value, ExifEntry *e, ExifByteOrder o)
{

    unsigned int i;
    char *value_p = NULL;

    /*
 * ASCII strings are handled separately,
 * since they don't require any conversion.
 */
    if (e->format == EXIF_FORMAT_ASCII) {
        if (e->data) free (e->data);
        e->components = strlen (set_value) + 1;
        e->size = sizeof (char) * e->components;
        /*
        e->data = (char*)malloc (e->size);
                if (!e->data) {
                        fprintf (stderr, "Not enough memory.");
                        fputc ('\n', stderr);
                        exit (1);
                }
        sprintf(e->data, "%s", set_value);
        */
        e->data = (unsigned char*)strdup(set_value);

        return;
    }

    value_p = (char*) set_value;
    for (i = 0; i < e->components; i++) {
        char *begin, *end;
        char *buf, s;
        char comp_separ = ' ';

        begin = value_p;
        value_p = index (begin, comp_separ);
        if (!value_p) {
            if (i != e->components - 1) {
                fprintf (stderr, "Too few components specified!");
                fputc ('\n', stderr);
                exit (1);
            }
            end = begin + strlen (begin);
        } else end = value_p++;

        buf = (char *)calloc (1, (end - begin + 1) * sizeof (char));
        strncpy (buf, begin, end - begin);
        buf[end - begin] = '\0';

        s = exif_format_get_size (e->format);
        switch (e->format) {
            case EXIF_FORMAT_ASCII:
                internal_error (); /* Previously handled */
                break;
            case EXIF_FORMAT_SHORT:
                exif_set_short (e->data + (s * i), o, atoi (buf));
                break;
            case EXIF_FORMAT_LONG:
                exif_set_long (e->data + (s * i), o, atol (buf));
                break;
            case EXIF_FORMAT_SLONG:
                exif_set_slong (e->data + (s * i), o, atol (buf));
                break;
            case EXIF_FORMAT_RATIONAL:
            case EXIF_FORMAT_SRATIONAL:
            case EXIF_FORMAT_BYTE:
            default:
                fprintf (stderr, "Not yet implemented!");
                fputc ('\n', stderr);
                exit (1);
        }

        free (buf);
    }

}

void
add_exif_to_jpg_rotate(const char* src_file, const char* dst_file, short rotation, int is_jpg)
{
    ExifData* ed = NULL;
    ExifEntry *e = NULL;
    const char *date = NULL;
    DataJPG * jd = NULL;
    ed = exif_data_new_from_file(src_file);
    if (is_jpg)
        jd = data_new_from_file (dst_file);

    if (ed && jd)
    {
        e = initialize_tag(ed, EXIF_TAG_DATE_TIME);
        if (e)
        {
            char* l_date = NULL;
            date = (char*)show_entry (e);
            if (date)
            {
                l_date = localize_date ((char *)date, getFormat ());
            }
            if (!l_date)
            {
                const char *cc_date = NULL;
                date = create_date(src_file);
                cc_date = strdup (date);
                convert_arg_to_entry (cc_date, e, exif_data_get_byte_order (ed));
            }
        }
        if (rotation)
        {
            e = initialize_tag(ed, EXIF_TAG_ORIENTATION);
            if (e)
            {
                exif_set_short (e->data, exif_data_get_byte_order (ed), (ExifShort)rotation);
            }
        }
    }
    else
    {
        if (!jd)
        {

            jd = data_new_from_file (dst_file);
        }


        if (!jd)
            return;


        date = create_date(src_file);
        ed = exif_data_new();

        if (!ed)
        {
            fprintf(stderr, "Out of memory\n");
            return;
        }
        exif_data_set_option(ed, EXIF_DATA_OPTION_FOLLOW_SPECIFICATION);
        exif_data_set_data_type(ed, EXIF_DATA_TYPE_COMPRESSED);
        exif_data_set_byte_order(ed, EXIF_BYTE_ORDER_INTEL);
        exif_data_fix(ed);
        if (rotation)
        {
            e = initialize_tag(ed, EXIF_TAG_ORIENTATION);
            exif_set_short (e->data, exif_data_get_byte_order (ed), (ExifShort)rotation);
        }
        e = initialize_tag(ed, EXIF_TAG_DATE_TIME);
        const char *cc_date = strdup (date);
        convert_arg_to_entry (cc_date, e, exif_data_get_byte_order (ed));
    }
    data_set_exif_data (jd, ed);
    unlink (dst_file);
    data_save_file(jd, dst_file);
    data_free (jd);
}

void
add_exif_to_jpg(const char* src_file, const char* dst_file)
{
    ExifData* ed = NULL;
    ExifEntry *e = NULL;
    const char *date = NULL;
    DataJPG * jd = NULL;
    return;
    ed = exif_data_new_from_file(src_file);
    jd = data_new_from_file (dst_file);
    if (ed && jd)
    {
        e = initialize_tag(ed, EXIF_TAG_DATE_TIME);
        if (e)
        {
            char* l_date = NULL;
            date = (char*)show_entry (e);
            if (date)
            {
                l_date = localize_date ((char *)date, getFormat ());
            }
            if (!l_date)
            {
                const char *cc_date = NULL;
                date = create_date(src_file);
                cc_date = strdup (date);
                convert_arg_to_entry (cc_date, e, exif_data_get_byte_order (ed));
                //memcpy(e->data, cc_date, sizeof(cc_date)-1);
            }
        }
        e = initialize_tag(ed, EXIF_TAG_ORIENTATION);
        if(e)
        {
            exif_set_short (e->data, exif_data_get_byte_order (ed), (ExifShort)1);
        }
    }
    else if (jd)
    {
        date = create_date(src_file);
        ed = exif_data_new();
        if (!ed)
        {
            fprintf(stderr, "Out of memory\n");
            return;
        }

        exif_data_set_option(ed, EXIF_DATA_OPTION_FOLLOW_SPECIFICATION);
        exif_data_set_data_type(ed, EXIF_DATA_TYPE_COMPRESSED);
        exif_data_set_byte_order(ed, EXIF_BYTE_ORDER_INTEL);

        exif_data_fix(ed);

        e = initialize_tag(ed, EXIF_TAG_DATE_TIME);
        if(e)
        {
            const char *cc_date = strdup (date);
            convert_arg_to_entry (cc_date, e, exif_data_get_byte_order (ed));
        }

        e = initialize_tag(ed, EXIF_TAG_ORIENTATION);
        if(e)
        {
            exif_set_short (e->data, exif_data_get_byte_order (ed), (ExifShort)1);
        }
    }
    data_set_exif_data (jd, ed);
    unlink (dst_file);
    data_save_file(jd, dst_file);
    data_free (jd);
}

/*
int main(void){
  //  void add_exif_to_jpg(const char* src_file, const char* dst_file);
  //  add_exif_to_jpg("./po1.png", "./po1.jpg");
  //void add_exif_to_jpg_rotate(const char* src_file, const char* dst_file, short rotation, int is_jpg);
  add_exif_to_jpg_rotate("./po1.png", "./po2.jpg", 3, 1);
  return 0;
}*/

