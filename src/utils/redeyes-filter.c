/* EogToolbar - Plugin.

   Copyright (C) 2019 Yahiaoui Fakhri

   This file is part of the plugin for Eog.

   EogToolbar is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License as published by the Free
   Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   EogToolbar is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
   for more details.

   You should have received a copy of the GNU General Public License
   along with EogImagesManager; see the file COPYING.  If not, write to the Free
   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   This file implements a toolbar for eog.  */

#include "redeyes-filter.h"
#include <stdlib.h>
////:::::::: YEUX ROUGE ::::::::::////

#define REGION_SEARCH_SIZE 1
static const double RED_FACTOR = 0.5133333;
static const double GREEN_FACTOR  = 1.0;
static const double BLUE_FACTOR =  0.1933333;

int circle_point (char *isred, int x, int y, int col, int row, int width, int height)
{
    int minx = (-x + col) > 0 ? (-x + col) : 0;
    int maxx = (x + col) < width ? (x + col) : width;
    int miny = 0;
    int maxy = (y + row) < height ? (y + row) : height;
    for (; minx <= maxx; minx++)
    {
        if (isred[minx + maxy * width] == 1)
            isred[minx + maxy * width] = 2;
    }
    minx = (-y + col) > 0 ? (-y + col) : 0;
    maxx = (y + col) < width ? (y + col) : width;
    maxy = (x + row) < height ? (x + row) : height;
    for (; minx <= maxx; minx++)
    {
        if (isred[minx + maxy * width] == 1)
            isred[minx + maxy * width] = 2;
    }
    minx = (-y + col) > 0 ? (-y + col) : 0;
    maxx = (y + col) < width ? (y + col) : width;
    maxy = (-x + row) > 0 ? (-x + row) : 0;
    for (; minx <= maxx; minx++)
    {
        if (isred[minx + maxy * width] == 1)
            isred[minx + maxy * width] = 2;
    }
    minx = (-x + col) > 0 ? (-x + col) : 0;
    maxx = (x + col) < width ? (x + col) : width;
    maxy = (-y + row) > 0 ? (-y + row) : 0;
    for (; minx <= maxx; minx++)
    {
        if (isred[minx + maxy * width] == 1)
            isred[minx + maxy * width] = 2;
    }
}

void MidPointCircle (int rayon, char *isred, int col, int row, int width, int height)
{
    int x, y, d;
    x = 0;
    y = rayon;
    d = 1- rayon;

    circle_point (isred, x, y, col, row, width, height);
    while (y > x)
    {
        if (d < 0)
        {
            d += x * 2 + 3;
            x++;
        }
        else
        {
            d += (x - y) * 2 + 5;
            x++;
            y--;
        }
        circle_point (isred, x, y, col, row, width, height);
    }
}

int find_region (int   row,
                 int   col,
                 int  *rtop,
                 int  *rbot,
                 int  *rleft,
                 int  *rright,
                 char *isred,
                 int   width,
                 int   height)
{

    int *rows, *cols, list_length = 0, max_length = 100;
    int  mydir;
    int  total = 1;
    int deport = 0;

    /* a relatively efficient way to find all connected points in a
     * region.  It considers points that have isred == 1, sets them to 2
     * if they are connected to the starting point.
     * row and col are the starting point of the region,
     * the next four params define a rectangle our region fits into.
     * isred is an array that tells us if pixels are red or not.
     */

    *rtop = row;
    *rbot = row;
    *rleft = col;
    *rright = col;

    rows = (int*) malloc (width * height * sizeof(int));
    cols = (int*) malloc (width * height * sizeof(int));

    rows[0] = row;
    cols[0] = col;
    list_length = 1;

    do {
        list_length -= 1;
        max_length -= 1;
        row = rows[list_length];
        col = cols[list_length];
        for (mydir = 0; mydir < 8 ; mydir++)
        {
            switch (mydir) {
                case 0:
                    //  going left
                    if (col - 1 < 0) break;
                    if (isred[col-1+row*width] == 1) {
                        isred[col-1+row*width] = 2;
                        if (*rleft > col-1) *rleft = col-1;
                        rows[list_length] = row;
                        cols[list_length] = col-1;
                        list_length+=1;
                        total += 1;
                    }
                    break;
                case 1:
                    // up and left
                    if (col - 1 < 0 || row -1 < 0 ) break;
                    if (isred[col-1+(row-1)*width] == 1 ) {
                        isred[col-1+(row-1)*width] = 2;
                        if (*rleft > col -1) *rleft = col-1;
                        if (*rtop > row -1) *rtop = row-1;
                        rows[list_length] = row-1;
                        cols[list_length] = col-1;
                        list_length += 1;
                        total += 1;
                    }
                    break;
                case 2:
                    // up
                    if (row -1 < 0 ) break;
                    if (isred[col + (row-1)*width] == 1) {
                        isred[col + (row-1)*width] = 2;
                        if (*rtop > row-1) *rtop=row-1;
                        rows[list_length] = row-1;
                        cols[list_length] = col;
                        list_length +=1;
                        total += 1;
                    }
                    break;
                case 3:
                    //  up and right
                    if (col + 1 >= width || row -1 < 0 ) break;
                    if (isred[col+1+(row-1)*width] == 1) {
                        isred[col+1+(row-1)*width] = 2;
                        if (*rright < col +1) *rright = col+1;
                        if (*rtop > row -1) *rtop = row-1;
                        rows[list_length] = row-1;
                        cols[list_length] = col+1;
                        list_length += 1;
                        total +=1;
                    }
                    break;
                case 4:
                    // going right
                    if (col + 1 >= width) break;
                    if (isred[col+1+row*width] == 1) {
                        isred[col+1+row*width] = 2;
                        if (*rright < col+1) *rright = col+1;
                        rows[list_length] = row;
                        cols[list_length] = col+1;
                        list_length += 1;
                        total += 1;
                    }
                    break;
                case 5:
                    // down and right
                    if (col + 1 >= width || row +1 >= height ) break;
                    if (isred[col+1+(row+1)*width] ==1) {
                        isred[col+1+(row+1)*width] = 2;
                        if (*rright < col +1) *rright = col+1;
                        if (*rbot < row +1) *rbot = row+1;
                        rows[list_length] = row+1;
                        cols[list_length] = col+1;
                        list_length += 1;
                        total += 1;
                    }
                    break;
                case 6:
                    // down
                    if (row +1 >=  height ) break;
                    if (isred[col + (row+1)*width] == 1) {
                        isred[col + (row+1)*width] = 2;
                        if (*rbot < row+1) *rbot=row+1;
                        rows[list_length] = row+1;
                        cols[list_length] = col;
                        list_length += 1;
                        total += 1;
                    }
                    break;
                case 7:
                    // down and left
                    if (col - 1 < 0  || row +1 >= height ) break;
                    if (isred[col-1+(row+1)*width] == 1) {
                        isred[col-1+(row+1)*width] = 2;
                        if (*rleft  > col -1) *rleft = col-1;
                        if (*rbot < row +1) *rbot = row+1;
                        rows[list_length] = row+1;
                        cols[list_length] = col-1;
                        list_length += 1;
                        total += 1;
                    }
                    break;
                default:
                    break;
            }
        }

    } while (list_length > 0 && max_length > 0);  // stop when we add no more
    //fclose (f);
    free (rows);
    free (cols);


    return total;
}

char* init_is_red (GdkPixbuf* pix, int width, int height){
    char*      is_red = NULL;
    int        i, j;
    guchar 	ad_red, ad_green, ad_blue;
    const int  THRESHOLD = 1;
    is_red = (char*)malloc(sizeof(char) * (width * height));

    guchar* pixels = gdk_pixbuf_get_pixels (pix);

    for (i = 0; i < height; i++)  {
        for (j = 0; j < width; j++) {
            ad_red = pixels[(j + i * width) * 3 + 0];
            ad_green = pixels[(j + i * width) * 3 + 1];
            ad_blue = pixels[(j + i * width) * 3 + 2];
            ad_red *= RED_FACTOR;
            ad_green *= GREEN_FACTOR;
            ad_blue *= BLUE_FACTOR;

            if ((ad_red >= ad_green - THRESHOLD) && (ad_red >= ad_blue - THRESHOLD))
                is_red[j + i * width] = 1;
        }
    }
    return is_red;
}


/* returns TRUE if the pixbuf has been modified */
int fix_redeye(GdkPixbuf* pix, int width, int height,
               char      *isred,
               int        x,
               int        y)
{

    int  region_fixed = FALSE;
    int       rowstride, channels;
    int       search, i, j, ii, jj;
    unsigned char    ad_red, ad_blue, ad_green;
    int       rtop, rbot, rleft, rright; /* edges of region */
    int       num_pix;

    guchar* pixels = gdk_pixbuf_get_pixels (pix);

    channels = 3;
    rowstride = width * channels;

    /*
        * if isred is 0, we don't think the point is red, 1 means red, 2 means
        * part of our region already.
        */

    for (search = 0; ! region_fixed && (search < REGION_SEARCH_SIZE); search++)
        for (i = MAX (0, y - search); ! region_fixed && (i <= MIN (height - 1, y + search)); i++ )
            for (j = MAX (0, x - search); ! region_fixed && (j <= MIN (width - 1, x + search)); j++) {
                if (isred[j + i * width] == 0)
                    continue;

                isred[j + i * width] = 2;

                num_pix = find_region (i, j, &rtop, &rbot, &rleft, &rright, isred, width, height);

                /* Fix the region. */
                for (ii = rtop; ii <= rbot; ii++)
                    for (jj = rleft; jj <= rright; jj++)
                        if (isred[jj + ii * width] == 2) { /* Fix the pixel. */
                            ad_red = pixels[(jj + ii * width) * 3 + 0];
                            ad_green = pixels[(jj + ii * width) * 3 + 1];
                            ad_blue = pixels[(jj + ii * width) * 3 + 2];
                            //ad_red *= RED_FACTOR;
                            pixels[(jj + ii * width) * 3 + 0] = (guchar)(((float) (ad_green + ad_blue)) / (2.0 * RED_FACTOR));
                            pixels[(jj + ii * width) * 3 + 1] = (guchar)((float)ad_green * GREEN_FACTOR);
                            pixels[(jj + ii * width) * 3 + 2] = (guchar)((float)ad_blue * BLUE_FACTOR);

                            isred[jj + ii * width] = 0;
                        }

                region_fixed = TRUE;
            }

    return region_fixed;
}

/* returns TRUE if the pixbuf has been modified */
int fix_redeye2(GdkPixbuf* pix, int width, int height,
                char      *isred,
                int 	   rayon,
                int        x,
                int        y)
{
    int  region_fixed = FALSE;
    int       rowstride, channels, ii, jj;
    unsigned char    ad_red, ad_blue, ad_green;
    int       num_pix;

    guchar* pixels = gdk_pixbuf_get_pixels (pix);

    channels = 3;
    rowstride = width * channels;

    /*
        * if isred is 0, we don't think the point is red, 1 means red, 2 means
        * part of our region already.
        */
    MidPointCircle (rayon, isred, x, y, width, height);
    /* Fix the region. */
    for (ii = y - rayon ; ii <= y + rayon; ii++)
        //for (ii = 0 ; ii < height; ii++)
    {
        for (jj = x - rayon; jj <= x + rayon; jj++)
            //for (jj = 0; jj < width; jj++)
        {
            if (isred[jj + ii * width] == 2)
            { /* Fix the pixel. */
                ad_red = pixels[(jj + ii * width) * 3 + 0];
                ad_green = pixels[(jj + ii * width) * 3 + 1];
                ad_blue = pixels[(jj + ii * width) * 3 + 2];
                //ad_red *= RED_FACTOR;
                pixels[(jj + ii * width) * 3 + 0] = (guchar)(((float) (ad_green + ad_blue)) / (2.0 * RED_FACTOR));
                pixels[(jj + ii * width) * 3 + 1] = (guchar)((float)ad_green * GREEN_FACTOR);
                pixels[(jj + ii * width) * 3 + 2] = (guchar)((float)ad_blue * BLUE_FACTOR);

                isred[jj + ii * width] = 0;
                region_fixed = TRUE;
            }
        }
    }
    return region_fixed;
}

int ImageRedEyes(GdkPixbuf* pix, int x, int y)
{
    int width = gdk_pixbuf_get_width(pix);
    int height = gdk_pixbuf_get_height(pix);
    char* is_red = init_is_red (pix, width, height);
    int ret = 0;
    if (fix_redeye (pix, width, height, is_red, x, y))
        ret = 1;
    if(is_red)
        free (is_red);
    return ret;
}

int ImageRedEyes2(GdkPixbuf* pix, int rayon, int x, int y)
{
    int width = gdk_pixbuf_get_width(pix);
    int height = gdk_pixbuf_get_height(pix);
    char* is_red = init_is_red (pix, width, height);
    int ret = 0;
    if (fix_redeye2 (pix, width, height, is_red, rayon, x, y))
        ret = 1;
    if(is_red)
        free (is_red);
    return ret;
}

int
image_red_eyes(GdkPixbuf *pix, GSList *l)
{
    int width = gdk_pixbuf_get_width(pix);
    int height = gdk_pixbuf_get_height(pix);

    char* is_red = init_is_red (pix, width, height);
    int ret = 0;

    GSList *it;
    for(it = l; it != NULL; it = it->next) {
        CoordRedEyes* coords = (CoordRedEyes*)it->data;
        if (fix_redeye2(pix, width, height, is_red, coords->radius, coords->center_x, coords->center_y))
            ret = 1;
    }
    if(is_red)
        free (is_red);
    return ret;
}