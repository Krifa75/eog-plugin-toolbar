#ifndef __EVAS_FILTERS_H__
#define __EVAS_FILTERS_H__

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifdef __cplusplus

extern "C"
{
#endif

#include <math.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <limits.h>
#include <time.h>
#include <unistd.h>
#include <libintl.h>
#include <ctype.h>

#include <libexif/exif-utils.h>
#include <libexif/exif-loader.h>
#include <libexif/exif-ifd.h>
#include <libexif/exif-tag.h>
#include <libexif/exif-content.h>
#include <libexif/exif-format.h>
#include <libexif/exif-mem.h>

#define deg2rad(x) (x * 180.0 / M_PI) 

#define RangQuantum 255UL
#define ScaleQuantum ((double)1.0/ (double)RangQuantum)
#define Epsilon 1.0e-6

#define MAX_F 255
#define MAX_FF 255.0
#define SCALE_FF (1.0 / MAX_FF)
#define MagickPI 3.1415926535897932384624338327950288419716939937510


#define ARRONDI_F(value)	( (unsigned char)( (value) < 0.0 ? 0.0 :((value) > MAX_F ? MAX_F : (value) + 0.5) ) )

/*
  C'est le nombre maximal de filtrages
  autorises. 
 */
#define MAX_RETINEX_SCALES 8
/*
  Definit comment sont repartis les
  differents filtres en fonction de
  l'echelle (~= ecart type de la gaussienne)
 */
#define RETINEX_UNIFORM 0
#define RETINEX_LOW 1
#define RETINEX_HIGH 2

#ifndef MIN
#define MIN(x,y) ((x < y) ? x : y)
#endif

#ifndef MAX
#define MAX(x,y) ((x > y) ? x : y)
#endif

#define REGION_SEARCH_SIZE 100
#define MAX_TAILLE_IMAGE 512
#define TAG_VALUE_BUF 1024

#undef _
  //#define _(String) dgettext ("PACKAGE", String)
#define FE_A								_("à")
#define FE_LE								_("le")

typedef enum
{
	EOG_ORIENT_NONE = 1,
	EOG_FLIP_HORIZONTAL,
	EOG_ROTATE_180_CW,
	EOG_FLIP_VERTICAL,
	EOG_FLIP_TRANSPOSE,
	EOG_ROTATE_90_CW,
	EOG_FLIP_TRANSVERSE,
	EOG_ROTATE_90_CCW
} Eog_Orient;

// Rotation                   : 0 90 180 270 (0->6->3->8) 
// Rotation par transposition : 0 90 180 270 (2->7->4->5)

Eog_Orient  rotation_exif_set (const char *file);

int get_rotation_exif (const char *file);

Eog_Orient rotation_90_CW_get(Eog_Orient orient);

/* void svisionneuse_image_orient(Evas_Object *img, Eog_Orient orientation); */

void add_exif_to_jpg(const char* src_file, const char* dst_file);
void add_exif_to_jpg_rotate(const char* src_file, const char* dst_file, short rotation, int is_jpg);

/* int ImageRedEtes2(Evas_Object* img, int x, int y, int r); */
/* void ImageToGamma2(Evas_Object *image, double Gamma); */
/* void ImageToNormalize(Evas_Object *image); */

#ifdef __cplusplus
}
#endif

#endif /* __EVAS_FILTERS_H__ */
