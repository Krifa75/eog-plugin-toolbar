//
// Created by krifa on 13/07/2020.
//

#ifndef EOG_JOBS_FILTER_H
#define EOG_JOBS_FILTER_H

#include <gtk/gtk.h>
#include <eog-3.0/eog/eog-jobs.h>

G_BEGIN_DECLS

//#define EOG_TYPE_JOB_FILTER                (eog_job_filter_get_type ())
//#define EOG_JOB_FILTER(obj)                (G_TYPE_CHECK_INSTANCE_CAST ((obj), EOG_TYPE_JOB_FILTER, EogJobFilter))
//#define EOG_JOB_FILTER_CLASS(klass)        (G_TYPE_CHECK_CLASS_CAST ((klass),  EOG_TYPE_JOB_FILTER, EogJobFilterClass))
//#define EOG_IS_JOB_FILTER(obj)             (G_TYPE_CHECK_INSTANCE_TYPE ((obj), EOG_TYPE_JOB_FILTER))
//#define EOG_IS_JOB_FILTER_CLASS(klass)     (G_TYPE_CHECK_CLASS_TYPE ((klass),  EOG_TYPE_JOB_FILTER))
//#define EOG_JOB_FILTER_GET_CLASS(obj)      (G_TYPE_INSTANCE_GET_CLASS ((obj),  EOG_TYPE_JOB_FILTER, EogJobFilterClass))

#define EOG_TYPE_JOB_CROP_FILTER                (eog_job_crop_filter_get_type ())
#define EOG_JOB_CROP_FILTER(obj)                (G_TYPE_CHECK_INSTANCE_CAST ((obj), EOG_TYPE_JOB_CROP_FILTER, EogJobCropFilter))
#define EOG_JOB_CROP_FILTER_CLASS(klass)        (G_TYPE_CHECK_CLASS_CAST ((klass),  EOG_TYPE_JOB_CROP_FILTER, EogJobCropFilterClass))
#define EOG_IS_JOB_CROP_FILTER(obj)             (G_TYPE_CHECK_INSTANCE_TYPE ((obj), EOG_TYPE_JOB_CROP_FILTER))
#define EOG_IS_JOB_CROP_FILTER_CLASS(klass)     (G_TYPE_CHECK_CLASS_TYPE ((klass),  EOG_TYPE_JOB_CROP_FILTER))
#define EOG_JOB_CROP_FILTER_GET_CLASS(obj)      (G_TYPE_INSTANCE_GET_CLASS ((obj),  EOG_TYPE_JOB_CROP_FILTER, EogJobCropFilterClass))

#define EOG_TYPE_JOB_COMMENT_FILTER                (eog_job_comment_filter_get_type ())
#define EOG_JOB_COMMENT_FILTER(obj)                (G_TYPE_CHECK_INSTANCE_CAST ((obj), EOG_TYPE_JOB_COMMENT_FILTER, EogJobCommentFilter))
#define EOG_JOB_COMMENT_FILTER_CLASS(klass)        (G_TYPE_CHECK_CLASS_CAST ((klass),  EOG_TYPE_JOB_COMMENT_FILTER, EogJobCommentFilterClass))
#define EOG_IS_JOB_COMMENT_FILTER(obj)             (G_TYPE_CHECK_INSTANCE_TYPE ((obj), EOG_TYPE_JOB_COMMENT_FILTER))
#define EOG_IS_JOB_COMMENT_FILTER_CLASS(klass)     (G_TYPE_CHECK_CLASS_TYPE ((klass),  EOG_TYPE_JOB_COMMENT_FILTER))
#define EOG_JOB_COMMENT_FILTER_GET_CLASS(obj)      (G_TYPE_INSTANCE_GET_CLASS ((obj),  EOG_TYPE_JOB_COMMENT_FILTER, EogJobCommentFilterClass))

#define EOG_TYPE_JOB_REDUCE_FILTER                (eog_job_reduce_filter_get_type ())
#define EOG_JOB_REDUCE_FILTER(obj)                (G_TYPE_CHECK_INSTANCE_CAST ((obj), EOG_TYPE_JOB_REDUCE_FILTER, EogJobReduceFilter))
#define EOG_JOB_REDUCE_FILTER_CLASS(klass)        (G_TYPE_CHECK_CLASS_CAST ((klass),  EOG_TYPE_JOB_REDUCE_FILTER, EogJobReduceFilterClass))
#define EOG_IS_JOB_REDUCE_FILTER(obj)             (G_TYPE_CHECK_INSTANCE_TYPE ((obj), EOG_TYPE_JOB_REDUCE_FILTER))
#define EOG_IS_JOB_REDUCE_FILTER_CLASS(klass)     (G_TYPE_CHECK_CLASS_TYPE ((klass),  EOG_TYPE_JOB_REDUCE_FILTER))
#define EOG_JOB_REDUCE_FILTER_GET_CLASS(obj)      (G_TYPE_INSTANCE_GET_CLASS ((obj),  EOG_TYPE_JOB_REDUCE_FILTER, EogJobReduceFilterClass))

#define EOG_TYPE_JOB_BRIGHTNESS_FILTER                (eog_job_brightness_filter_get_type ())
#define EOG_JOB_BRIGHTNESS_FILTER(obj)                (G_TYPE_CHECK_INSTANCE_CAST ((obj), EOG_TYPE_JOB_BRIGHTNESS_FILTER, EogJobBrightnessFilter))
#define EOG_JOB_BRIGHTNESS_FILTER_CLASS(klass)        (G_TYPE_CHECK_CLASS_CAST ((klass),  EOG_TYPE_JOB_BRIGHTNESS_FILTER, EogJobBrightnessFilterClass))
#define EOG_IS_JOB_BRIGHTNESS_FILTER(obj)             (G_TYPE_CHECK_INSTANCE_TYPE ((obj), EOG_TYPE_JOB_BRIGHTNESS_FILTER))
#define EOG_IS_JOB_BRIGHTNESS_FILTER_CLASS(klass)     (G_TYPE_CHECK_CLASS_TYPE ((klass),  EOG_TYPE_JOB_BRIGHTNESS_FILTER))
#define EOG_JOB_BRIGHTNESS_FILTER_GET_CLASS(obj)      (G_TYPE_INSTANCE_GET_CLASS ((obj),  EOG_TYPE_JOB_BRIGHTNESS_FILTER, EogJobBrightnessFilterClass))

#define EOG_TYPE_JOB_CONTRAST_FILTER                (eog_job_contrast_filter_get_type ())
#define EOG_JOB_CONTRAST_FILTER(obj)                (G_TYPE_CHECK_INSTANCE_CAST ((obj), EOG_TYPE_JOB_CONTRAST_FILTER, EogJobContrastFilter))
#define EOG_JOB_CONTRAST_FILTER_CLASS(klass)        (G_TYPE_CHECK_CLASS_CAST ((klass),  EOG_TYPE_JOB_CONTRAST_FILTER, EogJobContrastFilterClass))
#define EOG_IS_JOB_CONTRAST_FILTER(obj)             (G_TYPE_CHECK_INSTANCE_TYPE ((obj), EOG_TYPE_JOB_CONTRAST_FILTER))
#define EOG_IS_JOB_CONTRAST_FILTER_CLASS(klass)     (G_TYPE_CHECK_CLASS_TYPE ((klass),  EOG_TYPE_JOB_CONTRAST_FILTER))
#define EOG_JOB_CONTRAST_FILTER_GET_CLASS(obj)      (G_TYPE_INSTANCE_GET_CLASS ((obj),  EOG_TYPE_JOB_CONTRAST_FILTER, EogJobContrastFilterClass))

#define EOG_TYPE_JOB_RED_EYES_FILTER                (eog_job_red_eyes_filter_get_type ())
#define EOG_JOB_RED_EYES_FILTER(obj)                (G_TYPE_CHECK_INSTANCE_CAST ((obj), EOG_TYPE_JOB_RED_EYES_FILTER, EogJobRedEyesFilter))
#define EOG_JOB_RED_EYES_FILTER_CLASS(klass)        (G_TYPE_CHECK_CLASS_CAST ((klass),  EOG_TYPE_JOB_RED_EYES_FILTER, EogJobRedEyesFilterClass))
#define EOG_IS_JOB_RED_EYES_FILTER(obj)             (G_TYPE_CHECK_INSTANCE_TYPE ((obj), EOG_TYPE_JOB_RED_EYES_FILTER))
#define EOG_IS_JOB_RED_EYES_FILTER_CLASS(klass)     (G_TYPE_CHECK_CLASS_TYPE ((klass),  EOG_TYPE_JOB_RED_EYES_FILTER))
#define EOG_JOB_RED_EYES_FILTER_GET_CLASS(obj)      (G_TYPE_INSTANCE_GET_CLASS ((obj),  EOG_TYPE_JOB_RED_EYES_FILTER, EogJobRedEyesFilterClass))

#define EOG_TYPE_JOB_GRAYSCALE_FILTER                (eog_job_grayscale_filter_get_type ())
#define EOG_JOB_GRAYSCALE_FILTER(obj)                (G_TYPE_CHECK_INSTANCE_CAST ((obj), EOG_TYPE_JOB_GRAYSCALE_FILTER, EogJobGrayscaleFilter))
#define EOG_JOB_GRAYSCALE_FILTER_CLASS(klass)        (G_TYPE_CHECK_CLASS_CAST ((klass),  EOG_TYPE_JOB_GRAYSCALE_FILTER, EogJobGrayscaleFilterClass))
#define EOG_IS_JOB_GRAYSCALE_FILTER(obj)             (G_TYPE_CHECK_INSTANCE_TYPE ((obj), EOG_TYPE_JOB_GRAYSCALE_FILTER))
#define EOG_IS_JOB_GRAYSCALE_FILTER_CLASS(klass)     (G_TYPE_CHECK_CLASS_TYPE ((klass),  EOG_TYPE_JOB_GRAYSCALE_FILTER))
#define EOG_JOB_GRAYSCALE_FILTER_GET_CLASS(obj)      (G_TYPE_INSTANCE_GET_CLASS ((obj),  EOG_TYPE_JOB_GRAYSCALE_FILTER, EogJobGrayscaleFilterClass))

#define EOG_TYPE_JOB_SEPIA_FILTER                (eog_job_sepia_filter_get_type ())
#define EOG_JOB_SEPIA_FILTER(obj)                (G_TYPE_CHECK_INSTANCE_CAST ((obj), EOG_TYPE_JOB_SEPIA_FILTER, EogJobSepiaFilter))
#define EOG_JOB_SEPIA_FILTER_CLASS(klass)        (G_TYPE_CHECK_CLASS_CAST ((klass),  EOG_TYPE_JOB_SEPIA_FILTER, EogJobSepiaFilterClass))
#define EOG_IS_JOB_SEPIA_FILTER(obj)             (G_TYPE_CHECK_INSTANCE_TYPE ((obj), EOG_TYPE_JOB_SEPIA_FILTER))
#define EOG_IS_JOB_SEPIA_FILTER_CLASS(klass)     (G_TYPE_CHECK_CLASS_TYPE ((klass),  EOG_TYPE_JOB_SEPIA_FILTER))
#define EOG_JOB_SEPIA_FILTER_GET_CLASS(obj)      (G_TYPE_INSTANCE_GET_CLASS ((obj),  EOG_TYPE_JOB_SEPIA_FILTER, EogJobSepiaFilterClass))

//typedef struct _EogJobFilterClass          EogJobFilterClass;

typedef struct _EogJobCropFilter          EogJobCropFilter;
typedef struct _EogJobCropFilterClass     EogJobCropFilterClass;

typedef struct _EogJobCommentFilter          EogJobCommentFilter;
typedef struct _EogJobCommentFilterClass     EogJobCommentFilterClass;

typedef struct _EogJobReduceFilter          EogJobReduceFilter;
typedef struct _EogJobReduceFilterClass     EogJobReduceFilterClass;

typedef struct _EogJobBrightnessFilter          EogJobBrightnessFilter;
typedef struct _EogJobBrightnessFilterClass     EogJobBrightnessFilterClass;

typedef struct _EogJobContrastFilter          EogJobContrastFilter;
typedef struct _EogJobContrastFilterClass     EogJobContrastFilterClass;

typedef struct _EogJobRedEyesFilter          EogJobRedEyesFilter;
typedef struct _EogJobRedEyesFilterClass     EogJobRedEyesFilterClass;

typedef struct _EogJobGrayscaleFilter          EogJobGrayscaleFilter;
typedef struct _EogJobGrayscaleFilterClass     EogJobGrayscaleFilterClass;

typedef struct _EogJobSepiaFilter          EogJobSepiaFilter;
typedef struct _EogJobSepiaFilterClass     EogJobSepiaFilterClass;

struct _EogJobCropFilter
{
    EogJob           parent;

    EogImage *image;
    GSList   *rect;
};

struct _EogJobCropFilterClass
{
    EogJobClass      parent_class;
};

struct _EogJobCommentFilter
{
    EogJob           parent;

    EogImage *image;
    gchar    *text;
    gint      pos_x;
    gint      pos_y;
    gint      font_size;
    GdkPixbuf       *pixbuf_output;
};

struct _EogJobCommentFilterClass
{
    EogJobClass      parent_class;
};

struct _EogJobReduceFilter
{
    EogJob           parent;

    EogImage        *image;
    guint            width;
    guint            height;
};

struct _EogJobReduceFilterClass
{
    EogJobClass      parent_class;
};

struct _EogJobBrightnessFilter
{
    EogJob           parent;

    EogImage        *image;
    GdkPixbuf       *pixbuf_original;
    GdkPixbuf       *pixbuf_output;
    gdouble          brightness_value;
};

struct _EogJobBrightnessFilterClass
{
    EogJobClass      parent_class;
};

struct _EogJobContrastFilter
{
    EogJob           parent;

    EogImage        *image;
    GdkPixbuf       *pixbuf_output;
};

struct _EogJobContrastFilterClass
{
    EogJobClass      parent_class;
};

struct _EogJobRedEyesFilter
{
    EogJob           parent;

    EogImage  *image;
    GSList    *rects;
    GdkPixbuf *pixbuf_output;
};

struct _EogJobRedEyesFilterClass
{
    EogJobClass      parent_class;
};

struct _EogJobGrayscaleFilter
{
    EogJob           parent;

    EogImage  *image;
    GdkPixbuf *pixbuf_output;
};

struct _EogJobGrayscaleFilterClass
{
    EogJobClass      parent_class;
};

struct _EogJobSepiaFilter
{
    EogJob           parent;

    EogImage  *image;
    GdkPixbuf *pixbuf_output;
};

struct _EogJobSepiaFilterClass
{
    EogJobClass      parent_class;
};

GType 	 eog_job_crop_filter_get_type     (void) G_GNUC_CONST;
EogJob 	*eog_job_crop_filter_new          (EogImage *image,
                                           GSList *rect);

GType 	 eog_job_comment_filter_get_type   (void) G_GNUC_CONST;
EogJob 	*eog_job_comment_filter_new        (EogImage *image,
                                            gchar    *text,
                                            gint      pos_x,
                                            gint      pos_y,
                                            gint      font_size);

GType 	 eog_job_reduce_filter_get_type     (void) G_GNUC_CONST;
EogJob 	*eog_job_reduce_filter_new          (EogImage *image,
                                             guint     width,
                                             guint     height);

GType 	 eog_job_brightness_filter_get_type (void) G_GNUC_CONST;
EogJob 	*eog_job_brightness_filter_new      (EogImage  *image,
                                             GdkPixbuf *pixbuf_original,
                                             gdouble    brightness_value);

GType 	 eog_job_contrast_filter_get_type   (void) G_GNUC_CONST;
EogJob 	*eog_job_contrast_filter_new        (EogImage *image);

GType 	 eog_job_red_eyes_filter_get_type   (void) G_GNUC_CONST;
EogJob 	*eog_job_red_eyes_filter_new        (EogImage *image,
                                             GSList *rect);

GType 	 eog_job_grayscale_filter_get_type  (void) G_GNUC_CONST;
EogJob 	*eog_job_grayscale_filter_new       (EogImage *image);

GType 	 eog_job_sepia_filter_get_type      (void) G_GNUC_CONST;
EogJob 	*eog_job_sepia_filter_new           (EogImage *image);

G_END_DECLS

#endif //EOG_JOBS_FILTER_H
