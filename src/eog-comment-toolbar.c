/* EogToolbar - Plugin.

   Copyright (C) 2019 Yahiaoui Fakhri

   This file is part of the plugin for Eog.

   EogToolbar is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License as published by the Free
   Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   EogToolbar is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
   for more details.

   You should have received a copy of the GNU General Public License
   along with EogImagesManager; see the file COPYING.  If not, write to the Free
   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   This file implements a toolbar for eog.  */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "eog-comment-toolbar.h"
#include <glib/gi18n.h>
#include <eog-3.0/eog/eog-scroll-view.h>
#include <eog-3.0/eog/eog-thumb-view.h>
#include <eog-3.0/eog/eog-thumb-nav.h>
#include <eog/eog-job-scheduler.h>
#include "eog-utils.h"
#include "eog-jobs-filter.h"

struct _EogCommentToolbarPrivate {
    EogWindow *window;
    GtkStack *stack;
    GtkWidget *area_progress;

    GtkWidget *select;

    GString *input_text;

    guint font_size;
    guint new_font_size;

    gint x, y;
    gint w, h;

    gint pos_x, pos_y;
};

G_DEFINE_TYPE_WITH_PRIVATE (EogCommentToolbar, eog_comment_toolbar, GTK_TYPE_TOOLBAR)

enum {
    PROP_0,
    PROP_WINDOW,
    PROP_STACK,
    PROP_AREA,
};

static gpointer save_comment (gpointer comment);

static gboolean
remove_last_character (GString *input_text)
{
    gboolean removed = FALSE;
    gchar *last_char = g_utf8_find_prev_char(input_text->str, input_text->str + (input_text->len));
    if(last_char) {
        input_text = g_string_erase(input_text, input_text->len - strlen(last_char), -1);
        removed = TRUE;
    }
    return removed;
}

static void
draw_text (cairo_t *cr, gint pos_x, gint pos_y, const gchar *text)
{
    cairo_move_to(cr, pos_x, pos_y);
    cairo_set_source_rgb(cr, 0, 0, 0);
    cairo_show_text(cr, text);

    cairo_move_to(cr, pos_x-2, pos_y-2);
    cairo_set_source_rgb(cr, 1, 1, 1);
    cairo_show_text(cr, text);

    return;
}

static gboolean
draw_cb (GtkWidget *widget, cairo_t *cr, gpointer user_data)
{
    g_return_val_if_fail(EOG_IS_COMMENT_TOOLBAR(user_data), FALSE);
    EogCommentToolbarPrivate *priv = EOG_COMMENT_TOOLBAR(user_data)->priv;
    eog_scroll_view_get_image_coords (EOG_SCROLL_VIEW(eog_window_get_view (priv->window)), &priv->x, &priv->y, &priv->w, &priv->h);

    if(!priv->input_text->len)
        return FALSE;

    cairo_select_font_face(cr, "Helvetica", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
//    cairo_set_font_size(cr, priv->font_size);
    cairo_set_font_size(cr, priv->new_font_size);

    cairo_text_extents_t extents;
    cairo_text_extents(cr, priv->input_text->str, &extents);

//    g_warning("Text(%f, %f), Advance(%f, %f), Size(%f, %f)", extents.x_bearing, extents.y_bearing, extents.x_advance, extents.y_advance, extents.width, extents.height);

    if(extents.width > (priv->w - 10)){
        if(priv->font_size != priv->new_font_size) {
            g_warning("Text too big");
            priv->new_font_size = priv->font_size;
            cairo_set_font_size(cr, priv->font_size);
            cairo_text_extents(cr, priv->input_text->str, &extents);
        }
        else
            remove_last_character(priv->input_text);
    } else {
        gdouble w = (gdouble)(priv->w) / 2;

        priv->pos_x = (priv->x + w) - (extents.width/2 + extents.x_bearing);
        priv->pos_y = priv->y + priv->h - 20;
        priv->font_size = priv->new_font_size;
    }

    draw_text(cr, priv->pos_x, priv->pos_y, priv->input_text->str);
    return TRUE;
}

static gboolean
input_text_cb (GtkWidget *widget,
               GdkEvent  *event,
               gpointer   user_data)
{
    g_return_val_if_fail(EOG_IS_COMMENT_TOOLBAR(user_data), FALSE);
    EogCommentToolbarPrivate *priv = EOG_COMMENT_TOOLBAR(user_data)->priv;

    gboolean input_modified = FALSE, save = FALSE;
    switch(event->key.keyval){
        case GDK_KEY_BackSpace:
            input_modified = remove_last_character(priv->input_text);
            break;
        case GDK_KEY_KP_Enter:
        case GDK_KEY_Return:
            save_comment (EOG_COMMENT_TOOLBAR(user_data));
            save = TRUE;
            break;
        case GDK_KEY_KP_Add:
        case GDK_KEY_plus:
            if ( (event->key.state & GDK_CONTROL_MASK)){//modifiers)) {
                priv->new_font_size += 5;
                input_modified = TRUE;
            }
            break;
        case GDK_KEY_KP_Subtract:
        case GDK_KEY_minus:
            if ( (event->key.state & GDK_CONTROL_MASK)) {
                if(priv->font_size < 12)
                    priv->font_size = 12;
                else
                    priv->new_font_size -= 5;
                input_modified = TRUE;
            }
            break;
        case GDK_KEY_Tab:
            priv->input_text = g_string_append_c(priv->input_text, ' ');
            input_modified = TRUE;
            break;
        default:
            break;
    }

    if(save)
        return TRUE;

    if(!input_modified) {
        gunichar last_char = g_utf8_get_char(event->key.string);
        if (g_unichar_isprint(last_char))
            g_string_append(priv->input_text, event->key.string);
    }
    gtk_widget_queue_draw(widget);
    return TRUE;
}

static void
eog_comment_toolbar_constructed (GObject *object)
{
    if (G_OBJECT_CLASS (eog_comment_toolbar_parent_class)->constructed)
        G_OBJECT_CLASS (eog_comment_toolbar_parent_class)->constructed (object);

    eog_comment_toolbar_init_area(EOG_COMMENT_TOOLBAR(object));
    return;
}

void
eog_comment_toolbar_init_area (EogCommentToolbar *comment)
{
    g_return_if_fail(EOG_IS_COMMENT_TOOLBAR(comment));

    EogCommentToolbarPrivate *priv = comment->priv;

    EogScrollView  *view = EOG_SCROLL_VIEW(eog_window_get_view(priv->window));
    g_assert (GTK_IS_GRID(view));

    eog_scroll_view_set_zoom_mode (view, EOG_ZOOM_MODE_SHRINK_TO_FIT);
    eog_scroll_view_get_image_coords (EOG_SCROLL_VIEW(view), &priv->x, &priv->y, &priv->w, &priv->h);

    GtkWidget *overlay = gtk_grid_get_child_at(GTK_GRID(view), 0, 0);
    g_assert(GTK_IS_OVERLAY(overlay));

    priv->input_text = g_string_new(NULL);
    priv->font_size = priv->new_font_size = 36;

    priv->select = g_object_new (GTK_TYPE_DRAWING_AREA,
                                 "can-focus", TRUE,
                                 NULL);//gtk_drawing_area_new();
    gtk_widget_set_name (priv->select, "select-comment");
    gtk_widget_add_events(GTK_WIDGET(priv->select), GDK_KEY_PRESS_MASK);

    gtk_overlay_add_overlay(GTK_OVERLAY(overlay), GTK_WIDGET(priv->select));
    gtk_widget_show_all(GTK_WIDGET(priv->select));

//    g_warning("Coords : (%d, %d), (%d, %d)", priv->x, priv->y, priv->w, priv->h);

    g_signal_connect(GTK_WIDGET(priv->select), "key-press-event", G_CALLBACK(input_text_cb), comment);
    g_signal_connect(G_OBJECT(priv->select), "draw", G_CALLBACK(draw_cb), comment);
    gtk_widget_grab_focus(priv->select);

    return;
}

static void
eog_comment_toolbar_dispose (GObject *object)
{

    G_OBJECT_CLASS (eog_comment_toolbar_parent_class)->dispose (object);
    return;
}

static void
eog_comment_toolbar_get_property (GObject    *object,
                               guint       prop_id,
                               GValue     *value,
                               GParamSpec *pspec)
{
    EogCommentToolbarPrivate *priv = EOG_COMMENT_TOOLBAR(object)->priv;

    switch (prop_id){
        case PROP_WINDOW:
            g_value_set_object(value, priv->window);
            break;
        case PROP_STACK:
            g_value_set_object(value, priv->stack);
            break;
        case PROP_AREA:
            g_value_set_object(value, priv->area_progress);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
eog_comment_toolbar_set_property (GObject    *object,
                               guint       prop_id,
                               const GValue     *value,
                               GParamSpec *pspec)
{
    EogCommentToolbarPrivate *priv = EOG_COMMENT_TOOLBAR(object)->priv;

    switch (prop_id){
        case PROP_WINDOW:
            priv->window = EOG_WINDOW(g_value_dup_object (value));
            break;
        case PROP_STACK:
            priv->stack = GTK_STACK(g_value_dup_object (value));
            break;
        case PROP_AREA:
            priv->area_progress = GTK_WIDGET(g_value_get_object (value));
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
eog_comment_toolbar_class_init (EogCommentToolbarClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);

    object_class->constructed = eog_comment_toolbar_constructed;
    object_class->dispose = eog_comment_toolbar_dispose;
    object_class->set_property = eog_comment_toolbar_set_property;
    object_class->get_property = eog_comment_toolbar_get_property;

    g_object_class_install_property(object_class, PROP_WINDOW, g_param_spec_object("window", "", "", EOG_TYPE_WINDOW,
                                                                                   (G_PARAM_WRITABLE |
                                                                                    G_PARAM_CONSTRUCT_ONLY |
                                                                                    G_PARAM_STATIC_STRINGS)));
    g_object_class_install_property(object_class, PROP_STACK, g_param_spec_object("stack", "", "", GTK_TYPE_STACK,
                                                                                  (G_PARAM_WRITABLE |
                                                                                   G_PARAM_CONSTRUCT_ONLY |
                                                                                   G_PARAM_STATIC_STRINGS)));

    g_object_class_install_property(object_class, PROP_AREA, g_param_spec_object("area-progress", "", "", GTK_TYPE_DRAWING_AREA,
                                                                                 (G_PARAM_WRITABLE |
                                                                                  G_PARAM_CONSTRUCT_ONLY |
                                                                                  G_PARAM_STATIC_STRINGS)));

    bind_textdomain_codeset (ET_PACKAGE, "UTF-8");
    textdomain (ET_PACKAGE);
    return;
}

static void
destroy_area (EogCommentToolbar *comment) {
    EogCommentToolbarPrivate *priv = comment->priv;
    EogWindow *window = priv->window;

    if(priv->select) {
        GtkWidget *parent = gtk_widget_get_parent (priv->select);
        gtk_container_remove (GTK_CONTAINER (parent), priv->select);
        //g_object_unref(priv->select);
        priv->select = NULL;
    }

    gtk_stack_set_visible_child_full(GTK_STACK(priv->stack), "main-toolbar", GTK_STACK_TRANSITION_TYPE_OVER_RIGHT);
    eog_utils_set_sensitive_widgets (window, TRUE);
}

static void
cancel (EogCommentToolbar *comment)
{
    EogCommentToolbarPrivate *priv = comment->priv;
    EogWindow *window = priv->window;

    EogImage *image = eog_window_get_image(window);
    if(image == NULL)
        return;

    eog_thumb_view_select_single(EOG_THUMB_VIEW (eog_window_get_thumb_view(window)), EOG_THUMB_VIEW_SELECT_CURRENT);
    eog_image_modified(image);

    destroy_area(comment);
    return;
}

static void
button_comment_cancel_cb (GtkToolButton *toolbutton,
                       gpointer       data)
{
    g_return_if_fail(EOG_IS_COMMENT_TOOLBAR(data));

    cancel(EOG_COMMENT_TOOLBAR(data));
    return;
}

static GtkToolItem *
item_button_comment_cancel (EogCommentToolbar *toolbar)
{
    g_return_val_if_fail(EOG_IS_COMMENT_TOOLBAR(toolbar), NULL);

    GtkWidget *img_cancel = gtk_image_new_from_icon_name("edit-undo", GTK_ICON_SIZE_BUTTON);
    GtkToolItem *ss = gtk_tool_button_new(img_cancel, _("Cancel"));

    g_signal_connect(GTK_TOOL_BUTTON(ss), "clicked", G_CALLBACK(button_comment_cancel_cb), toolbar);

    return ss;
}

static void
job_filter_comment_finished_cb (EogJobCommentFilter *job, gpointer data)
{
    EogCommentToolbarPrivate *priv = EOG_COMMENT_TOOLBAR(data)->priv;
    GFile *file = eog_image_get_file (job->image);
    const gchar *path_file = g_file_get_parse_name(file);
    eog_utils_save_image (priv->window, job->pixbuf_output, g_file_get_parent(file), path_file, "Commented", TRUE);

    gtk_widget_hide (priv->area_progress);
    gtk_widget_set_sensitive(GTK_WIDGET(priv->stack), TRUE);
    eog_utils_set_sensitive_widgets (priv->window, TRUE);

    destroy_area(data);

    if (priv->input_text)
        g_string_free (priv->input_text, TRUE);
    return;
}

static void
button_comment_validate_cb (GtkToolButton *toolbutton,
                         gpointer       data)
{
    g_return_if_fail(EOG_IS_COMMENT_TOOLBAR(data));

    EogCommentToolbarPrivate *priv = EOG_COMMENT_TOOLBAR(data)->priv;
    EogWindow *window = priv->window;

    EogImage *image = eog_window_get_image(window);
    if(image == NULL)
        return;

    if (!priv->input_text->len) {
        destroy_area(data);
        button_comment_cancel_cb(toolbutton, data);
        return;
    }

    gtk_widget_show_all (priv->area_progress);
    if (gtk_widget_get_visible (priv->area_progress)) {
        EogScrollView *view = EOG_SCROLL_VIEW(eog_window_get_view(priv->window));
        gdouble zoom = eog_scroll_view_get_zoom(view);
        priv->font_size /= zoom;
        priv->pos_x = (priv->pos_x - priv->x) / zoom;
        priv->pos_y = (priv->pos_y - priv->y) / zoom;
        EogJob *job = eog_job_comment_filter_new(image, priv->input_text->str, priv->pos_x, priv->pos_y, priv->font_size);
        g_signal_connect (job, "finished", G_CALLBACK(job_filter_comment_finished_cb), data);
        eog_job_scheduler_add_job(job);
        g_object_unref(job);
    }
    return;
}

static GtkToolItem *
item_button_comment_validate (EogCommentToolbar *toolbar)
{
    g_return_val_if_fail(EOG_IS_COMMENT_TOOLBAR(toolbar), NULL);

    GtkWidget *img_plus = gtk_image_new_from_icon_name("edit-redo", GTK_ICON_SIZE_BUTTON);
    GtkToolItem *ss = gtk_tool_button_new(img_plus, _("Validate"));

    g_signal_connect(GTK_TOOL_BUTTON(ss), "clicked", G_CALLBACK(button_comment_validate_cb), toolbar);

    return ss;
}

static gpointer
save_comment (gpointer data)
{
    g_return_val_if_fail (EOG_IS_COMMENT_TOOLBAR (data), NULL);

    EogCommentToolbar *comment = EOG_COMMENT_TOOLBAR(data);
    EogCommentToolbarPrivate *priv = comment->priv;

    if (!priv->input_text->len) {
        destroy_area(comment);
        return NULL;
    }

    EogImage *image = eog_window_get_image(priv->window);
    GFile *file = eog_image_get_file(image);

    GdkPixbuf *pix = gdk_pixbuf_new_from_file(g_file_get_parse_name(file), NULL);
    pix = gdk_pixbuf_apply_embedded_orientation(pix);

    gint width = gdk_pixbuf_get_width (pix);
    gint height = gdk_pixbuf_get_height (pix);

    cairo_format_t format = (gdk_pixbuf_get_has_alpha (pix)) ? CAIRO_FORMAT_ARGB32 : CAIRO_FORMAT_RGB24;
    cairo_surface_t *surface = cairo_image_surface_create (format, width, height);
    cairo_t *cr = cairo_create(surface);
    gdk_cairo_set_source_pixbuf(cr, pix, 0, 0);
    cairo_paint(cr);

    EogScrollView *view = EOG_SCROLL_VIEW(eog_window_get_view(priv->window));
    gdouble zoom = eog_scroll_view_get_zoom(view);
    priv->font_size /= zoom;
    priv->pos_x = (priv->pos_x - priv->x) / zoom;
    priv->pos_y = (priv->pos_y - priv->y) / zoom;

    cairo_select_font_face(cr, "Helvetica", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
    cairo_set_font_size(cr, priv->font_size);
    draw_text(cr, priv->pos_x, priv->pos_y, priv->input_text->str);

    pix = gdk_pixbuf_get_from_surface(surface, 0, 0, width, height);

    GFile *filename = eog_image_get_file(image);
    const gchar *pathfile = g_file_get_parse_name(filename);
    eog_utils_save_image(priv->window, pix, g_file_get_parent(filename), pathfile, "Commented", TRUE);

    g_object_unref(pix);
    g_object_unref(filename);

    destroy_area(comment);

    if (priv->input_text)
        g_string_free (priv->input_text, TRUE);

    cairo_surface_destroy (surface);
    cairo_destroy (cr);
    return NULL;
}

static void
eog_comment_toolbar_init (EogCommentToolbar *m_toolbars)
{
    m_toolbars->priv = eog_comment_toolbar_get_instance_private(m_toolbars);

    gtk_toolbar_set_style(GTK_TOOLBAR(m_toolbars), GTK_TOOLBAR_BOTH);
    gtk_toolbar_insert(GTK_TOOLBAR(m_toolbars), item_button_comment_cancel (m_toolbars), 0);
    gtk_toolbar_insert(GTK_TOOLBAR(m_toolbars), item_button_comment_validate (m_toolbars), 1);
}

GtkWidget *
eog_comment_toolbar_new (EogWindow *window, GtkStack *stack, GtkWidget *area_progress)
{
    return g_object_new (EOG_TYPE_COMMENT_TOOLBAR, "window", window, "stack", stack, "area-progress", area_progress, NULL);
}

