/* EogToolbar - Plugin.

   Copyright (C) 2019 Yahiaoui Fakhri

   This file is part of the plugin for Eog.

   EogToolbar is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License as published by the Free
   Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   EogToolbar is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
   for more details.

   You should have received a copy of the GNU General Public License
   along with EogImagesManager; see the file COPYING.  If not, write to the Free
   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   This file implements a toolbar for eog.  */

#ifndef EOG_COMMENT_H
#define EOG_COMMENT_H

#include <gtk/gtk.h>
#include <eog-3.0/eog/eog-image.h>
G_BEGIN_DECLS

/*
 * Type checking and casting macros
 */
#define EOG_TYPE_COMMENT_TOOLBAR		(eog_comment_toolbar_get_type ())
#define EOG_COMMENT_TOOLBAR(o)		(G_TYPE_CHECK_INSTANCE_CAST ((o), EOG_TYPE_COMMENT_TOOLBAR, EogCommentToolbar))
#define EOG_COMMENT_TOOLBAR_CLASS(k)		G_TYPE_CHECK_CLASS_CAST((k),      EOG_TYPE_COMMENT_TOOLBAR, EogCommentToolbarClass))
#define EOG_IS_COMMENT_TOOLBAR(o)	        (G_TYPE_CHECK_INSTANCE_TYPE ((o), EOG_TYPE_COMMENT_TOOLBAR))
#define EOG_IS_COMMENT_TOOLBAR_CLASS(k)	(G_TYPE_CHECK_CLASS_TYPE ((k),    EOG_TYPE_COMMENT_TOOLBAR))
#define EOG_COMMENT_TOOLBAR_GET_CLASS(o)	(G_TYPE_INSTANCE_GET_CLASS ((o),  EOG_TYPE_COMMENT_TOOLBAR, EogCommentToolbarClass))

typedef struct _EogCommentToolbar EogCommentToolbar;
typedef struct _EogCommentToolbarClass _EogCommentToolbarClass;
typedef struct _EogCommentToolbarPrivate EogCommentToolbarPrivate;

struct _EogCommentToolbar
{
    GtkToolbar parent_instance;

    EogCommentToolbarPrivate *priv;
};

/*
 * Class definition
 */
typedef struct _EogCommentToolbarClass	EogCommentToolbarClass;

struct _EogCommentToolbarClass
{
    GtkToolbarClass parent_class;
};

/*
 * Public methods
 */
GType	eog_comment_toolbar_get_type		(void) G_GNUC_CONST;

GtkWidget *eog_comment_toolbar_new (EogWindow *window, GtkStack *stack, GtkWidget *area_progress);
void eog_comment_toolbar_init_area (EogCommentToolbar *comment);
G_END_DECLS

#endif //EOG_COMMENT_H

