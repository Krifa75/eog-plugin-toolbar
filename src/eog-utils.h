/* EogToolbar - Plugin.

   Copyright (C) 2019 Yahiaoui Fakhri

   This file is part of the plugin for Eog.

   EogToolbar is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License as published by the Free
   Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   EogToolbar is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
   for more details.

   You should have received a copy of the GNU General Public License
   along with EogImagesManager; see the file COPYING.  If not, write to the Free
   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   This file implements a toolbar for eog.  */

#ifndef EOG_PLUGIN_TOOLBAR_UTILS_H
#define EOG_PLUGIN_TOOLBAR_UTILS_H

#include <gtk/gtk.h>
#include <eog-3.0/eog/eog-window.h>

G_BEGIN_DECLS

G_GNUC_INTERNAL
const gchar *eog_utils_get_new_filename (const gchar *path_name,
                                               gchar *filter_name);

G_GNUC_INTERNAL
guint eog_utils_get_orientation_exif (const gchar *path);

G_GNUC_INTERNAL
void eog_utils_save_image (EogWindow *window, GdkPixbuf *pix,
                           GFile *parent, const gchar *path_name,
                           gchar *filter_name, gboolean reset_exif);

G_GNUC_INTERNAL
GtkWidget *eog_utils_get_widget_by_name (GtkWidget *parent,
                                         const gchar *name);

G_GNUC_INTERNAL
void eog_utils_set_sensitive_widgets (EogWindow *window,
                                      gboolean is_sensitive);

G_GNUC_INTERNAL
void eog_utils_set_mode_multiple_rows (EogWindow *window);

G_GNUC_INTERNAL
void eog_utils_set_mode_single_row (EogWindow *window);

G_GNUC_INTERNAL
gchar *eog_util_format_date (const gchar *date);

G_GNUC_INTERNAL
void HSBTransform(const double hue,const double saturation,
                  const double brightness, unsigned char *red,
                  unsigned char *green, unsigned char *blue);

G_GNUC_INTERNAL
void TransformHSB(const unsigned char red, const unsigned char green,
                  const unsigned char blue, double *hue,
                  double *saturation, double *brightness);

G_GNUC_INTERNAL
void CompositeMultiply(guint8 *p, double alpha,guint8 *q, double beta);

G_GNUC_INTERNAL
void ModulateRGBHSB (const double phue, const double psaturation, const double pbrightness,
                     unsigned char *red,unsigned char *green,unsigned char *blue);

G_END_DECLS

#endif //EOG_PLUGIN_TOOLBAR_UTILS_H
