/* EogToolbar - Plugin.

   Copyright (C) 2019 Yahiaoui Fakhri

   This file is part of the plugin for Eog.

   EogToolbar is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License as published by the Free
   Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   EogToolbar is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
   for more details.

   You should have received a copy of the GNU General Public License
   along with EogImagesManager; see the file COPYING.  If not, write to the Free
   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   This file implements a toolbar for eog.  */

#ifndef EOG_RED_EYES_H
#define EOG_RED_EYES_H

#include <gtk/gtk.h>
#include <eog-3.0/eog/eog-image.h>
G_BEGIN_DECLS

/*
 * Type checking and casting macros
 */
#define EOG_TYPE_RED_EYES_TOOLBAR		(eog_red_eyes_toolbar_get_type ())
#define EOG_RED_EYES_TOOLBAR(o)		(G_TYPE_CHECK_INSTANCE_CAST ((o), EOG_TYPE_RED_EYES_TOOLBAR, EogRedEyesToolbar))
#define EOG_RED_EYES_TOOLBAR_CLASS(k)		G_TYPE_CHECK_CLASS_CAST((k),      EOG_TYPE_RED_EYES_TOOLBAR, EogRedEyesToolbarClass))
#define EOG_IS_RED_EYES_TOOLBAR(o)	        (G_TYPE_CHECK_INSTANCE_TYPE ((o), EOG_TYPE_RED_EYES_TOOLBAR))
#define EOG_IS_RED_EYES_TOOLBAR_CLASS(k)	(G_TYPE_CHECK_CLASS_TYPE ((k),    EOG_TYPE_RED_EYES_TOOLBAR))
#define EOG_RED_EYES_TOOLBAR_GET_CLASS(o)	(G_TYPE_INSTANCE_GET_CLASS ((o),  EOG_TYPE_RED_EYES_TOOLBAR, EogRedEyesToolbarClass))

typedef struct _EogRedEyesToolbar EogRedEyesToolbar;
typedef struct _EogRedEyesToolbarClass _EogRedEyesToolbarClass;
typedef struct _EogRedEyesToolbarPrivate EogRedEyesToolbarPrivate;

struct _EogRedEyesToolbar
{
    GtkToolbar parent_instance;

    EogRedEyesToolbarPrivate *priv;
};

/*
 * Class definition
 */
typedef struct _EogRedEyesToolbarClass	EogRedEyesToolbarClass;

struct _EogRedEyesToolbarClass
{
    GtkToolbarClass parent_class;
};

/*
 * Public methods
 */
GType	eog_red_eyes_toolbar_get_type		(void) G_GNUC_CONST;

GtkWidget *eog_red_eyes_toolbar_new (EogWindow *window, GtkStack *stack, GtkWidget *area_progress);
void eog_red_eyes_toolbar_init_area (GtkWidget *red_eyes);
G_END_DECLS

#endif //EOG_RED_EYES_H
