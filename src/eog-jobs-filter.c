//
// Created by krifa on 13/07/2020.
//

#include "eog-jobs-filter.h"
#include "eog-utils.h"
#include "utils/exif-jpg.h"
#include "utils/redeyes-filter.h"
#include <eog-3.0/eog/eog-list-store.h>

G_DEFINE_TYPE (EogJobCropFilter,          eog_job_crop_filter,          EOG_TYPE_JOB);
G_DEFINE_TYPE (EogJobCommentFilter,       eog_job_comment_filter,       EOG_TYPE_JOB);
G_DEFINE_TYPE (EogJobReduceFilter,        eog_job_reduce_filter,        EOG_TYPE_JOB);
G_DEFINE_TYPE (EogJobBrightnessFilter,    eog_job_brightness_filter,    EOG_TYPE_JOB);
G_DEFINE_TYPE (EogJobContrastFilter,      eog_job_contrast_filter,      EOG_TYPE_JOB);
G_DEFINE_TYPE (EogJobRedEyesFilter,       eog_job_red_eyes_filter,      EOG_TYPE_JOB);
G_DEFINE_TYPE (EogJobGrayscaleFilter,     eog_job_grayscale_filter,     EOG_TYPE_JOB);
G_DEFINE_TYPE (EogJobSepiaFilter,         eog_job_sepia_filter,         EOG_TYPE_JOB);

static void     eog_job_crop_filter_class_init     (EogJobCropFilterClass     *class);
static void     eog_job_crop_filter_init           (EogJobCropFilter          *job);
static void     eog_job_crop_filter_dispose        (GObject                   *object);

static void     eog_job_comment_filter_class_init     (EogJobCommentFilterClass     *class);
static void     eog_job_comment_filter_init           (EogJobCommentFilter          *job);
static void     eog_job_comment_filter_dispose        (GObject                   *object);

static void     eog_job_reduce_filter_class_init     (EogJobReduceFilterClass     *class);
static void     eog_job_reduce_filter_init           (EogJobReduceFilter          *job);
static void     eog_job_reduce_filter_dispose        (GObject                   *object);

static void     eog_job_brightness_filter_class_init     (EogJobBrightnessFilterClass     *class);
static void     eog_job_brightness_filter_init           (EogJobBrightnessFilter          *job);
static void     eog_job_brightness_filter_dispose        (GObject                   *object);

static void     eog_job_contrast_filter_class_init     (EogJobContrastFilterClass     *class);
static void     eog_job_contrast_filter_init           (EogJobContrastFilter          *job);
static void     eog_job_contrast_filter_dispose        (GObject                   *object);

static void     eog_job_red_eyes_filter_class_init     (EogJobRedEyesFilterClass     *class);
static void     eog_job_red_eyes_filter_init           (EogJobRedEyesFilter          *job);
static void     eog_job_red_eyes_filter_dispose        (GObject                   *object);

static void     eog_job_grayscale_filter_class_init     (EogJobGrayscaleFilterClass     *class);
static void     eog_job_grayscale_filter_init           (EogJobGrayscaleFilter          *job);
static void     eog_job_grayscale_filter_dispose        (GObject                   *object);

static void     eog_job_sepia_filter_class_init     (EogJobSepiaFilterClass     *class);
static void     eog_job_sepia_filter_init           (EogJobSepiaFilter          *job);
static void     eog_job_sepia_filter_dispose        (GObject                   *object);

static void eog_job_crop_filter_run (EogJob *job);
static void eog_job_comment_filter_run (EogJob *job);
static void eog_job_reduce_filter_run (EogJob *job);
static void eog_job_brightness_filter_run (EogJob *job);
static void eog_job_contrast_filter_run (EogJob *job);
static void eog_job_red_eyes_filter_run (EogJob *job);
static void eog_job_grayscale_filter_run (EogJob *job);
static void eog_job_sepia_filter_run (EogJob *job);

static gboolean
notify_finished (EogJob *job)
{
    /* get finished signal */
    guint signal_finished = g_signal_lookup("finished", EOG_TYPE_JOB);

    /* notify job finalization */
    g_signal_emit (job,
                   signal_finished,
                   0);
    return FALSE;
}

static void
eog_job_crop_filter_class_init (EogJobCropFilterClass *class)
{
    GObjectClass *g_object_class = (GObjectClass *) class;
    EogJobClass  *eog_job_class  = (EogJobClass *)  class;

    g_object_class->dispose = eog_job_crop_filter_dispose;
    eog_job_class->run      = eog_job_crop_filter_run;
}

static
void eog_job_crop_filter_init (EogJobCropFilter *job)
{
    /* initialize all public and private members to reasonable
       default values. */
}

static
void eog_job_crop_filter_dispose (GObject *object)
{
    EogJobCropFilter *job;

    g_return_if_fail (EOG_IS_JOB_CROP_FILTER (object));

    job = EOG_JOB_CROP_FILTER (object);

    if (job->image)
        g_object_unref (job->image);
    /* call parent dispose */
    G_OBJECT_CLASS (eog_job_crop_filter_parent_class)->dispose (object);
}

static void
eog_job_crop_filter_run (EogJob *job)
{
    EogJobCropFilter *job_filter;

    /* initialization */
    g_return_if_fail (EOG_IS_JOB_CROP_FILTER (job));

    job_filter     = EOG_JOB_CROP_FILTER (g_object_ref (job));

    /* --- enter critical section --- */
//    g_mutex_lock (job->mutex);

// Not necessary, we save a subpixbuf

    /* --- leave critical section --- */
//    g_mutex_unlock (job->mutex);

    /* --- enter critical section --- */
    g_mutex_lock (job->mutex);

    /* job finished */
    job->finished = TRUE;

    /* --- leave critical section --- */
    g_mutex_unlock (job->mutex);

    /* notify job finalization */
    g_idle_add_full (G_PRIORITY_DEFAULT_IDLE,
                     (GSourceFunc) notify_finished,
                     job,
                     g_object_unref);
    return;
}

EogJob *
eog_job_crop_filter_new (EogImage *image, GSList *rect)
{
//    g_return_val_if_fail(EOG_IS_LIST_STORE(store), NULL);

    EogJobCropFilter *job;

    job = g_object_new (EOG_TYPE_JOB_CROP_FILTER, NULL);
    job->image = g_object_ref (image);
    job->rect = rect;

    return EOG_JOB (job);
}

static void
eog_job_comment_filter_class_init (EogJobCommentFilterClass *class)
{
    GObjectClass *g_object_class = (GObjectClass *) class;
    EogJobClass  *eog_job_class  = (EogJobClass *)  class;

    g_object_class->dispose = eog_job_comment_filter_dispose;
    eog_job_class->run      = eog_job_comment_filter_run;
}

static
void eog_job_comment_filter_init (EogJobCommentFilter *job)
{
    /* initialize all public and private members to reasonable
       default values. */
}

static
void eog_job_comment_filter_dispose (GObject *object)
{
    EogJobCommentFilter *job;

    g_return_if_fail (EOG_IS_JOB_COMMENT_FILTER (object));

    job = EOG_JOB_COMMENT_FILTER (object);

    if (job->image)
        g_object_unref (job->image);

    if (job->text)
        g_free (job->text);
    /* call parent dispose */
    G_OBJECT_CLASS (eog_job_comment_filter_parent_class)->dispose (object);
}

static void
draw_text (cairo_t *cr, gint pos_x, gint pos_y, const gchar *text)
{
    cairo_move_to(cr, pos_x, pos_y);
    cairo_set_source_rgb(cr, 0, 0, 0);
    cairo_show_text(cr, text);

    cairo_move_to(cr, pos_x-2, pos_y-2);
    cairo_set_source_rgb(cr, 1, 1, 1);
    cairo_show_text(cr, text);

    return;
}

static void
eog_job_comment_filter_run (EogJob *job)
{
    EogJobCommentFilter *job_filter;

    /* initialization */
    g_return_if_fail (EOG_IS_JOB_COMMENT_FILTER (job));

    job_filter     = EOG_JOB_COMMENT_FILTER (g_object_ref (job));

    GFile *file = eog_image_get_file(job_filter->image);

    job_filter->pixbuf_output = gdk_pixbuf_new_from_file(g_file_get_parse_name(file), NULL);
    job_filter->pixbuf_output = gdk_pixbuf_apply_embedded_orientation(job_filter->pixbuf_output);

    gint width = gdk_pixbuf_get_width (job_filter->pixbuf_output);
    gint height = gdk_pixbuf_get_height (job_filter->pixbuf_output);

    cairo_format_t format = (gdk_pixbuf_get_has_alpha (job_filter->pixbuf_output)) ? CAIRO_FORMAT_ARGB32 : CAIRO_FORMAT_RGB24;
    cairo_surface_t *surface = cairo_image_surface_create (format, width, height);
    cairo_t *cr = cairo_create(surface);
    gdk_cairo_set_source_pixbuf(cr, job_filter->pixbuf_output, 0, 0);
    cairo_paint(cr);

    cairo_select_font_face(cr, "Helvetica", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
    cairo_set_font_size(cr, job_filter->font_size);

    //Pointer function instead ?
    draw_text(cr, job_filter->pos_x, job_filter->pos_y, job_filter->text);

    job_filter->pixbuf_output = gdk_pixbuf_get_from_surface(surface, 0, 0, width, height);

    cairo_surface_destroy (surface);
    cairo_destroy (cr);

    /* --- enter critical section --- */
    g_mutex_lock (job->mutex);

    g_object_unref(file);
    /* job finished */
    job->finished = TRUE;

    /* --- leave critical section --- */
    g_mutex_unlock (job->mutex);

    /* notify job finalization */
    g_idle_add_full (G_PRIORITY_DEFAULT_IDLE,
                     (GSourceFunc) notify_finished,
                     job,
                     g_object_unref);
    return;
}

EogJob *
eog_job_comment_filter_new (EogImage *image, gchar *text, gint pos_x, gint pos_y, gint font_size)
{
    EogJobCommentFilter *job;

    job = g_object_new (EOG_TYPE_JOB_COMMENT_FILTER, NULL);
    job->image = g_object_ref (image);
    job->text = g_strdup (text);
    job->pos_x = pos_x;
    job->pos_y = pos_y;
    job->font_size = font_size;

    return EOG_JOB (job);
}

static void
eog_job_reduce_filter_class_init (EogJobReduceFilterClass *class)
{
    GObjectClass *g_object_class = (GObjectClass *) class;
    EogJobClass  *eog_job_class  = (EogJobClass *)  class;

    g_object_class->dispose = eog_job_reduce_filter_dispose;
    eog_job_class->run      = eog_job_reduce_filter_run;
}

static
void eog_job_reduce_filter_init (EogJobReduceFilter *job)
{
    /* initialize all public and private members to reasonable
       default values. */
}

static
void eog_job_reduce_filter_dispose (GObject *object)
{
    EogJobReduceFilter *job;

    g_return_if_fail (EOG_IS_JOB_REDUCE_FILTER (object));

    job = EOG_JOB_REDUCE_FILTER (object);

    if (job->image)
        g_object_unref (job->image);
    /* call parent dispose */
    G_OBJECT_CLASS (eog_job_reduce_filter_parent_class)->dispose (object);
}

static void
eog_job_reduce_filter_run (EogJob *job)
{
    EogJobReduceFilter *job_filter;

    /* initialization */
    g_return_if_fail (EOG_IS_JOB_REDUCE_FILTER (job));

    job_filter     = EOG_JOB_REDUCE_FILTER (g_object_ref (job));

    /* --- enter critical section --- */
//    g_mutex_lock (job->mutex);

// HERE THE CODE

    /* --- leave critical section --- */
//    g_mutex_unlock (job->mutex);

    /* --- enter critical section --- */
    g_mutex_lock (job->mutex);

    /* job finished */
    job->finished = TRUE;

    /* --- leave critical section --- */
    g_mutex_unlock (job->mutex);

    /* notify job finalization */
    g_idle_add_full (G_PRIORITY_DEFAULT_IDLE,
                     (GSourceFunc) notify_finished,
                     job,
                     g_object_unref);
    return;
}

EogJob *
eog_job_reduce_filter_new (EogImage *image, guint width, guint height)
{
    EogJobReduceFilter *job;

    job = g_object_new (EOG_TYPE_JOB_REDUCE_FILTER, NULL);
    job->image = g_object_ref (image);
    job->width  = width;
    job->height = height;

    return EOG_JOB (job);
}

static void
eog_job_brightness_filter_class_init (EogJobBrightnessFilterClass *class)
{
    GObjectClass *g_object_class = (GObjectClass *) class;
    EogJobClass  *eog_job_class  = (EogJobClass *)  class;

    g_object_class->dispose = eog_job_brightness_filter_dispose;
    eog_job_class->run      = eog_job_brightness_filter_run;
}

static
void eog_job_brightness_filter_init (EogJobBrightnessFilter *job)
{
    /* initialize all public and private members to reasonable
       default values. */
}

static
void eog_job_brightness_filter_dispose (GObject *object)
{
    EogJobBrightnessFilter *job;

    g_return_if_fail (EOG_IS_JOB_BRIGHTNESS_FILTER (object));

    job = EOG_JOB_BRIGHTNESS_FILTER (object);

    if (job->image)
        g_object_unref (job->image);

    if (job->pixbuf_original)
        g_object_unref (job->pixbuf_original);

    if (job->pixbuf_output)
        g_object_unref (job->pixbuf_output);
    /* call parent dispose */
    G_OBJECT_CLASS (eog_job_brightness_filter_parent_class)->dispose (object);
}

static void
reset_pixbuf_image (GdkPixbuf *pix, const guchar *pixels)
{
    int i, size = gdk_pixbuf_get_height(pix) * gdk_pixbuf_get_rowstride (pix);
    guchar *data = gdk_pixbuf_get_pixels (pix);

    for (i=0; i < size; i++)
        data[i] = pixels[i];

    return;
}

static void
eog_job_brightness_filter_run (EogJob *job)
{
    EogJobBrightnessFilter *job_filter;

    /* initialization */
    g_return_if_fail (EOG_IS_JOB_BRIGHTNESS_FILTER (job));

    job_filter     = EOG_JOB_BRIGHTNESS_FILTER (g_object_ref (job));

    job_filter->pixbuf_output = eog_image_get_pixbuf(job_filter->image);
    reset_pixbuf_image(job_filter->pixbuf_output, gdk_pixbuf_get_pixels (job_filter->pixbuf_original));

    GFile *filename = eog_image_get_file(job_filter->image);

//    GdkPixbuf *pix = eog_image_get_pixbuf(job_filter->image);//gdk_pixbuf_new_from_file(g_file_get_parse_name(filename), NULL);

    double	Table[256];
    int i, size = gdk_pixbuf_get_height(job_filter->pixbuf_output) * gdk_pixbuf_get_rowstride (job_filter->pixbuf_output);
    guchar *data = gdk_pixbuf_get_pixels (job_filter->pixbuf_output);

    for (i = 0; i < 256; i++)
        Table[i] = (double)pow((double)i / 255.0, 1.0 / job_filter->brightness_value);

    // alpha blend images
    for (i=0; i < size; i++)
        data[i] = (unsigned char)(Table[data[i]] * UCHAR_MAX);

    /* --- enter critical section --- */
    g_mutex_lock (job->mutex);

    g_object_unref (filename);
    /* job finished */
    job->finished = TRUE;

    /* --- leave critical section --- */
    g_mutex_unlock (job->mutex);

    /* notify job finalization */
    g_idle_add_full (G_PRIORITY_DEFAULT_IDLE,
                     (GSourceFunc) notify_finished,
                     job,
                     g_object_unref);
    return;
}

EogJob *
eog_job_brightness_filter_new (EogImage *image, GdkPixbuf *pixbuf_original, gdouble brightness_value)
{
    EogJobBrightnessFilter *job;

    job = g_object_new (EOG_TYPE_JOB_BRIGHTNESS_FILTER, NULL);
    job->image = g_object_ref (image);
    job->pixbuf_original = g_object_ref (pixbuf_original);
    job->brightness_value  = brightness_value;

    return EOG_JOB (job);
}

static void
eog_job_contrast_filter_class_init (EogJobContrastFilterClass *class)
{
    GObjectClass *g_object_class = (GObjectClass *) class;
    EogJobClass  *eog_job_class  = (EogJobClass *)  class;

    g_object_class->dispose = eog_job_contrast_filter_dispose;
    eog_job_class->run      = eog_job_contrast_filter_run;
}

static
void eog_job_contrast_filter_init (EogJobContrastFilter *job)
{
    /* initialize all public and private members to reasonable
       default values. */
}

static
void eog_job_contrast_filter_dispose (GObject *object)
{
    EogJobContrastFilter *job;

    g_return_if_fail (EOG_IS_JOB_CONTRAST_FILTER (object));

    job = EOG_JOB_CONTRAST_FILTER (object);

    if (job->image)
        g_object_unref (job->image);

    if (job->pixbuf_output)
        g_object_unref (job->pixbuf_output);
    /* call parent dispose */
    G_OBJECT_CLASS (eog_job_contrast_filter_parent_class)->dispose (object);
}

static void
Contrast (const int sign,unsigned char* red,unsigned char* green, unsigned char* blue)
{
    double brightness, hue, saturation;

    /*
      Enhance contrast: dark color become darker, light color become lighter.
    */
    hue=0.0;
    saturation=0.0;
    brightness=0.0;

    TransformHSB(*red,*green,*blue,&hue,&saturation,&brightness);

    brightness+=0.5*sign*(0.5*(sin(MagickPI*(brightness-0.5))+1.0)-brightness);

    if (brightness > 1.0)
        brightness=1.0;
    else if (brightness < 0.0)
        brightness=0.0;

    HSBTransform(hue,saturation,brightness,red,green,blue);
}

static void
eog_job_contrast_filter_run (EogJob *job)
{
    EogJobContrastFilter *job_filter;

    /* initialization */
    g_return_if_fail (EOG_IS_JOB_CONTRAST_FILTER (job));

    job_filter     = EOG_JOB_CONTRAST_FILTER (g_object_ref (job));

    GError *error = NULL;
    GFile *file = eog_image_get_file(job_filter->image);
    const gchar *pathfile = g_file_get_parse_name(file);
    job_filter->pixbuf_output = gdk_pixbuf_new_from_file(pathfile, &error);
    if (error){
        g_critical("%s : Could not load the image : %s\nError : %s\n", __func__, pathfile, error->message);
        g_clear_error(&error);
    }
    job_filter->pixbuf_output = gdk_pixbuf_apply_embedded_orientation(job_filter->pixbuf_output);
    long i = 0;
    int sign = 1;
    /*
        Contrast enhance image.
      */
    int Bpp = gdk_pixbuf_get_has_alpha(job_filter->pixbuf_output) ? 4 : 3;
    int size = gdk_pixbuf_get_width(job_filter->pixbuf_output) *
               gdk_pixbuf_get_height(job_filter->pixbuf_output) *
               Bpp;
    guchar *pixels = gdk_pixbuf_get_pixels (job_filter->pixbuf_output);
    for (; i < size; i += Bpp)
    {
        // Set pixel color to green with a transparency of 128
        Contrast(sign,&pixels[i + 2],&pixels[i + 1],&pixels[i]);
    }


    /* --- enter critical section --- */
    g_mutex_lock (job->mutex);

    g_object_unref (file);
    /* job finished */
    job->finished = TRUE;

    /* --- leave critical section --- */
    g_mutex_unlock (job->mutex);

    /* notify job finalization */
    g_idle_add_full (G_PRIORITY_DEFAULT_IDLE,
                     (GSourceFunc) notify_finished,
                     job,
                     g_object_unref);
    return;
}

EogJob *
eog_job_contrast_filter_new (EogImage *image)
{
    EogJobContrastFilter *job;

    job = g_object_new (EOG_TYPE_JOB_CONTRAST_FILTER, NULL);
    job->image = g_object_ref (image);

    return EOG_JOB (job);
}

static void
eog_job_red_eyes_filter_class_init (EogJobRedEyesFilterClass *class)
{
    GObjectClass *g_object_class = (GObjectClass *) class;
    EogJobClass  *eog_job_class  = (EogJobClass *)  class;

    g_object_class->dispose = eog_job_red_eyes_filter_dispose;
    eog_job_class->run      = eog_job_red_eyes_filter_run;
}

static
void eog_job_red_eyes_filter_init (EogJobRedEyesFilter *job)
{
    /* initialize all public and private members to reasonable
       default values. */
}

static
void eog_job_red_eyes_filter_dispose (GObject *object)
{
    EogJobRedEyesFilter *job;

    g_return_if_fail (EOG_IS_JOB_RED_EYES_FILTER (object));

    job = EOG_JOB_RED_EYES_FILTER (object);

    if (job->image)
        g_object_unref (job->image);

    if (job->pixbuf_output)
        g_object_unref (job->pixbuf_output);

    /* call parent dispose */
    G_OBJECT_CLASS (eog_job_red_eyes_filter_parent_class)->dispose (object);
}

static void
eog_job_red_eyes_filter_run (EogJob *job)
{
    EogJobRedEyesFilter *job_filter;

    /* initialization */
    g_return_if_fail (EOG_IS_JOB_RED_EYES_FILTER (job));

    job_filter     = EOG_JOB_RED_EYES_FILTER (g_object_ref (job));

    GFile *filename = eog_image_get_file(job_filter->image);
    GError *error = NULL;
    job_filter->pixbuf_output = gdk_pixbuf_new_from_file(g_file_get_parse_name(filename), &error);
    if (error){
        g_critical("(%s) : Cannot  open the file %s.\n Error message : %s", __func__, g_file_get_parse_name(filename), error ? error->message : "No message");
        g_clear_error(&error);
    }
    job_filter->pixbuf_output = gdk_pixbuf_apply_embedded_orientation(job_filter->pixbuf_output);

    image_red_eyes(job_filter->pixbuf_output, job_filter->rects);

    /* --- enter critical section --- */
    g_mutex_lock (job->mutex);

    g_object_unref (filename);

    /* job finished */
    job->finished = TRUE;

    /* --- leave critical section --- */
    g_mutex_unlock (job->mutex);

    /* notify job finalization */
    g_idle_add_full (G_PRIORITY_DEFAULT_IDLE,
                     (GSourceFunc) notify_finished,
                     job,
                     g_object_unref);
    return;
}

EogJob *
eog_job_red_eyes_filter_new (EogImage *image, GSList *rects)
{
    EogJobRedEyesFilter *job;

    job = g_object_new (EOG_TYPE_JOB_RED_EYES_FILTER, NULL);
    job->image = g_object_ref (image);
    job->rects  = rects;

    return EOG_JOB (job);
}


static void
eog_job_grayscale_filter_class_init (EogJobGrayscaleFilterClass *class)
{
    GObjectClass *g_object_class = (GObjectClass *) class;
    EogJobClass  *eog_job_class  = (EogJobClass *)  class;

    g_object_class->dispose = eog_job_grayscale_filter_dispose;
    eog_job_class->run      = eog_job_grayscale_filter_run;
}

static
void eog_job_grayscale_filter_init (EogJobGrayscaleFilter *job)
{
    /* initialize all public and private members to reasonable
       default values. */
}

static
void eog_job_grayscale_filter_dispose (GObject *object)
{
    EogJobGrayscaleFilter *job;

    g_return_if_fail (EOG_IS_JOB_GRAYSCALE_FILTER (object));

    job = EOG_JOB_GRAYSCALE_FILTER (object);

    if (job->image)
        g_object_unref (job->image);

    if (job->pixbuf_output)
        g_object_unref (job->pixbuf_output);

    /* call parent dispose */
    G_OBJECT_CLASS (eog_job_grayscale_filter_parent_class)->dispose (object);
}

static void
eog_job_grayscale_filter_run (EogJob *job)
{
    EogJobGrayscaleFilter *job_filter;

    /* initialization */
    g_return_if_fail (EOG_IS_JOB_GRAYSCALE_FILTER (job));

    job_filter     = EOG_JOB_GRAYSCALE_FILTER (g_object_ref (job));

    GError *error = NULL;
    GFile *file = eog_image_get_file(job_filter->image);
    const gchar *pathfile = g_file_get_parse_name(file);
    job_filter->pixbuf_output = gdk_pixbuf_new_from_file(pathfile, &error);
    job_filter->pixbuf_output = gdk_pixbuf_apply_embedded_orientation(job_filter->pixbuf_output);

    if (error){
        g_critical("%s : Could not load the image : %s\nError : %s\n", __func__, pathfile, error->message);
        g_clear_error(&error);
    }

    long i;
    int Bpp = gdk_pixbuf_get_has_alpha(job_filter->pixbuf_output) ? 4 : 3;
    int size = gdk_pixbuf_get_width(job_filter->pixbuf_output) *
               gdk_pixbuf_get_height(job_filter->pixbuf_output) *
               Bpp;
    guchar *pixels = gdk_pixbuf_get_pixels (job_filter->pixbuf_output);
    for (i = 0; i < size; i += Bpp)
    {
        // Set pixel color to green with a transparency of 128
        double r = (double)pixels[i + 2];
        double g = (double)pixels[i + 1];
        double b = (double)pixels[i + 0];
        double rn = r * 0.3 + g * 0.59 + b * 0.11;
        double gn = r * 0.3 + g * 0.59 + b * 0.11;
        double bn = r * 0.3 + g * 0.59 + b * 0.11;
        pixels[i + 2] = (unsigned char)rn;
        pixels[i + 1] = (unsigned char)gn;
        pixels[i + 0] = (unsigned char)bn;
    }

    /* --- enter critical section --- */
    g_mutex_lock (job->mutex);

    g_object_unref (file);
    /* job finished */
    job->finished = TRUE;

    /* --- leave critical section --- */
    g_mutex_unlock (job->mutex);

    /* notify job finalization */
    g_idle_add_full (G_PRIORITY_DEFAULT_IDLE,
                     (GSourceFunc) notify_finished,
                     job,
                     g_object_unref);
    return;
}

EogJob *
eog_job_grayscale_filter_new (EogImage *image)
{
    EogJobGrayscaleFilter *job;

    job = g_object_new (EOG_TYPE_JOB_GRAYSCALE_FILTER, NULL);
    job->image = g_object_ref (image);

    return EOG_JOB (job);
}

static void
eog_job_sepia_filter_class_init (EogJobSepiaFilterClass *class)
{
    GObjectClass *g_object_class = (GObjectClass *) class;
    EogJobClass  *eog_job_class  = (EogJobClass *)  class;

    g_object_class->dispose = eog_job_sepia_filter_dispose;
    eog_job_class->run      = eog_job_sepia_filter_run;
}

static
void eog_job_sepia_filter_init (EogJobSepiaFilter *job)
{
    /* initialize all public and private members to reasonable
       default values. */
}

static
void eog_job_sepia_filter_dispose (GObject *object)
{
    EogJobSepiaFilter *job;

    g_return_if_fail (EOG_IS_JOB_SEPIA_FILTER (object));

    job = EOG_JOB_SEPIA_FILTER (object);

    if (job->image)
        g_object_unref (job->image);

    if (job->pixbuf_output)
        g_object_unref (job->pixbuf_output);

    /* call parent dispose */
    G_OBJECT_CLASS (eog_job_sepia_filter_parent_class)->dispose (object);
}

static void
eog_job_sepia_filter_run (EogJob *job)
{
    EogJobSepiaFilter *job_filter;

    /* initialization */
    g_return_if_fail (EOG_IS_JOB_SEPIA_FILTER (job));

    job_filter     = EOG_JOB_SEPIA_FILTER (g_object_ref (job));

    GError *error = NULL;

    GFile *file = eog_image_get_file(job_filter->image);
    const gchar *path_file = g_file_get_parse_name(file);
    GdkPixbuf *pix = gdk_pixbuf_new_from_file(path_file, &error);
    pix = gdk_pixbuf_apply_embedded_orientation(pix);
    if (error || !pix) {
        g_critical("%s : Could not load the image : %s\nError : %s\n", __func__, path_file, error->message);
        g_clear_error(&error);
    }

    job_filter->pixbuf_output = gdk_pixbuf_add_alpha(pix, FALSE, 0, 0, 0);
    if(!gdk_pixbuf_get_has_alpha(pix)) { // if not alpha add it in the original pixbuf
        g_object_unref(pix);
        pix = gdk_pixbuf_copy(job_filter->pixbuf_output); // for adding alpha
    }

    register long i, j, n_channels, rowstride;
    guchar *pixels, *blackwhites, *p, *pp;

    rowstride = gdk_pixbuf_get_rowstride (pix);
    n_channels = gdk_pixbuf_get_n_channels (pix);
    pixels = gdk_pixbuf_get_pixels (pix);

    blackwhites = gdk_pixbuf_get_pixels (job_filter->pixbuf_output);

    int w = gdk_pixbuf_get_width(pix);
    int h = gdk_pixbuf_get_height(pix);

    const int hue_sepia = 55;
    const int saturation_sepia = 80;
    const int light_sepia = 100;

    for (i = 0; i < w; i++){
        for (j = 0; j < h; j++){
            p = pixels + j * rowstride + i * n_channels;
            pp = blackwhites + j * rowstride + i * n_channels;
            p[1] = (unsigned char)((double)p[0] * 0.3 + (double)p[1] * 0.59 + (double)p[2] * 0.11);
            pp[0] = p[1];
            pp[1] = p[1];
            pp[2] = p[1];
            p[0] = p[1];
            p[2] = p[1];
            p[1] = p[1] != 0 ? 255 : 0;
            CompositeMultiply(p, 0.0, pp, 0.0);
            ModulateRGBHSB(hue_sepia, saturation_sepia, light_sepia, &pp[0],&pp[1],&pp[2]);
        }
    }

    if (pix)
        g_object_unref(pix);

    /* --- enter critical section --- */
    g_mutex_lock (job->mutex);

    g_object_unref (file);
    /* job finished */
    job->finished = TRUE;

    /* --- leave critical section --- */
    g_mutex_unlock (job->mutex);

    /* notify job finalization */
    g_idle_add_full (G_PRIORITY_DEFAULT_IDLE,
                     (GSourceFunc) notify_finished,
                     job,
                     g_object_unref);
    return;
}

EogJob *
eog_job_sepia_filter_new (EogImage *image)
{
    EogJobSepiaFilter *job;

    job = g_object_new (EOG_TYPE_JOB_SEPIA_FILTER, NULL);
    job->image = g_object_ref (image);

    return EOG_JOB (job);
}
