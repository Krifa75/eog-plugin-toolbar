/* EogToolbar - Plugin.

   Copyright (C) 2019 Yahiaoui Fakhri

   This file is part of the plugin for Eog.

   EogToolbar is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License as published by the Free
   Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   EogToolbar is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
   for more details.

   You should have received a copy of the GNU General Public License
   along with EogImagesManager; see the file COPYING.  If not, write to the Free
   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   This file implements a toolbar for eog.  */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <glib/gi18n.h>
#include <eog-3.0/eog/eog-scroll-view.h>
#include <eog-3.0/eog/eog-thumb-view.h>
#include <eog-3.0/eog/eog-thumb-nav.h>
#include <eog/eog-job-scheduler.h>
#include "eog-red-eyes-toolbar.h"
#include "eog-utils.h"
#include "utils/redeyes-filter.h"
#include "eog-selection-tool.h"
#include "eog-jobs-filter.h"

struct _EogRedEyesToolbarPrivate {
    EogWindow *window;
    GtkWidget *view;
    GtkStack *stack;
    GtkWidget *select;
    GtkWidget *area_progress;

    GSList *l_coords;

    GdkPixbuf *orig_pix;
};

G_DEFINE_TYPE_WITH_PRIVATE (EogRedEyesToolbar, eog_red_eyes_toolbar, GTK_TYPE_TOOLBAR)

enum {
    PROP_0,
    PROP_WINDOW,
    PROP_STACK,
    PROP_AREA,
};

static void
eog_red_eyes_toolbar_constructed (GObject *object)
{
    if (G_OBJECT_CLASS (eog_red_eyes_toolbar_parent_class)->constructed)
        G_OBJECT_CLASS (eog_red_eyes_toolbar_parent_class)->constructed (object);

    eog_red_eyes_toolbar_init_area (GTK_WIDGET(object));

    return;
}

static void
eog_red_eyes_toolbar_dispose (GObject *object)
{

    G_OBJECT_CLASS (eog_red_eyes_toolbar_parent_class)->dispose (object);
    return;
}

static void
eog_red_eyes_toolbar_get_property (GObject    *object,
                                     guint       prop_id,
                                     GValue     *value,
                                     GParamSpec *pspec)
{
    EogRedEyesToolbarPrivate *priv = EOG_RED_EYES_TOOLBAR(object)->priv;

    switch (prop_id){
        case PROP_WINDOW:
            g_value_set_object(value, priv->window);
            break;
        case PROP_STACK:
            g_value_set_object(value, priv->stack);
            break;
        case PROP_AREA:
            g_value_set_object(value, priv->area_progress);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
eog_red_eyes_toolbar_set_property (GObject    *object,
                                     guint       prop_id,
                                     const GValue     *value,
                                     GParamSpec *pspec)
{
    EogRedEyesToolbarPrivate *priv = EOG_RED_EYES_TOOLBAR(object)->priv;

    switch (prop_id){
        case PROP_WINDOW:
            priv->window = EOG_WINDOW(g_value_dup_object (value));
            break;
        case PROP_STACK:
            priv->stack = GTK_STACK(g_value_dup_object (value));
            break;
        case PROP_AREA:
            priv->area_progress = GTK_WIDGET(g_value_get_object (value));
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
eog_red_eyes_toolbar_class_init (EogRedEyesToolbarClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);

    object_class->constructed = eog_red_eyes_toolbar_constructed;
    object_class->dispose = eog_red_eyes_toolbar_dispose;
    object_class->set_property = eog_red_eyes_toolbar_set_property;
    object_class->get_property = eog_red_eyes_toolbar_get_property;

    g_object_class_install_property(object_class, PROP_WINDOW, g_param_spec_object("window", "", "", EOG_TYPE_WINDOW,
                                                                                   (G_PARAM_WRITABLE |
                                                                                    G_PARAM_CONSTRUCT_ONLY |
                                                                                    G_PARAM_STATIC_STRINGS))); //G_PARAM_READWRITE));
    g_object_class_install_property(object_class, PROP_STACK, g_param_spec_object("stack", "", "", GTK_TYPE_STACK,
                                                                                  (G_PARAM_WRITABLE |
                                                                                   G_PARAM_CONSTRUCT_ONLY |
                                                                                   G_PARAM_STATIC_STRINGS)));//G_PARAM_READWRITE));
    g_object_class_install_property(object_class, PROP_AREA, g_param_spec_object("area-progress", "", "", GTK_TYPE_DRAWING_AREA,
                                                                                 (G_PARAM_WRITABLE |
                                                                                  G_PARAM_CONSTRUCT_ONLY |
                                                                                  G_PARAM_STATIC_STRINGS)));
    bind_textdomain_codeset (ET_PACKAGE, "UTF-8");
    textdomain (ET_PACKAGE);
    return;
}

static gboolean
red_eyes_filter (EogSelection *sel, GtkWidget *view, GdkRectangle rect)
{

    int center_x = rect.x + rect.width / 2;
    int center_y = rect.y + rect.height / 2;

    int radius = rect.width < rect.height ? rect.width / 2 : rect.height / 2;

    EogImage *image = eog_scroll_view_get_image(EOG_SCROLL_VIEW(view));
    GdkPixbuf *pix = eog_image_get_pixbuf(image);
    pix = gdk_pixbuf_apply_embedded_orientation(pix);

    CoordRedEyes *coord = g_new0(CoordRedEyes, 1);
    coord->radius = radius;
    coord->center_x = center_x;
    coord->center_y = center_y;

    sel->l_coords = g_slist_append(sel->l_coords, coord);
    int ret = image_red_eyes(pix, g_slist_last(sel->l_coords));

    if (ret)
        eog_image_updated(image);
    return TRUE;
}

void
eog_red_eyes_toolbar_init_area (GtkWidget *red_eyes)
{
    g_return_if_fail(EOG_IS_RED_EYES_TOOLBAR(red_eyes));

    EogRedEyesToolbarPrivate *priv = EOG_RED_EYES_TOOLBAR(red_eyes)->priv;

    priv->view = eog_window_get_view(EOG_WINDOW(priv->window));
    g_assert (GTK_IS_GRID(priv->view));

    GtkWidget *overlay = gtk_grid_get_child_at(GTK_GRID(priv->view), 0, 0);
    g_assert(GTK_IS_OVERLAY(overlay));

    EogImage *current_image = eog_window_get_image(priv->window);
    priv->orig_pix = gdk_pixbuf_copy(eog_image_get_pixbuf(current_image));

    priv->select = eog_selection_new(priv->view);
    eog_selection_set_func_filter(priv->select, red_eyes_filter);

    gtk_overlay_add_overlay(GTK_OVERLAY(overlay), GTK_WIDGET(priv->select));
    gtk_widget_show_all(GTK_WIDGET(priv->select));

    return;
}

static void
destroy_area (EogRedEyesToolbar *toolbar) {
    EogRedEyesToolbarPrivate *priv = toolbar->priv;
    EogWindow *window = priv->window;

    if(priv->select) {
        GtkWidget *parent = gtk_widget_get_parent (priv->select);
        gtk_container_remove (GTK_CONTAINER (parent), priv->select);
        priv->select = NULL;
    }

    gtk_stack_set_visible_child_full(GTK_STACK(priv->stack), "main-toolbar", GTK_STACK_TRANSITION_TYPE_OVER_RIGHT);
    eog_utils_set_sensitive_widgets (window, TRUE);
}

static void
reset_pixbuf_image (GdkPixbuf *pix, const guchar *pixels)
{
    int i, size = gdk_pixbuf_get_height(pix) * gdk_pixbuf_get_rowstride (pix);
    guchar *data = gdk_pixbuf_get_pixels (pix);

    for (i=0; i < size; i++)
        data[i] = pixels[i];

    return;
}

static void
button_red_eyes_cancel_cb (GtkToolButton *toolbutton,
                             gpointer       data)
{
    g_return_if_fail(EOG_IS_RED_EYES_TOOLBAR(data));

    EogRedEyesToolbar *red_eyes = EOG_RED_EYES_TOOLBAR(data);
    EogRedEyesToolbarPrivate *priv = red_eyes->priv;
    EogWindow *window = priv->window;

    EogImage *image = eog_window_get_image(window);
    if(image == NULL)
        return;

    EogSelection *sel = EOG_SELECTION(priv->select);
    if(sel->l_coords != NULL) {
        GdkPixbuf *pix = eog_image_get_pixbuf(image);
        reset_pixbuf_image(pix, gdk_pixbuf_get_pixels (priv->orig_pix));
    }
    eog_thumb_view_select_single(EOG_THUMB_VIEW (eog_window_get_thumb_view(window)), EOG_THUMB_VIEW_SELECT_CURRENT);
    eog_image_modified(image);

    destroy_area(red_eyes);
    return;
}

static GtkToolItem *
item_button_red_eyes_cancel (EogRedEyesToolbar *toolbar)
{
    g_return_val_if_fail(EOG_IS_RED_EYES_TOOLBAR(toolbar), NULL);

    GtkWidget *img_cancel = gtk_image_new_from_icon_name("edit-undo", GTK_ICON_SIZE_BUTTON);
    GtkToolItem *ss = gtk_tool_button_new(img_cancel, _("Cancel"));

    g_signal_connect(GTK_TOOL_BUTTON(ss), "clicked", G_CALLBACK(button_red_eyes_cancel_cb), toolbar);

    return ss;
}

static void job_filter_red_eyes_finished_cb (EogJobRedEyesFilter *job, gpointer data)
{
    GFile *file = eog_image_get_file (job->image);
    const gchar *path_file = g_file_get_parse_name(file);
    eog_utils_save_image (EOG_RED_EYES_TOOLBAR(data)->priv->window, job->pixbuf_output, g_file_get_parent(file), path_file, "RedEyes", TRUE);

    gtk_widget_hide (EOG_RED_EYES_TOOLBAR(data)->priv->area_progress);
    gtk_widget_set_sensitive(GTK_WIDGET(EOG_RED_EYES_TOOLBAR(data)->priv->stack), TRUE);
    eog_utils_set_sensitive_widgets (EOG_RED_EYES_TOOLBAR(data)->priv->window, TRUE);

    destroy_area(EOG_RED_EYES_TOOLBAR(data));
    return;
}

static void
button_red_eyes_validate_cb (GtkToolButton *toolbutton,
                             gpointer       data)
{
    g_return_if_fail(EOG_IS_RED_EYES_TOOLBAR(data));

    EogRedEyesToolbarPrivate *priv = EOG_RED_EYES_TOOLBAR(data)->priv;

    EogWindow *window = priv->window;
    EogImage *image = eog_window_get_image(window);
    EogSelection *sel = EOG_SELECTION(priv->select);
    if(image == NULL || !sel->l_coords) {
        destroy_area(EOG_RED_EYES_TOOLBAR (data));
        return;
    }

    gtk_widget_show_all (priv->area_progress);

    EogJob *job = eog_job_red_eyes_filter_new(image, sel->l_coords);
    g_signal_connect (job, "finished", G_CALLBACK (job_filter_red_eyes_finished_cb), data);
    eog_job_scheduler_add_job(job);
    g_object_unref (job);
//    if (gtk_widget_get_visible (priv->area_progress))
//        g_timeout_add(500, set_thread_read_eyes, data);
}

static GtkToolItem *
item_button_red_eyes_validate (EogRedEyesToolbar *toolbar)
{
    g_return_val_if_fail(EOG_IS_RED_EYES_TOOLBAR(toolbar), NULL);

    GtkWidget *img_plus = gtk_image_new_from_icon_name("edit-redo", GTK_ICON_SIZE_BUTTON);
    GtkToolItem *ss = gtk_tool_button_new(img_plus, _("Validate"));

    g_signal_connect(GTK_TOOL_BUTTON(ss), "clicked", G_CALLBACK(button_red_eyes_validate_cb), toolbar);

    return ss;
}

static void
eog_red_eyes_toolbar_init (EogRedEyesToolbar *m_toolbars)
{
    m_toolbars->priv = eog_red_eyes_toolbar_get_instance_private(m_toolbars);

    gtk_toolbar_set_style(GTK_TOOLBAR(m_toolbars), GTK_TOOLBAR_BOTH);
    gtk_toolbar_insert(GTK_TOOLBAR(m_toolbars), item_button_red_eyes_cancel (m_toolbars), 0);
    gtk_toolbar_insert(GTK_TOOLBAR(m_toolbars), item_button_red_eyes_validate (m_toolbars), 1);
}

GtkWidget *
eog_red_eyes_toolbar_new (EogWindow *window, GtkStack *stack, GtkWidget *area_progress)
{
    return g_object_new (EOG_TYPE_RED_EYES_TOOLBAR, "window", window, "stack", stack, "area-progress", area_progress, NULL);
}
