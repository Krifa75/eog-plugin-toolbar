/* EogToolbar - Plugin.

   Copyright (C) 2019 Yahiaoui Fakhri

   This file is part of the plugin for Eog.

   EogToolbar is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License as published by the Free
   Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   EogToolbar is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
   for more details.

   You should have received a copy of the GNU General Public License
   along with EogImagesManager; see the file COPYING.  If not, write to the Free
   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   This file implements a toolbar for eog.  */

#include "eog-utils.h"
#include "utils/exif-jpg.h"
#include <glib.h>
#include <unistd.h>
#include <string.h>
#include <eog-3.0/eog/eog-thumb-view.h>

#include <glib/gi18n.h>

#define DATE_BUF_SIZE 200

/* gboolean <-> gpointer conversion macros taken from gedit */
#ifndef GBOOLEAN_TO_POINTER
#define GBOOLEAN_TO_POINTER(i) (GINT_TO_POINTER ((i) ? 2 : 1))
#endif
#ifndef GPOINTER_TO_BOOLEAN
#define GPOINTER_TO_BOOLEAN(i) ((gboolean) ((GPOINTER_TO_INT (i) == 2) ? TRUE : FALSE))
#endif

const gchar *
eog_utils_get_new_filename (const gchar *path_name, gchar *filter_name)
{
    gchar *path_name_without_extension = g_strdup(path_name);
    gchar *ext = g_utf8_strrchr(path_name_without_extension, -1, '.');
    gchar *ext_dup = NULL;

    if(!ext)
        ext_dup = ".jpeg";
    else if (strcmp(ext, ".jpg") != 0 && strcmp(ext, ".jpeg") != 0) {
        ext_dup = ".jpeg";
    } else
        ext_dup = g_strdup(ext);

    if (ext)
        *ext = 0;

    if(*filter_name != 0)
        filter_name = g_strconcat("-", filter_name, NULL);

    gchar *new_file_name = g_strconcat(path_name_without_extension, filter_name, ext_dup, NULL);

    int count = 1;
    while (!access(new_file_name, F_OK)) {
        g_free (new_file_name);

        gchar *str_count = g_strdup_printf("-%i", count);

        new_file_name = g_strconcat(path_name_without_extension, filter_name, str_count, ext_dup, NULL);

        count++;

        g_free (str_count);
    }
    return new_file_name;
}

// Get the actual orientation
guint
eog_utils_get_orientation_exif (const gchar *path)
{
#ifdef HAVE_EXIF
    guint orientation = 0;

    ExifData *exif = exif_data_new_from_file(path);
    if (!exif)
        orientation = 1;
    else {
        ExifByteOrder o = exif_data_get_byte_order (exif);

        ExifEntry *entry = exif_data_get_entry (exif, EXIF_TAG_ORIENTATION);

        if (entry && entry->data != NULL)
            orientation = exif_get_short (entry->data, o);

        exif_data_free (exif);
    }
    return orientation ? orientation : 1;
#endif
}

typedef struct {
    EogThumbView *view;
    EogImage *image;
} SelectImage;

static gboolean
select_new_image (gpointer data)
{
    SelectImage *select = (SelectImage*)data;

    eog_thumb_view_set_current_image(select->view, select->image, TRUE);

    g_object_unref(select->view);
    g_object_unref(select->image);
    g_free(select);

    return G_SOURCE_REMOVE;
}

void
eog_utils_save_image (EogWindow *window, GdkPixbuf *pix, GFile *parent, const gchar *path_name, gchar *filter_name, gboolean reset_exif)
{
    g_return_if_fail(GDK_IS_PIXBUF(pix));

    GError *error = NULL;

    gboolean is_jpeg = TRUE;
    if (!g_str_has_suffix(path_name, ".jpeg") && !g_str_has_suffix(path_name, ".jpg"))
        is_jpeg = FALSE;

    const gchar *new_name = eog_utils_get_new_filename(path_name, filter_name);

    gboolean saved = gdk_pixbuf_save(pix, new_name, "jpeg", &error, "quality", "100", NULL);
    if (error || !saved){
        g_critical("%s : Could not save the image : %s\nError : %s\n", __func__, new_name, error ? error->message : "Unknown error");
        g_clear_error(&error);
        return;
    }

    guint orientation = reset_exif ? 1 : eog_utils_get_orientation_exif(path_name);
    add_exif_to_jpg_rotate(path_name, new_name, orientation, is_jpeg);

    EogListStore *store = eog_window_get_store(window);
    EogImage *new_image = eog_image_new_file(g_file_new_for_path(new_name), g_path_get_basename(new_name));

    eog_list_store_append_image(store, new_image);
    eog_image_autorotate(new_image);
    eog_image_modified(new_image);

    SelectImage *select = g_new0(SelectImage, 1);
    select->view = g_object_ref(EOG_THUMB_VIEW(eog_window_get_thumb_view(window)));
    select->image = g_object_ref(new_image);

    g_timeout_add(400, select_new_image, select);

    return;
}

GtkWidget *
eog_utils_get_widget_by_name (GtkWidget *parent, const gchar *name)
{
    g_return_val_if_fail (GTK_IS_WIDGET (parent), NULL);
    g_return_val_if_fail (name != NULL, NULL);

    if (!g_strcmp0(gtk_widget_get_name(GTK_WIDGET(parent)), name))
        return parent;
    else if (GTK_IS_CONTAINER(parent)){
        GList *children = gtk_container_get_children(GTK_CONTAINER(parent));
        GList *it;
        for(it = children; it != NULL; it = it->next) {
            GtkWidget *child = eog_utils_get_widget_by_name(GTK_WIDGET(it->data), name);
            if (child != NULL) {
                g_list_free(children);
                return child;
            }
        }
    } else if (GTK_IS_BIN(parent)){
        GtkWidget *child = gtk_bin_get_child(GTK_BIN(parent));
        return eog_utils_get_widget_by_name(child, name);
    }
    return NULL;
}

void
eog_utils_set_sensitive_widgets (EogWindow *window, gboolean is_sensitive)
{
    g_return_if_fail(EOG_IS_WINDOW(window));

    GtkWidget *thumb_nav = eog_window_get_thumb_nav(window);
    gtk_widget_set_sensitive(thumb_nav, is_sensitive);

//    GtkWidget *thumb_view = eog_window_get_thumb_view(window);
//    gtk_widget_set_sensitive(thumb_view, is_sensitive);

    g_assert (GTK_IS_WIDGET(window));
    GtkWidget *manager = eog_utils_get_widget_by_name(GTK_WIDGET(window), "Manager");
    if(manager)
        gtk_widget_set_sensitive(manager, is_sensitive);

    GtkWidget *view = eog_window_get_view (window);
    GtkWidget *overlay = gtk_grid_get_child_at(GTK_GRID(view), 0, 0);

    GList *children = gtk_container_get_children(GTK_CONTAINER(overlay)); // hide left/right and rotat  ion buttons
    GList *it;
    for(it = children; it != NULL; it = it->next) {
        GtkWidget *widget = GTK_WIDGET(it->data);
        if (GTK_IS_REVEALER(widget))
            gtk_widget_set_visible(widget, is_sensitive);
    }
    g_list_free(children);

    return;
}

void
eog_utils_set_mode_multiple_rows (EogWindow *window)
{
    g_return_if_fail (EOG_IS_WINDOW (window));

    GtkWidget *eog_nav = eog_window_get_thumb_nav(window);
//    EogThumbNavMode mode = eog_thumb_nav_get_mode(EOG_THUMB_NAV (eog_nav));

//    if (mode == EOG_THUMB_NAV_MODE_MULTIPLE_ROWS)
//        return;

    GtkWidget *sidebar = eog_window_get_sidebar (window);
    GtkWidget *hpaned = gtk_widget_get_parent (sidebar);
    GtkWidget *box_paned = gtk_widget_get_parent(hpaned);
    GtkWidget *eog_view = eog_window_get_thumb_view(window);
    GtkWidget *scrolled_view = gtk_widget_get_parent(eog_view);

    GList *renderers = gtk_cell_layout_get_cells(GTK_CELL_LAYOUT (eog_view));
    GtkCellRenderer *text_cell = renderers->next->data;
    g_object_set (text_cell, "visible", FALSE, NULL);
    g_list_free (renderers);

    gtk_widget_set_visible (hpaned, FALSE);
    gtk_box_set_child_packing(GTK_BOX(box_paned), GTK_WIDGET(eog_nav), TRUE, TRUE, 0, GTK_PACK_START);

//    eog_thumb_nav_set_mode(EOG_THUMB_NAV (eog_nav), EOG_THUMB_NAV_MODE_MULTIPLE_ROWS);

    gtk_scrolled_window_set_propagate_natural_height (GTK_SCROLLED_WINDOW(scrolled_view), TRUE);

    gtk_icon_view_set_selection_mode (GTK_ICON_VIEW (eog_view),GTK_SELECTION_SINGLE);

    GtkAllocation alloc_nav;
    gtk_widget_get_allocation (GTK_WIDGET(eog_nav), &alloc_nav);

    gtk_icon_view_unselect_all(GTK_ICON_VIEW (eog_view));
    return;
}

void
eog_utils_set_mode_single_row (EogWindow *window)
{
    g_return_if_fail (EOG_IS_WINDOW (window));

    GtkWidget *eog_nav = eog_window_get_thumb_nav(window);
//    EogThumbNavMode mode = eog_thumb_nav_get_mode(EOG_THUMB_NAV (eog_nav));

//    if (mode == EOG_THUMB_NAV_MODE_ONE_ROW)
//        return;

    GtkWidget *sidebar = eog_window_get_sidebar (window);
    GtkWidget *hpaned = gtk_widget_get_parent (sidebar);
    GtkWidget *box_paned = gtk_widget_get_parent(hpaned);
    GtkWidget *eog_view = eog_window_get_thumb_view(window);
    GtkWidget *scrolled_view = gtk_widget_get_parent(eog_view);

    GList *renderers = gtk_cell_layout_get_cells(GTK_CELL_LAYOUT (eog_view));
    GtkCellRenderer *text_cell = renderers->next->data;
    g_object_set (text_cell, "visible", TRUE, NULL);
    g_list_free (renderers);

//    eog_thumb_nav_set_mode(EOG_THUMB_NAV(eog_nav), EOG_THUMB_NAV_MODE_ONE_ROW);
    gtk_scrolled_window_set_propagate_natural_height (GTK_SCROLLED_WINDOW(scrolled_view), FALSE);
    
    gtk_icon_view_set_selection_mode (GTK_ICON_VIEW (eog_view), GTK_SELECTION_BROWSE);
    gtk_icon_view_set_item_padding (GTK_ICON_VIEW(eog_view), 1);
    gtk_icon_view_set_margin (GTK_ICON_VIEW (eog_view), 0);

    gtk_box_set_child_packing(GTK_BOX(box_paned), GTK_WIDGET(eog_nav), FALSE, FALSE, 0, GTK_PACK_START);

    gtk_widget_show (hpaned);
    gtk_widget_show_all (eog_window_get_view (window));
    return;
}

#ifdef HAVE_STRPTIME
static gpointer
_check_strptime_updates_wday (gpointer data)
{
	struct tm tm;

	memset (&tm, '\0', sizeof (tm));
	strptime ("2008:12:24 20:30:45", "%Y:%m:%d %T", &tm);
	/* Check if tm.tm_wday is set to Wednesday (3) now */
	return GBOOLEAN_TO_POINTER (tm.tm_wday == 3);
}
#endif

/**
 * _calculate_wday_yday:
 * @tm: A struct tm that should be updated.
 *
 * Ensure tm_wday and tm_yday are set correctly in a struct tm.
 * The other date (dmy) values must have been set already.
 **/
static void
_calculate_wday_yday (struct tm *tm)
{
    GDate *exif_date;
    struct tm tmp_tm;

    exif_date = g_date_new_dmy (tm->tm_mday,
                                tm->tm_mon+1,
                                tm->tm_year+1900);

    g_return_if_fail (exif_date != NULL && g_date_valid (exif_date));

    // Use this to get GLib <-> libc corrected values
    g_date_to_struct_tm (exif_date, &tmp_tm);
    g_date_free (exif_date);

    tm->tm_wday = tmp_tm.tm_wday;
    tm->tm_yday = tmp_tm.tm_yday;
}

/* Older GCCs don't support pragma diagnostic inside functions.
 * Put these here to avoid problems with the strftime format strings
 * without breaking the build for these older GCCs */
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wformat-nonliteral"

#ifdef HAVE_STRPTIME
static gchar *
eog_util_format_date_with_strptime (const gchar *date, const gchar* format)
{
	static GOnce strptime_updates_wday = G_ONCE_INIT;
	gchar *new_date = NULL;
	gchar tmp_date[DATE_BUF_SIZE];
	gchar *p;
	gsize dlen;
	struct tm tm;

	memset (&tm, '\0', sizeof (tm));
	p = strptime (date, "%Y:%m:%d %T", &tm);

	if (p == date + strlen (date)) {
		g_once (&strptime_updates_wday,
			_check_strptime_updates_wday,
			NULL);

		// Ensure tm.tm_wday and tm.tm_yday are set
		if (!GPOINTER_TO_BOOLEAN (strptime_updates_wday.retval))
			_calculate_wday_yday (&tm);

		/* A strftime-formatted string, to display the date the image was taken.  */
		dlen = strftime (tmp_date, DATE_BUF_SIZE * sizeof(gchar), format, &tm);

		new_date = g_strndup (tmp_date, dlen);
	}

	return new_date;
}
#else
static gchar *
eog_util_format_date_by_hand (const gchar *date, const gchar* format)
{
    int year, month, day, hour, minutes, seconds;
    int result;
    gchar *new_date = NULL;

    result = sscanf (date, "%d:%d:%d %d:%d:%d",
                     &year, &month, &day, &hour, &minutes, &seconds);

    if (result < 3 || !g_date_valid_dmy (day, month, year)) {
        return NULL;
    } else {
        gchar tmp_date[DATE_BUF_SIZE];
        gsize dlen;
        struct tm tm;

        memset (&tm, '\0', sizeof (tm));
        tm.tm_mday = day;
        tm.tm_mon = month-1;
        tm.tm_year = year-1900;
        // Calculate tm.tm_wday
        _calculate_wday_yday (&tm);

        tm.tm_sec = result < 6 ? 0 : seconds;
        tm.tm_min = result < 5 ? 0 : minutes;
        tm.tm_hour = result < 4 ? 0 : hour;

        dlen = strftime (tmp_date, DATE_BUF_SIZE * sizeof(gchar), format, &tm);

        if (dlen == 0)
            return NULL;
        else
            new_date = g_strndup (tmp_date, dlen);
    }
    return new_date;
}
#endif /* HAVE_STRPTIME */

#pragma GCC diagnostic pop

/**
 * eog_exif_util_format_date:
 * @date: a date string following Exif specifications
 *
 * Takes a date string formatted after Exif specifications and generates a
 * more readable, possibly localized, string out of it.
 *
 * Returns: a newly allocated date string formatted according to the
 * current locale.
 *
 * Code took from eog
 */
gchar *
eog_util_format_date (const gchar *date)
{
    gchar *new_date;
#ifdef HAVE_STRPTIME
    /* A strftime-formatted string, to display the date the image was taken.  */
	new_date = eog_exif_util_format_date_with_strptime (date, _("%d %B %Y at %Hh%M"));
#else
    new_date = eog_util_format_date_by_hand (date, _("%d %B %Y at %Hh%M"));
#endif /* HAVE_STRPTIME */
    return new_date;
}

/*
 * Func for filters
 */
void
HSBTransform(const double hue,const double saturation,
             const double brightness, unsigned char *red, unsigned char *green, unsigned char *blue)
{
    double
            f,
            h,
            p,
            q,
            t;

    /*
      Convert HSB to RGB colorspace.
    */
    if (saturation == 0.0)
    {
        *red=ARRONDI_F(MAX_FF*brightness);
        *green=(*red);
        *blue=(*red);
        return;
    }
    h=6.0*(hue-floor(hue));
    f=h-floor((double) h);
    p=brightness*(1.0-saturation);
    q=brightness*(1.0-saturation*f);
    t=brightness*(1.0-(saturation*(1.0-f)));
    switch ((int) h)
    {
        case 0:
        default:
        {
            *red=ARRONDI_F(MAX_FF*brightness);
            *green=ARRONDI_F(MAX_FF*t);
            *blue=ARRONDI_F(MAX_FF*p);
            break;
        }
        case 1:
        {
            *red=ARRONDI_F(MAX_FF*q);
            *green=ARRONDI_F(MAX_FF*brightness);
            *blue=ARRONDI_F(MAX_FF*p);
            break;
        }
        case 2:
        {
            *red=ARRONDI_F(MAX_FF*p);
            *green=ARRONDI_F(MAX_FF*brightness);
            *blue=ARRONDI_F(MAX_FF*t);
            break;
        }
        case 3:
        {
            *red=ARRONDI_F(MAX_FF*p);
            *green=ARRONDI_F(MAX_FF*q);
            *blue=ARRONDI_F(MAX_FF*brightness);
            break;
        }
        case 4:
        {
            *red=ARRONDI_F(MAX_FF*t);
            *green=ARRONDI_F(MAX_FF*p);
            *blue=ARRONDI_F(MAX_FF*brightness);
            break;
        }
        case 5:
        {
            *red=ARRONDI_F(MAX_FF*brightness);
            *green=ARRONDI_F(MAX_FF*p);
            *blue=ARRONDI_F(MAX_FF*q);
            break;
        }
    }
}

void
TransformHSB(const unsigned char red,const unsigned char green,
             const unsigned char blue,double *hue,double *saturation,double *brightness)
{
    double
            delta,
            max,
            min;

    /*
      Convert RGB to HSB colorspace.
    */
    max=(double) (red > green ? red : green);

    if ((double) blue > max)
        max=(double) blue;

    min=(double) (red < green ? red : green);

    if ((double) blue < min)
        min=(double) blue;

    *brightness=(double) (SCALE_FF*max);

    if (max == 0.0)
    {
        *saturation=0.0;
        *hue=0.0;

        return;
    }

    *saturation=(double) (1.0-min/max);
    delta=max-min;

    if ((double) red == max)
        *hue=(double) ((green-(double) blue)/delta);
    else if ((double) green == max)
        *hue=(double) (2.0+(blue-(double) red)/delta);
    else
        *hue=(double) (4.0+(red-(double) green)/delta);

    *hue /= 6.0;

    if (*hue < 0.0)
        *hue+=1.0;
}

static double
RoundToUnity (double value)
{
    return (value < 0.0 ? 0.0 :(value > 1.0) ? 1.0 : value);
}

static double
RationToQuantum (double value)
{
    if(value <= 0.0)
        return 0.0;
    if(value >= MAX_FF)
        return 255.0;
    return value;
}

static guint8
Multiply(double p, double alpha,double q,double beta)
{
    double pixel;

    pixel=ScaleQuantum*(1.0-ScaleQuantum*alpha)*p*(1.0-ScaleQuantum*beta)*q+
          (1.0-ScaleQuantum*alpha)*p*ScaleQuantum*beta+(1.0-ScaleQuantum*beta)*q*
                                                       ScaleQuantum*alpha;
    return RationToQuantum(pixel);
}

static void
RGBToHSLColorspace (const unsigned char red,const unsigned char green, const unsigned char blue,double *hue,double *saturation,double *luminosity)
{
    double r=(double) red/MAX_F;
    double g=(double) green/MAX_F;
    double b=(double) blue/MAX_F;
    double max=MAX(r,MAX(g,b));
    double min=MIN(r,MIN(g,b));
    double delta=max-min;
    *luminosity=(double) ((min+max)/2.0);
    *saturation=0.0;
    *hue=0.0;
    if (delta == 0.0) return;
    *saturation=(double) (delta/((*luminosity < 0.5) ? (min+max) : (2.0-max-min)));
    if (r == max) *hue=(double) (g == min ? 5.0+(max-b)/delta : 1.0-(max-g)/delta);
    else if (g == max) *hue=(double) (b == min ? 1.0+(max-r)/delta : 3.0-(max-b)/delta);
    else *hue=(double) (r == min ? 3.0+(max-g)/delta : 5.0-(max-r)/delta);
    *hue/=6.0;
}

static void
HSLToRGBColorspace (const double hue,const double saturation, const double luminosity,unsigned char *red,unsigned char *green,unsigned char *blue)
{
    if (saturation == 0.0)
    {
        *red=(unsigned char) (MAX_F*luminosity+0.5);
        *green=(unsigned char) (MAX_F*luminosity+0.5);
        *blue=(unsigned char) (MAX_F*luminosity+0.5);
        return;
    }
    double v=(luminosity <= 0.5) ? (luminosity*(1.0+saturation)) : (luminosity+saturation-luminosity*saturation);
    double y=2.0*luminosity-v;
    double x=y+(v-y)*(6.0*hue-floor(6.0*hue));
    double z=v-(v-y)*(6.0*hue-floor(6.0*hue));
    double r = 0.0;
    double g = 0.0;
    double b = 0.0;
    switch ((int) (6.0*hue))
    {
        case 0: r=v; g=x; b=y; break;
        case 1: r=z; g=v; b=y; break;
        case 2: r=y; g=v; b=x; break;
        case 3: r=y; g=z; b=v; break;
        case 4: r=x; g=y; b=v; break;
        case 5: r=v; g=y; b=z; break;
        default: r=v; g=x; b=y; break;
    }
    *red=ARRONDI_F(MAX_F*r);
    *green=ARRONDI_F(MAX_F*g);
    *blue=ARRONDI_F(MAX_F*b);
}

void
CompositeMultiply(guint8 *p, double alpha,guint8 *q, double beta)
{
    double gamma=RoundToUnity((1.0-ScaleQuantum*alpha)+(1.0-ScaleQuantum*beta)-
                              (1.0-ScaleQuantum*alpha)*(1.0-ScaleQuantum*beta));
    //q[3]=(int)((double)RangQuantum*(1.0-gamma));
    gamma=1.0/(fabs(gamma) <= Epsilon ? 1.0 : gamma);

    q[0]=gamma * Multiply(p[0],alpha,q[0],beta);
    q[1]=gamma * Multiply(p[1],alpha,q[1],beta);
    q[2]=gamma * Multiply(p[2],alpha,q[2],beta);
}

void
ModulateRGBHSB (const double phue, const double psaturation,const double pbrightness, unsigned char *red,unsigned char *green,unsigned char *blue)
{
    double	brightness, hue, saturation;

    RGBToHSLColorspace(*red,*green,*blue,&hue,&saturation,&brightness);
    saturation*=0.01*psaturation;
    brightness*=0.01*pbrightness;
    hue+=0.5*(0.01*phue-1.0);
    while (hue < 0.0) hue+=1.0;
    while (hue > 1.0) hue-=1.0;
    HSLToRGBColorspace(hue,saturation,brightness,red,green,blue);
}