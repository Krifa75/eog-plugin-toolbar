/* EogToolbar - Plugin.

   Copyright (C) 2019 Yahiaoui Fakhri

   This file is part of the plugin for Eog.

   EogToolbar is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License as published by the Free
   Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   EogToolbar is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
   for more details.

   You should have received a copy of the GNU General Public License
   along with EogImagesManager; see the file COPYING.  If not, write to the Free
   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   This file implements a toolbar for eog.  */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <eog-3.0/eog/eog-window-activatable.h>
#include <eog-3.0/eog/eog-debug.h>
#include <eog-3.0/eog/eog-scroll-view.h>
#include "eog-toolbar-plugin.h"
#include "eog-main-toolbar.h"
#include "utils/exif-jpg.h"
#include <eog-3.0/eog/eog-image.h>
#include "eog-utils.h"
#include <glib/gi18n.h>
#include <eog-3.0/eog/eog-thumb-view.h>
#include <eog-3.0/eog/eog-thumb-nav.h>

static void eog_window_activatable_iface_init (EogWindowActivatableInterface *iface);

struct _EogToolbarPluginPrivate {
    guint id_prepared;

    GtkWidget *stack_toolbars;
    GtkWidget *toolbar;

    GtkWidget *box_toolbar;
    GtkWidget *scroll;
    GtkWidget *left_button, *right_button;

    GtkWidget *area_progress;
    cairo_surface_t *throbber;

    guint size_throbber;
    GTimer *timer_throbber;
    gfloat throbber_x, throbber_y;
    gfloat middle_area_width, middle_area_height;

    guint text_size;
    gchar *text_display;
    gfloat text_x, text_y;

    GtkWidget *revealer_info;
    GtkWidget *box_info;
    GtkWidget *label_info;
    gchar     *format;
    GSource   *overlay_timeout_source;

    GAction *act_left, *act_right;
};

G_DEFINE_DYNAMIC_TYPE_EXTENDED (EogToolbarPlugin,
                                eog_toolbar_plugin,
                                PEAS_TYPE_EXTENSION_BASE,
                                0,
                                G_ADD_PRIVATE_DYNAMIC(EogToolbarPlugin)
                                G_IMPLEMENT_INTERFACE_DYNAMIC (EOG_TYPE_WINDOW_ACTIVATABLE,
                                                               eog_window_activatable_iface_init))

enum {
    PROP_0,
    PROP_WINDOW
};

enum {
    EOG_ROTATION_NONE=1,
    EOG_ROTATION_FLIP_HORIZONTAL,
    EOG_ROTATION_ROT_180,
    EOG_ROTATION_FLIP_VERTICAL,
    EOG_ROTATION_TRANSPOSE,
    EOG_ROTATION_ROT_90,
    EOG_ROTATION_TRANSVERSE,
    EOG_ROTATION_ROT_270,
};

typedef struct {
    EogThumbView *thumbview;
    EogImage *image;
    gchar *original_file;
    gchar *new_file;
    guint orientation;
} Rotation;

static void
eog_toolbar_plugin_set_property (GObject      *object,
                                 guint         prop_id,
                                 const GValue *value,
                                 GParamSpec   *pspec)
{
    EogToolbarPlugin *plugin = EOG_TOOLBAR_PLUGIN (object);

    switch (prop_id)
    {
        case PROP_WINDOW:
            plugin->window = EOG_WINDOW (g_value_dup_object (value));
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
            break;
    }
}

static void
eog_toolbar_plugin_get_property (GObject    *object,
                                 guint       prop_id,
                                 GValue     *value,
                                 GParamSpec *pspec)
{
    EogToolbarPlugin *plugin = EOG_TOOLBAR_PLUGIN (object);

    switch (prop_id)
    {
        case PROP_WINDOW:
            g_value_set_object (value, plugin->window);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
            break;
    }
}

static void
eog_toolbar_plugin_init (EogToolbarPlugin *plugin)
{
    eog_debug_message (DEBUG_PLUGINS, "EogToolbarPlugin initializing");

    bindtextdomain (ET_PACKAGE, TOOLBAR_LOCAL_DIR);
    bind_textdomain_codeset (ET_PACKAGE, "UTF-8");
    textdomain (ET_PACKAGE);

    plugin->priv = eog_toolbar_plugin_get_instance_private (plugin);
    plugin->priv->middle_area_width = 0;
    plugin->priv->middle_area_height = 0;
    plugin->priv->size_throbber = 30;//100;//200;

    plugin->priv->text_size = 20;//25;//50;
    plugin->priv->text_display = _("Change in progress...");
}

static void
eog_toolbar_plugin_dispose (GObject *object)
{
    EogToolbarPlugin *plugin = EOG_TOOLBAR_PLUGIN (object);

    eog_debug_message (DEBUG_PLUGINS, "EogToolbarPlugin disposing");

    if (plugin->window != NULL) {
        g_object_unref (plugin->window);
        plugin->window = NULL;
    }

    G_OBJECT_CLASS (eog_toolbar_plugin_parent_class)->dispose (object);
}

/*
 *
 *   EOG_ROTATION_NONE=1,
 *   EOG_ROTATION_FLIP_HORIZONTAL=2,
 *   EOG_ROTATION_ROT_180=3,
 *   EOG_ROTATION_FLIP_VERTICAL=4,
 *   EOG_ROTATION_TRANSPOSE=5,
 *   EOG_ROTATION_ROT_90=6,
 *   EOG_ROTATION_TRANSVERSE=7,
 *   EOG_ROTATION_ROT_270=8,
 */
static guint transform_to_exif(guint orientation, gboolean inv) {
    guint exif_type = 1;
    switch (orientation) {
        /* NO FLIP */
        case EOG_ROTATION_NONE: // inv ? 1 to 8 : 1 to 6
            exif_type = inv ? EOG_ROTATION_ROT_270 : EOG_ROTATION_ROT_90;
            break;
        case EOG_ROTATION_ROT_90: //inv ? 6 to 1 : 6 to 3
            exif_type = inv ? EOG_ROTATION_NONE : EOG_ROTATION_ROT_180;
            break;
        case EOG_ROTATION_ROT_180: //inv ? 3 to 6 : 3 to 8
            exif_type = inv ? EOG_ROTATION_ROT_90 : EOG_ROTATION_ROT_270;
            break;
        case EOG_ROTATION_ROT_270: //inv ? 8 to 3 : 8 to 1
            exif_type = inv ? EOG_ROTATION_ROT_180 : EOG_ROTATION_NONE;
            break;
            /* FLIP */
        case EOG_ROTATION_FLIP_HORIZONTAL: // inv ? 2 to 7 : 2 to 5
            exif_type = inv ? EOG_ROTATION_TRANSPOSE : EOG_ROTATION_TRANSVERSE;
            break;
        case EOG_ROTATION_FLIP_VERTICAL: // inv ? 4 to 5 : 4 to 7
            exif_type = inv ? EOG_ROTATION_TRANSVERSE : EOG_ROTATION_TRANSPOSE;
            break;
        case EOG_ROTATION_TRANSPOSE: // inv ? 5 to 2 : 5 to 4
            exif_type = inv ? EOG_ROTATION_FLIP_VERTICAL : EOG_ROTATION_FLIP_HORIZONTAL;
            break;
        case EOG_ROTATION_TRANSVERSE: // inv ? 7 to 4 : 7 to 2
            exif_type = inv ? EOG_ROTATION_FLIP_HORIZONTAL : EOG_ROTATION_FLIP_VERTICAL;
            break;
        default:
            g_warning("Orientation not found : %d\n", orientation);
            break;
    }
    return exif_type;
}

static gboolean
select_image_rotated (gpointer user_data){

    Rotation *r = (Rotation*)user_data;

    add_exif_to_jpg_rotate(r->original_file, r->new_file, r->orientation, FALSE);
    eog_image_autorotate(r->image);
    eog_image_file_changed(r->image);

    eog_list_store_append_image(EOG_LIST_STORE(gtk_icon_view_get_model(GTK_ICON_VIEW(r->thumbview))), r->image);
    eog_thumb_view_set_current_image(r->thumbview, r->image, TRUE);

    g_free(r->original_file);
    g_free(r->new_file);
    g_object_unref(r->thumbview);
    g_object_unref(r->image);
    g_free (r);
    return G_SOURCE_REMOVE;
}
#include <eog-3.0/eog/eog-job-scheduler.h>

static void
do_rotation (EogWindow *window, gboolean inv)
{
    EogThumbView *thumbview = EOG_THUMB_VIEW(eog_window_get_thumb_view(window));
    EogListStore *store = EOG_LIST_STORE(gtk_icon_view_get_model(GTK_ICON_VIEW(thumbview)));

    GError *error = NULL;
    EogImage *image_selected = eog_window_get_image(window);
    GFile *file_image = eog_image_get_file(image_selected);
    GFile *parent_folder = g_file_get_parent(file_image);
    if (access(g_file_get_parse_name(parent_folder), W_OK)) {
        g_warning("Cannot do rotation, the folder is readonly");
        return;
    }
    gchar *f_name = g_file_get_parse_name(file_image);

    int can_read = eog_image_is_file_writable (image_selected);
    if(!can_read)
        g_warning("(%s - %s) The user cannot read the file %s.", __FILE__, __func__, f_name);
//    const gchar *caption = eog_image_get_caption(image_selected);
    guint orientation = transform_to_exif(eog_utils_get_orientation_exif(f_name), inv);

    if (eog_image_is_jpeg(image_selected) && can_read) {
        add_exif_to_jpg_rotate(f_name, f_name, orientation, TRUE);

        eog_image_autorotate(image_selected);

        if (eog_list_store_get_monitoring()) {
            eog_image_file_changed(image_selected);
            eog_list_store_append_image(store, image_selected);
        }
    //eog_window_reload_image(window);
    } else {
        GdkPixbuf *orig_pix = gdk_pixbuf_new_from_file(f_name, &error);
        if (error || !orig_pix) {
            g_warning("(%s) Cannot load pixbuf %s : %s.", __func__, f_name, error ? error->message : "Unknown error");
            g_clear_error(&error);
            return;
        }

        const gchar *new_fname = eog_utils_get_new_filename(f_name, "");

        gdk_pixbuf_save(orig_pix, new_fname, "jpeg", &error, "quality", "100", NULL);
        if (error) {
            g_warning("(%s : %s) Cannot save pixbuf %s : %s", __FILE__, __func__, new_fname, error ? error->message : "Unknown error");
            g_clear_error(&error);
            return;
        }

        GFile *g_fname = g_file_new_for_path(new_fname);
        EogImage *image_rotated = eog_image_new_file(g_fname, g_file_get_basename(g_fname));

        Rotation *r = g_new0(Rotation, 1);
        r->thumbview = g_object_ref(thumbview);
        r->image = g_object_ref(image_rotated);
        r->original_file = g_strdup(f_name);
        r->new_file = g_strdup(new_fname);
        r->orientation = orientation;
        g_timeout_add (100, select_image_rotated, r);

        g_object_unref(g_fname);
        g_object_unref(orig_pix);
    }
    g_object_unref (file_image);
    g_object_unref (parent_folder);
//    g_object_unref(image_selected);
    g_free (f_name);

    return;
}

static void
rotation_right (GSimpleAction *action,
                GVariant      *parameter,
                gpointer       user_data)
{
    g_return_if_fail (EOG_IS_WINDOW (user_data));
    EogWindow *window = EOG_WINDOW(user_data);
    do_rotation(window, FALSE);

    return;
}

static void
rotation_left (GSimpleAction *action,
               GVariant      *parameter,
               gpointer       user_data)
{
    g_return_if_fail (EOG_IS_WINDOW (user_data));

    EogWindow *window = EOG_WINDOW(user_data);
    do_rotation(window, TRUE);

    return;
}

static void
hide_items_in_read_only (GtkIconView *iconview,
                         EogToolbarPlugin *plugin)
{
    EogToolbarPluginPrivate *priv = plugin->priv;

    EogImage *image = eog_thumb_view_get_first_selected_image(EOG_THUMB_VIEW(iconview));
    if (image != NULL) {
        EogListStore *store = eog_window_get_store(plugin->window);
        gint pos = eog_list_store_get_pos_by_image(store, image);
        if (pos == eog_list_store_length(store) - 1) {
            g_simple_action_set_enabled(G_SIMPLE_ACTION(priv->act_right), FALSE);
            g_simple_action_set_enabled(G_SIMPLE_ACTION(priv->act_left), TRUE);
            g_message ("Hide last");
        } else if (pos == 0) {
            g_simple_action_set_enabled(G_SIMPLE_ACTION(priv->act_left), FALSE);
            g_simple_action_set_enabled(G_SIMPLE_ACTION(priv->act_right), TRUE);
            g_message ("Hide first");
        }else {
            gboolean enabled;

            enabled = g_action_get_enabled (G_ACTION(priv->act_right));
            if (!enabled)
                g_simple_action_set_enabled(G_SIMPLE_ACTION(priv->act_right), TRUE);

            enabled = g_action_get_enabled (G_ACTION(priv->act_left));
            if (!enabled)
                g_simple_action_set_enabled(G_SIMPLE_ACTION(priv->act_left), TRUE);
        }

        if (eog_list_store_length(store) == 1)
            eog_main_toolbar_hide_gallery (EOG_MAIN_TOOLBAR(priv->toolbar), FALSE);
        else
            eog_main_toolbar_hide_gallery (EOG_MAIN_TOOLBAR(priv->toolbar), TRUE);


        gboolean image_writable = eog_image_is_file_writable(image);
        GFile *file = eog_image_get_file (image);
        GFile *file_parent = g_file_get_parent (file);

        gboolean folder_writable = access(g_file_get_path(file_parent), W_OK) == 0 ? TRUE : FALSE;

        if (!folder_writable || !image_writable)
            eog_main_toolbar_hide_filters(EOG_MAIN_TOOLBAR(priv->toolbar), FALSE);
        else
            eog_main_toolbar_hide_filters(EOG_MAIN_TOOLBAR(priv->toolbar), TRUE);

        g_object_unref (image);
        g_object_unref (file_parent);
        g_object_unref (file);
    }

    return;
}

static gboolean
scroll_button (GtkWidget *widget,
               GdkEvent  *event,
               gpointer   user_data)
{
    g_return_val_if_fail (GTK_IS_BUTTON(widget), FALSE);
    g_return_val_if_fail (EOG_IS_TOOLBAR_PLUGIN(user_data), FALSE);

    EogToolbarPluginPrivate *priv = EOG_TOOLBAR_PLUGIN(user_data)->priv;

    gboolean direction = gtk_widget_get_direction (widget) == GTK_TEXT_DIR_LTR ?
                         GTK_WIDGET (widget) == priv->right_button :
                         GTK_WIDGET (widget) == priv->left_button;

    GtkAdjustment *adj = gtk_scrolled_window_get_hadjustment(GTK_SCROLLED_WINDOW(priv->scroll));
    if (!direction) {
        gtk_adjustment_set_value (adj, gtk_adjustment_get_lower(adj));
        gtk_widget_show(priv->right_button);
    } else {
        gtk_adjustment_set_value (adj, gtk_adjustment_get_upper(adj));
        gtk_widget_show(priv->left_button);
    }

    return TRUE;
}

static void
eog_toolbar_plugin_adj_changed (GtkAdjustment *adj, gpointer user_data)
{
    g_return_if_fail (EOG_IS_TOOLBAR_PLUGIN(user_data));

    EogToolbarPluginPrivate *priv = EOG_TOOLBAR_PLUGIN(user_data)->priv;

    if (!strcmp(gtk_stack_get_visible_child_name(GTK_STACK(priv->stack_toolbars)), "main-toolbar")) {
        gboolean ltr;

        ltr = gtk_widget_get_direction(priv->scroll) == GTK_TEXT_DIR_LTR;

        gtk_widget_set_visible(ltr ? priv->right_button : priv->left_button,
                               gtk_adjustment_get_value(adj)
                               < gtk_adjustment_get_upper(adj)
                                 - gtk_adjustment_get_page_size(adj));
    } else {
        if (gtk_widget_get_visible(priv->left_button))
            gtk_widget_set_visible(priv->left_button, FALSE);

        if (gtk_widget_get_visible(priv->right_button))
            gtk_widget_set_visible(priv->right_button, FALSE);
    }
}

static void
eog_toolbar_plugin_adj_value_changed (GtkAdjustment *adj, gpointer user_data)
{
    g_return_if_fail (EOG_IS_TOOLBAR_PLUGIN(user_data));

    EogToolbarPluginPrivate *priv = EOG_TOOLBAR_PLUGIN(user_data)->priv;

    gboolean ltr;

    ltr = gtk_widget_get_direction (priv->scroll) == GTK_TEXT_DIR_LTR;

    gtk_widget_set_visible (ltr ? priv->left_button : priv->right_button,
                            gtk_adjustment_get_value (adj) > 0);

    gtk_widget_set_visible (ltr ? priv->right_button : priv->left_button,
                            gtk_adjustment_get_value (adj)
                            < gtk_adjustment_get_upper (adj)
                              - gtk_adjustment_get_page_size (adj));
}

static void
display_arrows (GObject    *gobject,
                GParamSpec *pspec,
                gpointer    user_data)
{
    g_return_if_fail (GTK_IS_STACK (gobject));
    g_return_if_fail (EOG_IS_TOOLBAR_PLUGIN(user_data));

    EogToolbarPlugin *plugin = EOG_TOOLBAR_PLUGIN(user_data);
    EogToolbarPluginPrivate *priv = plugin->priv;

    GtkAdjustment *adj = gtk_scrolled_window_get_hadjustment(GTK_SCROLLED_WINDOW (priv->scroll));
    if (!strcmp(gtk_stack_get_visible_child_name(GTK_STACK(priv->stack_toolbars)), "main-toolbar")) {
        gtk_adjustment_set_value (adj, gtk_adjustment_get_lower(adj));

        GtkWidget *eog_nav = eog_window_get_thumb_nav(plugin->window);
        if (!gtk_widget_get_visible(eog_nav) && eog_list_store_length(eog_window_get_store (plugin->window)) > 1)
            gtk_widget_show_all (eog_nav);

        GtkWidget *eog_scrollview = eog_window_get_view (plugin->window);
        GtkWidget *box_info = gtk_grid_get_child_at(GTK_GRID (eog_scrollview), 0, 2);//g_list_nth_data(children_grid, 1);
        gtk_widget_hide (box_info);
    } else
        eog_toolbar_plugin_adj_changed (adj, user_data);
}

static gboolean
configure_area_cb (GtkWidget *widget,
                   GdkEvent  *event,
                   EogToolbarPlugin *plugin)
{
    g_return_val_if_fail (EOG_IS_TOOLBAR_PLUGIN(plugin), FALSE);

    EogToolbarPluginPrivate *priv = plugin->priv;
    GtkWidget *eog_scrollview = eog_window_get_view(plugin->window);
    GtkAllocation alloc_sw;
    gtk_widget_get_allocation(eog_scrollview, &alloc_sw);
    priv->middle_area_width = (gfloat)alloc_sw.width / 2.f;
    priv->middle_area_height = (gfloat)alloc_sw.height / 2.f;

//        g_message ("AREA(%d, %d), HALF(%lf, %lf)", gtk_widget_get_allocated_width(box_next), gtk_widget_get_allocated_height(box_next), priv->middle_area_width, priv->middle_area_height);

    priv->throbber_x = priv->middle_area_width - priv->size_throbber / 2.f;
    priv->throbber_y = priv->middle_area_height - priv->size_throbber / 2.f;

    cairo_text_extents_t extents;
    cairo_t *cr = cairo_create(priv->throbber);
    cairo_select_font_face(cr, "Helvetica", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
    cairo_set_font_size(cr, priv->text_size);
    cairo_text_extents(cr, priv->text_display, &extents);
    priv->text_x = priv->middle_area_width - (extents.width/2 - extents.x_bearing);
    priv->text_y = priv->middle_area_height + 25;
    cairo_destroy(cr);

    return TRUE;
}

static gboolean
on_draw_event (GtkWidget    *widget,
               cairo_t *cr,
               EogToolbarPlugin      *plugin){

    g_return_val_if_fail(GTK_IS_DRAWING_AREA(widget), FALSE);

    EogToolbarPluginPrivate *priv = plugin->priv;

    gdouble el = g_timer_elapsed (priv->timer_throbber, NULL);
    gdouble r = (2 * G_PI) * el;

    cairo_set_source_rgba(cr, 0, 0, 0, 0.4);
    cairo_paint(cr);

    cairo_select_font_face(cr, "Helvetica", CAIRO_FONT_SLANT_NORMAL, CAIRO_FONT_WEIGHT_BOLD);
    cairo_set_font_size(cr, priv->text_size);
    cairo_move_to(cr, priv->text_x, priv->text_y);
    cairo_set_source_rgb(cr, 1, 1, 1);
    cairo_show_text(cr, priv->text_display);

    cairo_translate(cr, priv->throbber_x, priv->throbber_y);
    cairo_rotate(cr, r/(2*G_PI)*4.0);
    cairo_translate(cr, -(priv->middle_area_width - priv->throbber_x), -(priv->middle_area_height - priv->throbber_y));

    // rotate around center of image
//    cairo_matrix_t matrix;
//    cairo_matrix_init_identity (&matrix)
//    cairo_matrix_translate (&matrix, priv->middle_area_width, s_height/2.0)
//    cairo_matrix_rotate (&matrix, deg2rad(30));
//    cairo_matrix_translate (&matrix, -(s_width-d_width)/2.0, -(s_height-d_height)/2.0)
//    cairo_pattern_set_matrix (source_pattern, &matrix);

    cairo_set_source_surface(cr, priv->throbber, 0, 0);//priv->throbber_x, priv->throbber_y);
    cairo_paint (cr);

    gtk_widget_queue_draw(widget);

    return FALSE;
}

static void
start_timer_throbber (GtkWidget *widget,
                      gpointer   user_data)
{
    g_return_if_fail (EOG_IS_TOOLBAR_PLUGIN(user_data));
    EogToolbarPluginPrivate *priv = EOG_TOOLBAR_PLUGIN(user_data)->priv;

    if (!priv->timer_throbber)
        priv->timer_throbber = g_timer_new();
    else
        g_timer_start(priv->timer_throbber);
    return;
}

static void
stop_timer_throbber (GtkWidget *widget,
                      gpointer   user_data)
{
    g_return_if_fail (EOG_IS_TOOLBAR_PLUGIN(user_data));
    EogToolbarPluginPrivate *priv = EOG_TOOLBAR_PLUGIN(user_data)->priv;

    if (priv->timer_throbber)
        g_timer_stop(priv->timer_throbber);
    return;
}

static gchar*
get_date_from_file (EogImage *image)
{
    gchar *str_date = NULL;
    GFile *file = eog_image_get_file(image);
    GFileInfo *file_info = g_file_query_info(file,
                                             G_FILE_ATTRIBUTE_TIME_MODIFIED,
                                             0, NULL, NULL);
    if (file_info) {
        guint64 modif_time = g_file_info_get_attribute_uint64 (file_info, G_FILE_ATTRIBUTE_TIME_MODIFIED);
        GDateTime *date_time = g_date_time_new_from_unix_local (modif_time);
        str_date = g_date_time_format (date_time, _("%d %B %Y at %Hh%M"));
        g_date_time_unref(date_time);
        g_object_unref(file_info);
    }
    g_object_unref (file);
    return str_date;
}

static gboolean
test (gpointer user_data)
{
    EogToolbarPlugin *plugin = EOG_TOOLBAR_PLUGIN (user_data);

    hide_items_in_read_only (GTK_ICON_VIEW (eog_window_get_thumb_view(plugin->window)), plugin);

    return G_SOURCE_REMOVE;
}

static void
_notify_image_cb (GObject *gobject, GParamSpec *pspec, gpointer user_data)
{
    g_return_if_fail (EOG_IS_TOOLBAR_PLUGIN(user_data));
    g_return_if_fail(EOG_IS_SCROLL_VIEW(gobject));

    EogToolbarPlugin *plugin = EOG_TOOLBAR_PLUGIN (user_data);

    EogThumbNavMode mode = eog_thumb_nav_get_mode(EOG_THUMB_NAV (eog_window_get_thumb_nav (plugin->window)));
    EogImage *image = eog_scroll_view_get_image(EOG_SCROLL_VIEW(gobject));
    if (!image || mode == EOG_THUMB_NAV_MODE_MULTIPLE_ROWS)
        return;

    gchar *info_display = NULL;
    const gchar *image_name = eog_image_get_caption(image);
    gchar *display_name = g_strdup (image_name);
    gchar *suffix = g_strrstr (display_name, ".");
    if (suffix)
        *suffix = 0;
    const gchar *str_date = NULL;

#ifdef HAVE_EXIF
    ExifData *exif_data = (ExifData *) eog_image_get_exif_info(image);
    if (exif_data) {
        ExifEntry *exif_entry = NULL;
        gchar time_buffer[PATH_MAX];

        const gchar *exif_date = NULL;
        if ( (exif_entry = exif_data_get_entry(exif_data, EXIF_TAG_DATE_TIME_ORIGINAL)) != NULL) {
            exif_date = exif_entry_get_value (exif_entry, time_buffer, PATH_MAX);
//            g_message ("DATE_TIME_ORIGINIAL %s", exif_date);
        } else if ( (exif_entry = exif_data_get_entry(exif_data, EXIF_TAG_DATE_TIME)) != NULL) {
            exif_date = exif_entry_get_value (exif_entry, time_buffer, PATH_MAX);
//            g_message ("DATE_TIME %s", exif_date);
        }

        if(!exif_date)
            str_date = get_date_from_file(image);
        else
            str_date = eog_util_format_date (exif_date);
        exif_data_unref (exif_data);
    } else
#endif
        {
            str_date = get_date_from_file(image);
    }

    gint width, height;
    eog_image_get_size (image, &width, &height);
    gchar *size_str = g_strdup_printf("%ix%i", width, height);

    info_display = g_strconcat(display_name, " (", _("on "), str_date, ", ", size_str, ")", NULL);
    gchar *markup = g_markup_printf_escaped(plugin->priv->format, info_display);
    gtk_label_set_markup(GTK_LABEL (plugin->priv->label_info), markup);
    g_free (markup);

    if (image)
        g_object_unref (image);

    gtk_revealer_set_reveal_child(GTK_REVEALER (plugin->priv->revealer_info), TRUE);

    g_timeout_add(500, test, plugin);
}

static void
_clear_overlay_timeout (EogToolbarPlugin *plugin)
{
    EogToolbarPluginPrivate *priv = plugin->priv;

    if (priv->overlay_timeout_source != NULL) {
        g_source_unref (priv->overlay_timeout_source);
        g_source_destroy (priv->overlay_timeout_source);
    }

    priv->overlay_timeout_source = NULL;
}

static gboolean
_overlay_timeout_cb (gpointer data)
{
    EogToolbarPluginPrivate *priv = EOG_TOOLBAR_PLUGIN(data)->priv;

    gtk_revealer_set_reveal_child (GTK_REVEALER (priv->revealer_info), FALSE);

    _clear_overlay_timeout (data);

    return FALSE;
}

static void
_set_overlay_timeout (EogToolbarPlugin *plugin)
{
    GSource *source;

    _clear_overlay_timeout (plugin);

    source = g_timeout_source_new (1000);
    g_source_set_callback (source, _overlay_timeout_cb, plugin, NULL);

    g_source_attach (source, NULL);

    plugin->priv->overlay_timeout_source = source;
}

static gboolean
_motion_notify_cb (GtkWidget *widget, GdkEventMotion *event, gpointer user_data)
{
    g_return_val_if_fail (EOG_IS_TOOLBAR_PLUGIN(user_data), FALSE);

    EogToolbarPluginPrivate *priv = EOG_TOOLBAR_PLUGIN(user_data)->priv;
    gboolean reveal_child;

    reveal_child = gtk_revealer_get_reveal_child (GTK_REVEALER (priv->revealer_info));

    if (!reveal_child)
        gtk_revealer_set_reveal_child (GTK_REVEALER (priv->revealer_info), TRUE);

    /* reset timeout */
    _set_overlay_timeout(user_data);

    return FALSE;
}

static void
display_toolbar (EogWindow *window,
                 gpointer user_data)
{
    g_return_if_fail (EOG_IS_TOOLBAR_PLUGIN(user_data));

    EogToolbarPlugin *plugin = EOG_TOOLBAR_PLUGIN (user_data);
    EogToolbarPluginPrivate *priv = EOG_TOOLBAR_PLUGIN(user_data)->priv;

    if (priv->id_prepared != 0) {
        g_signal_handler_disconnect(window, priv->id_prepared);
        priv->id_prepared = 0;
    }

    GtkAdjustment *adj = gtk_scrolled_window_get_hadjustment (GTK_SCROLLED_WINDOW(priv->scroll));

    g_signal_connect (adj,
                      "changed",
                      G_CALLBACK (eog_toolbar_plugin_adj_changed),
                      plugin);

    g_signal_connect (adj,
                      "value-changed",
                      G_CALLBACK (eog_toolbar_plugin_adj_value_changed),
                      plugin);
    eog_toolbar_plugin_adj_value_changed (gtk_scrolled_window_get_hadjustment(GTK_SCROLLED_WINDOW(priv->scroll)), plugin);

    GtkWidget *eog_scrollview = eog_window_get_view (window);
    GtkWidget *overlay_scrollview = gtk_grid_get_child_at(GTK_GRID (eog_scrollview), 0, 0);
    g_assert (GTK_IS_OVERLAY (overlay_scrollview));

    GList *revealers = gtk_container_get_children (GTK_CONTAINER (overlay_scrollview));
    priv->revealer_info = g_list_nth_data(revealers, 3);
    g_assert (GTK_IS_REVEALER(priv->revealer_info));

    gtk_container_remove (GTK_CONTAINER(priv->revealer_info), gtk_bin_get_child (GTK_BIN(priv->revealer_info)));

    priv->box_info = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 0);
    gtk_widget_set_opacity(priv->box_info, 0.8);
    gtk_widget_set_name (priv->box_info, "box-info");

    priv->label_info = gtk_label_new (NULL);
    gtk_label_set_max_width_chars(GTK_LABEL (priv->label_info), 80);
    gtk_label_set_ellipsize(GTK_LABEL (priv->label_info), PANGO_ELLIPSIZE_END);
    priv->format = "<span foreground='white' size='medium'><b>%s</b></span>";

    gtk_box_pack_start(GTK_BOX (priv->box_info), priv->label_info, FALSE, FALSE, 0);

    gtk_container_add (GTK_CONTAINER (priv->revealer_info), priv->box_info);

    gtk_widget_show_all (priv->revealer_info);

    g_signal_connect (overlay_scrollview, "motion-notify-event", G_CALLBACK (_motion_notify_cb), plugin);

    GtkWidget *box_info = gtk_grid_get_child_at(GTK_GRID (eog_scrollview), 0, 2);
    gtk_widget_hide (box_info);

    g_signal_connect (priv->stack_toolbars, "notify::visible-child", G_CALLBACK(display_arrows), plugin);
    g_signal_connect (eog_scrollview, "notify::image", G_CALLBACK (_notify_image_cb), plugin);

    g_signal_connect(priv->area_progress, "show", G_CALLBACK(start_timer_throbber), plugin);
    g_signal_connect(priv->area_progress, "hide", G_CALLBACK(stop_timer_throbber), plugin);
    g_signal_connect (priv->area_progress, "configure-event", G_CALLBACK(configure_area_cb), plugin);
    g_signal_connect(priv->area_progress, "draw", G_CALLBACK(on_draw_event), plugin);
/*
    gint pos = eog_list_store_get_pos_by_image(eog_window_get_store(plugin->window), eog_thumb_view_get_first_selected_image(EOG_THUMB_VIEW(eog_window_get_thumb_view(plugin->window))));
    if (pos == eog_list_store_length(eog_window_get_store(plugin->window)) - 1) {
        g_simple_action_set_enabled(G_SIMPLE_ACTION(priv->act_right), FALSE);
        g_message ("Last");
    } else if (pos == 0) {
        g_simple_action_set_enabled(G_SIMPLE_ACTION(priv->act_left), FALSE);
        g_message ("00");
    }*/

//    hide_items_in_read_only (GTK_ICON_VIEW (eog_window_get_thumb_view(plugin->window)), plugin);
//    gtk_widget_show_all (priv->area_progress);
    gtk_widget_hide (priv->area_progress);
    return;
}


static void
eog_toolbar_plugin_activate (EogWindowActivatable *activatable)
{
    EogToolbarPlugin *plugin = EOG_TOOLBAR_PLUGIN(activatable);
    EogToolbarPluginPrivate *priv = plugin->priv;

    EogWindow *window = plugin->window;
    g_assert (EOG_IS_WINDOW(window));

    gtk_window_set_decorated (GTK_WINDOW (window), FALSE);

    /* Add application specific icons to search path */
    gtk_icon_theme_append_search_path (gtk_icon_theme_get_default (),
                                       TOOLBAR_DATA_DIR G_DIR_SEPARATOR_S "icons");

    GtkCssProvider *provider = gtk_css_provider_new();
    gtk_css_provider_load_from_resource(provider, "/org/gnome/eog/ui/eog-plugin-toolbar.css");
    gtk_style_context_add_provider_for_screen (gdk_screen_get_default(),
                                               GTK_STYLE_PROVIDER (provider),
                                               GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
    g_object_unref (provider);

    const GActionEntry new_rotation_funcs[] = {
            {"rotate-90", rotation_right, NULL, NULL, NULL },
            {"rotate-270", rotation_left, NULL, NULL, NULL }
    };

    g_action_map_remove_action (G_ACTION_MAP(window), "rotate-90");
    g_action_map_remove_action (G_ACTION_MAP(window), "rotate-270");

    g_action_map_add_action_entries (G_ACTION_MAP (window),
                                     new_rotation_funcs, G_N_ELEMENTS (new_rotation_funcs),
                                     window);

    priv->act_left = g_action_map_lookup_action(G_ACTION_MAP(window), "go-previous");
    priv->act_right = g_action_map_lookup_action(G_ACTION_MAP(window), "go-next");
    g_assert (priv->act_right != NULL && priv->act_left != NULL);

    GtkWidget* hbox = eog_window_get_boxtool(window);

    GtkWidget *eog_thumbview = eog_window_get_thumb_view (window);
//    g_signal_connect (eog_thumbview, "selection_changed", G_CALLBACK (hide_items_in_read_only), plugin);
    gtk_icon_view_set_activate_on_single_click(GTK_ICON_VIEW(eog_thumbview), TRUE);
//    gtk_icon_view_set_selection_mode(GTK_ICON_VIEW(eog_thumbview), GTK_SELECTION_BROWSE);

    GError *error = NULL;
    GdkPixbuf *pix = gdk_pixbuf_new_from_resource_at_scale("/org/gnome/eog/ui/icons/spinner.png", priv->size_throbber, priv->size_throbber, TRUE, &error);
    if (error || !pix){
        g_warning("(%s - %s) Could not load throbber : %s", __FILE__, __func__, error ? error->message : "");
        g_clear_error(&error);
    }

    priv->throbber = gdk_cairo_surface_create_from_pixbuf(pix, 0, NULL);
    if (cairo_surface_status(priv->throbber) != CAIRO_STATUS_SUCCESS)
        g_warning("(%s - %s) >Failed to create cairo_surface, status : %d", __FILE__, __func__, cairo_surface_status(priv->throbber));

    GtkWidget *view = eog_window_get_view(window);
    GtkWidget *overlay = gtk_widget_get_parent(view);
    g_assert (GTK_IS_OVERLAY(overlay));

    priv->area_progress = gtk_drawing_area_new();
    gtk_overlay_add_overlay(GTK_OVERLAY(overlay), priv->area_progress);

    priv->stack_toolbars = eog_utils_get_widget_by_name(hbox, "stack_toolbar");
    if (!priv->stack_toolbars) {
        priv->stack_toolbars = gtk_stack_new();
        gtk_widget_set_name(priv->stack_toolbars, "stack_toolbar");
    }

    priv->toolbar = eog_main_toolbar_new(window, GTK_STACK(priv->stack_toolbars));
    gtk_toolbar_set_show_arrow (GTK_TOOLBAR(priv->toolbar), FALSE);

    priv->scroll = gtk_scrolled_window_new(NULL, NULL);
    gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(priv->scroll), GTK_POLICY_EXTERNAL, GTK_POLICY_NEVER);
    gtk_container_add (GTK_CONTAINER (priv->scroll), priv->toolbar);

    gtk_stack_add_named(GTK_STACK(priv->stack_toolbars), priv->scroll, "main-toolbar");

    priv->left_button = gtk_button_new_from_icon_name ("go-previous-symbolic",
                                                         GTK_ICON_SIZE_BUTTON);
    g_signal_connect (priv->left_button, "button-press-event", G_CALLBACK(scroll_button), plugin);

    priv->right_button = gtk_button_new_from_icon_name ("go-next-symbolic",
                                                          GTK_ICON_SIZE_BUTTON);
    g_signal_connect ( priv->right_button, "button-press-event", G_CALLBACK(scroll_button), plugin);

    priv->box_toolbar = eog_utils_get_widget_by_name(hbox, "box_toolbar");
    if (!priv->box_toolbar) {
        priv->box_toolbar = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
        gtk_widget_set_name(priv->box_toolbar, "box_toolbar");

        gtk_box_pack_start(GTK_BOX(priv->box_toolbar), priv->stack_toolbars, TRUE, TRUE, 0);
        gtk_box_pack_start(GTK_BOX(hbox), priv->box_toolbar, TRUE, TRUE, 0);
    }

    gtk_box_pack_start(GTK_BOX(priv->box_toolbar), priv->left_button, FALSE, FALSE, 0);
    gtk_box_reorder_child (GTK_BOX(priv->box_toolbar), priv->left_button, 0);

    gtk_box_pack_end(GTK_BOX(priv->box_toolbar), priv->right_button, FALSE, FALSE, 0);
    gtk_box_reorder_child (GTK_BOX(priv->box_toolbar), priv->right_button, -1);

//    g_signal_connect (plugin->scroll, "edge-reached", G_CALLBACK(scroll_reached), plugin);

    priv->id_prepared = g_signal_connect (window, "prepared", G_CALLBACK (display_toolbar), plugin);

    gtk_widget_show_all(hbox);
    gtk_widget_hide (priv->area_progress);

    return;
}

static void
eog_toolbar_plugin_deactivate (EogWindowActivatable *activatable)
{
    return;
}

static void
eog_toolbar_plugin_class_init (EogToolbarPluginClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);

    object_class->dispose = eog_toolbar_plugin_dispose;
    object_class->set_property = eog_toolbar_plugin_set_property;
    object_class->get_property = eog_toolbar_plugin_get_property;

    g_object_class_override_property (object_class, PROP_WINDOW, "window");
/*
    bind_textdomain_codeset (ET_PACKAGE, "UTF-8");
    textdomain (ET_PACKAGE);*/
    return;
}

static void
eog_toolbar_plugin_class_finalize (EogToolbarPluginClass *klass)
{
}


static void
eog_window_activatable_iface_init (EogWindowActivatableInterface *iface)
{
    iface->activate = eog_toolbar_plugin_activate;
    iface->deactivate = eog_toolbar_plugin_deactivate;
}

G_MODULE_EXPORT void
peas_register_types (PeasObjectModule *module)
{
    eog_toolbar_plugin_register_type(G_TYPE_MODULE (module));
    peas_object_module_register_extension_type(module,
                                               EOG_TYPE_WINDOW_ACTIVATABLE,
                                               EOG_TYPE_TOOLBAR_PLUGIN);
}
