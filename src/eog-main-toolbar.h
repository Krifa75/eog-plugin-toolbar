/* EogToolbar - Plugin.

   Copyright (C) 2019 Yahiaoui Fakhri

   This file is part of the plugin for Eog.

   EogToolbar is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License as published by the Free
   Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   EogToolbar is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
   for more details.

   You should have received a copy of the GNU General Public License
   along with EogImagesManager; see the file COPYING.  If not, write to the Free
   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   This file implements a toolbar for eog.  */

#ifndef EOG_MAIN_TOOLBAR_H
#define EOG_MAIN_TOOLBAR_H

#include <eog-3.0/eog/eog-window.h>
#include <gtk/gtk.h>

G_BEGIN_DECLS

/*
 * Type checking and casting macros
 */
#define EOG_TYPE_MAIN_TOOLBAR		(eog_main_toolbar_get_type ())
#define EOG_MAIN_TOOLBAR(o)		(G_TYPE_CHECK_INSTANCE_CAST ((o), EOG_TYPE_MAIN_TOOLBAR, EogMainToolbar))
#define EOG_MAIN_TOOLBAR_CLASS(k)		G_TYPE_CHECK_CLASS_CAST((k),      EOG_TYPE_MAIN_TOOLBAR, EogMainToolbarClass))
#define EOG_IS_MAIN_TOOLBAR(o)	        (G_TYPE_CHECK_INSTANCE_TYPE ((o), EOG_TYPE_MAIN_TOOLBAR))
#define EOG_IS_MAIN_TOOLBAR_CLASS(k)	(G_TYPE_CHECK_CLASS_TYPE ((k),    EOG_TYPE_MAIN_TOOLBAR))
#define EOG_MAIN_TOOLBAR_GET_CLASS(o)	(G_TYPE_INSTANCE_GET_CLASS ((o),  EOG_TYPE_MAIN_TOOLBAR, EogMainToolbarClass))

typedef struct _EogMainToolbar EogMainToolbar;
typedef struct _EogMainToolbarClass _EogMainToolbarClass;
typedef struct _EogMainToolbarPrivate EogMainToolbarPrivate;

struct _EogMainToolbar
{
    GtkToolbar parent_instance;

    EogMainToolbarPrivate *priv;
};

/*
 * Class definition
 */
typedef struct _EogMainToolbarClass	EogMainToolbarClass;

struct _EogMainToolbarClass
{
    GtkToolbarClass parent_class;
};

/*
 * Public methods
 */
GType	eog_main_toolbar_get_type		(void) G_GNUC_CONST;

GtkWidget *eog_main_toolbar_new (EogWindow *window, GtkStack *stack);

void eog_main_toolbar_hide_gallery (EogMainToolbar *toolbar, gboolean visible);
void eog_main_toolbar_hide_filters (EogMainToolbar *toolbar, gboolean is_visible);
gboolean eog_main_toolbar_filters_is_hidden (EogMainToolbar *toolbar);

G_END_DECLS

#endif //EOG_MAIN_TOOLBAR_H