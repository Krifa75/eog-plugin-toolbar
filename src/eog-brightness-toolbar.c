/* EogToolbar - Plugin.

   Copyright (C) 2019 Yahiaoui Fakhri

   This file is part of the plugin for Eog.

   EogToolbar is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License as published by the Free
   Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   EogToolbar is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
   for more details.

   You should have received a copy of the GNU General Public License
   along with EogImagesManager; see the file COPYING.  If not, write to the Free
   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   This file implements a toolbar for eog.  */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <eog-3.0/eog/eog-thumb-view.h>
#include <glib/gi18n.h>
#include <math.h>
#include <eog/eog-job-scheduler.h>
#include "eog-brightness-toolbar.h"
#include "eog-utils.h"
#include "eog-jobs-filter.h"

struct _EogBrightnessToolbarPrivate {
    EogWindow *window;
    GtkStack *stack;
    GtkWidget *area_progress;
    GdkPixbuf *orig_pix;
    gdouble brightness_value;
};

G_DEFINE_TYPE_WITH_PRIVATE (EogBrightnessToolbar, eog_brightness_toolbar, GTK_TYPE_TOOLBAR)

enum {
    PROP_0,
    PROP_WINDOW,
    PROP_STACK,
    PROP_AREA,
};

static void
eog_brightness_toolbar_constructed (GObject *object)
{
    if (G_OBJECT_CLASS (eog_brightness_toolbar_parent_class)->constructed)
        G_OBJECT_CLASS (eog_brightness_toolbar_parent_class)->constructed (object);

    return;
}

static void
eog_brightness_toolbar_dispose (GObject *object)
{

    G_OBJECT_CLASS (eog_brightness_toolbar_parent_class)->dispose (object);
    return;
}

static void
eog_brightness_toolbar_get_property (GObject    *object,
                                 guint       prop_id,
                                 GValue     *value,
                                 GParamSpec *pspec)
{
    EogBrightnessToolbarPrivate *priv = EOG_BRIGHTNESS_TOOLBAR(object)->priv;

    switch (prop_id){
        case PROP_WINDOW:
            g_value_set_object(value, priv->window);
            break;
        case PROP_STACK:
            g_value_set_object(value, priv->stack);
            break;
        case PROP_AREA:
            g_value_set_object(value, priv->area_progress);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
eog_brightness_toolbar_set_property (GObject    *object,
                                 guint       prop_id,
                                 const GValue     *value,
                                 GParamSpec *pspec)
{
    EogBrightnessToolbarPrivate *priv = EOG_BRIGHTNESS_TOOLBAR(object)->priv;

    EogImage *img = NULL;
    switch (prop_id){
        case PROP_WINDOW:
            priv->window = EOG_WINDOW(g_value_dup_object (value));
            img = eog_window_get_image(priv->window);
            priv->orig_pix = gdk_pixbuf_copy(eog_image_get_pixbuf(img));
            break;
        case PROP_STACK:
            priv->stack = GTK_STACK(g_value_dup_object (value));
            break;
        case PROP_AREA:
            priv->area_progress = GTK_WIDGET(g_value_get_object (value));
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }

    if (img)
        g_object_unref(img);
}

static void
eog_brightness_toolbar_class_init (EogBrightnessToolbarClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);

    object_class->constructed = eog_brightness_toolbar_constructed;
    object_class->dispose = eog_brightness_toolbar_dispose;
    object_class->set_property = eog_brightness_toolbar_set_property;
    object_class->get_property = eog_brightness_toolbar_get_property;

    g_object_class_install_property(object_class, PROP_WINDOW, g_param_spec_object("window", "", "", EOG_TYPE_WINDOW, G_PARAM_READWRITE));
    g_object_class_install_property(object_class, PROP_STACK, g_param_spec_object("stack", "", "", GTK_TYPE_STACK, G_PARAM_READWRITE));
    g_object_class_install_property(object_class, PROP_AREA, g_param_spec_object("area-progress", "", "", GTK_TYPE_DRAWING_AREA,
                                                                                 (G_PARAM_WRITABLE |
                                                                                  G_PARAM_CONSTRUCT_ONLY |
                                                                                  G_PARAM_STATIC_STRINGS)));
    bind_textdomain_codeset (ET_PACKAGE, "UTF-8");
    textdomain (ET_PACKAGE);
    return;
}

void
eog_brightness_set_current_image (EogBrightnessToolbar *toolbar, EogImage *current_image)
{
    g_return_if_fail(EOG_IS_BRIGHTNESS_TOOLBAR(toolbar));

    if (toolbar->priv->orig_pix != NULL) {
        g_object_unref(toolbar->priv->orig_pix);
        toolbar->priv->orig_pix = NULL;
    }

    GdkPixbuf *pix = eog_image_get_pixbuf(current_image);
    toolbar->priv->orig_pix = gdk_pixbuf_copy(pix);
    g_object_unref (pix);

    return;
}

static void
reset_pixbuf_image (GdkPixbuf *pix, const guchar *pixels)
{
    int i, size = gdk_pixbuf_get_height(pix) * gdk_pixbuf_get_rowstride (pix);
    guchar *data = gdk_pixbuf_get_pixels (pix);

    for (i=0; i < size; i++)
        data[i] = pixels[i];

    return;
}

static void
button_brightness_cancel_cb (GtkToolButton *toolbutton,
                         gpointer       data)
{
    g_return_if_fail(EOG_IS_BRIGHTNESS_TOOLBAR(data));

    EogBrightnessToolbarPrivate *priv = EOG_BRIGHTNESS_TOOLBAR(data)->priv;
    EogWindow *window = priv->window;

    EogImage *image = eog_window_get_image(window);
    if(image == NULL)
        return;

    if (priv->brightness_value != 1) {
        GdkPixbuf *pix = eog_image_get_pixbuf(image);

        reset_pixbuf_image(pix, gdk_pixbuf_get_pixels (priv->orig_pix));
    }

    priv->brightness_value = 1;

    eog_thumb_view_select_single(EOG_THUMB_VIEW (eog_window_get_thumb_view(window)), EOG_THUMB_VIEW_SELECT_CURRENT);
    eog_image_modified(image);

    GtkStack *stack = EOG_BRIGHTNESS_TOOLBAR(data)->priv->stack;
    g_return_if_fail(GTK_IS_STACK(stack));

    gtk_stack_set_visible_child_full(GTK_STACK(stack), "main-toolbar", GTK_STACK_TRANSITION_TYPE_OVER_RIGHT);
    eog_utils_set_sensitive_widgets (window, TRUE);
    return;
}

static GtkToolItem *
item_button_brightness_cancel (EogBrightnessToolbar *toolbar)
{
    g_return_val_if_fail(EOG_IS_BRIGHTNESS_TOOLBAR(toolbar), NULL);

    GtkWidget *img_cancel = gtk_image_new_from_icon_name("edit-undo", GTK_ICON_SIZE_BUTTON);
    GtkToolItem *ss = gtk_tool_button_new(img_cancel, _("Cancel"));

    g_signal_connect(GTK_TOOL_BUTTON(ss), "clicked", G_CALLBACK(button_brightness_cancel_cb), toolbar);

    return ss;
}

static void job_filter_update_brightness_finished_cb (EogJobBrightnessFilter *job, gpointer data)
{
    eog_image_modified(job->image);

    gtk_widget_hide (EOG_BRIGHTNESS_TOOLBAR(data)->priv->area_progress);
    gtk_widget_set_sensitive(GTK_WIDGET(EOG_BRIGHTNESS_TOOLBAR(data)->priv->stack), TRUE);

    return;
}

static void
update_brightness_image (EogImage *image, EogBrightnessToolbar *toolbar)
{
    EogBrightnessToolbarPrivate *priv = toolbar->priv;

    gtk_widget_show_all (priv->area_progress);
    if (gtk_widget_get_visible (priv->area_progress)) {
        gtk_widget_set_sensitive(GTK_WIDGET(priv->stack), FALSE);
        g_message ("Brightness value : %lf", priv->brightness_value);
        EogJob *job = eog_job_brightness_filter_new(image, priv->orig_pix, priv->brightness_value);
        g_signal_connect (job, "finished", G_CALLBACK(job_filter_update_brightness_finished_cb), toolbar);
        eog_job_scheduler_add_job(job);
        g_object_unref(job);
    }
    return;
}

static void
button_brightness_minus_cb (GtkToolButton *toolbutton,
                            gpointer       data)
{
    g_return_if_fail(EOG_IS_BRIGHTNESS_TOOLBAR(data));

    EogBrightnessToolbarPrivate *priv = EOG_BRIGHTNESS_TOOLBAR(data)->priv;
    EogWindow *window = priv->window;
    g_return_if_fail(EOG_IS_WINDOW(window));

    EogImage *image = eog_window_get_image(window);
    if(image == NULL) {
        g_warning ("No image on the window");
        return;
    }

    priv->brightness_value -= 0.2f;

    update_brightness_image (image, EOG_BRIGHTNESS_TOOLBAR(data));

    return;
}

static void
button_brightness_plus_cb (GtkToolButton *toolbutton,
                           gpointer       data)
{
    g_return_if_fail(EOG_IS_BRIGHTNESS_TOOLBAR(data));

    EogBrightnessToolbarPrivate *priv = EOG_BRIGHTNESS_TOOLBAR(data)->priv;
    EogWindow *window = priv->window;
    g_return_if_fail(EOG_IS_WINDOW(window));

    EogImage *image = eog_window_get_image(window);
    if(image == NULL) {
        g_warning ("No image on the window");
        return;
    }

    priv->brightness_value += 0.2f;

    update_brightness_image (image, EOG_BRIGHTNESS_TOOLBAR(data));

    return;
}

static GtkToolItem *
item_button_brightness_minus (EogBrightnessToolbar *toolbar)
{
    g_return_val_if_fail(EOG_IS_BRIGHTNESS_TOOLBAR(toolbar), NULL);

    GtkWidget *img_minus = gtk_image_new_from_icon_name("list-remove", GTK_ICON_SIZE_BUTTON);
    GtkToolItem *ss = gtk_tool_button_new(img_minus, _("Minus"));

    g_signal_connect(GTK_TOOL_BUTTON(ss), "clicked", G_CALLBACK(button_brightness_minus_cb), toolbar);

    return ss;
}

static GtkToolItem *
item_button_brightness_plus (EogBrightnessToolbar *toolbar)
{
    g_return_val_if_fail(EOG_IS_BRIGHTNESS_TOOLBAR(toolbar), NULL);

    GtkWidget *img_plus = gtk_image_new_from_icon_name("list-add", GTK_ICON_SIZE_BUTTON);
    GtkToolItem *ss = gtk_tool_button_new(img_plus, _("Plus"));

    g_signal_connect(GTK_TOOL_BUTTON(ss), "clicked", G_CALLBACK(button_brightness_plus_cb), toolbar);

    return ss;
}

static void
job_filter_brightness_finished_cb (EogJobBrightnessFilter *job, gpointer data)
{
    GFile *file = eog_image_get_file (job->image);
    const gchar *path_file = g_file_get_parse_name(file);
    eog_utils_save_image (EOG_BRIGHTNESS_TOOLBAR(data)->priv->window, job->pixbuf_output, g_file_get_parent(file), path_file, "Brightness", TRUE);

    gtk_widget_hide (EOG_BRIGHTNESS_TOOLBAR(data)->priv->area_progress);
    gtk_widget_set_sensitive(GTK_WIDGET(EOG_BRIGHTNESS_TOOLBAR(data)->priv->stack), TRUE);
    eog_utils_set_sensitive_widgets (EOG_BRIGHTNESS_TOOLBAR(data)->priv->window, TRUE);
    button_brightness_cancel_cb (NULL, data);

    return;
}

static void
button_brightness_validate_cb (GtkToolButton *toolbutton,
                               gpointer       data)
{
    g_return_if_fail(EOG_IS_BRIGHTNESS_TOOLBAR(data));

    EogBrightnessToolbarPrivate *priv = EOG_BRIGHTNESS_TOOLBAR(data)->priv;
    EogWindow *window = priv->window;

    EogImage *image = eog_window_get_image(window);
    if(image == NULL)
        return;

    if (priv->brightness_value != 1) {
        gtk_widget_show_all (priv->area_progress);
        EogJob *job = eog_job_brightness_filter_new(image, priv->orig_pix, priv->brightness_value);
        g_signal_connect (job, "finished", G_CALLBACK(job_filter_brightness_finished_cb), data);
        eog_job_scheduler_add_job(job);
        g_object_unref(job);
    }

}

static GtkToolItem *
item_button_brightness_validate (EogBrightnessToolbar *toolbar)
{
    g_return_val_if_fail(EOG_IS_BRIGHTNESS_TOOLBAR(toolbar), NULL);

    GtkWidget *img_plus = gtk_image_new_from_icon_name("edit-redo", GTK_ICON_SIZE_BUTTON);
    GtkToolItem *ss = gtk_tool_button_new(img_plus, _("Validate"));

    g_signal_connect(GTK_TOOL_BUTTON(ss), "clicked", G_CALLBACK(button_brightness_validate_cb), toolbar);

    return ss;
}

static void
eog_brightness_toolbar_init (EogBrightnessToolbar *m_toolbars)
{
    m_toolbars->priv = eog_brightness_toolbar_get_instance_private(m_toolbars);
    //EogBrightnessToolbarPrivate *priv = m_toolbars->priv;
    m_toolbars->priv->brightness_value = 1.f;
    gtk_toolbar_set_style(GTK_TOOLBAR(m_toolbars), GTK_TOOLBAR_BOTH);
    gtk_toolbar_insert(GTK_TOOLBAR(m_toolbars), item_button_brightness_cancel (m_toolbars), 1);
    gtk_toolbar_insert(GTK_TOOLBAR(m_toolbars), item_button_brightness_minus (m_toolbars), 2);
    gtk_toolbar_insert(GTK_TOOLBAR(m_toolbars), item_button_brightness_plus (m_toolbars), 3);
    gtk_toolbar_insert(GTK_TOOLBAR(m_toolbars), item_button_brightness_validate (m_toolbars), 4);
}

GtkWidget *
eog_brightness_toolbar_new (EogWindow *window, GtkStack *stack, GtkWidget *area_progress)
{
    return g_object_new (EOG_TYPE_BRIGHTNESS_TOOLBAR, "window", window, "stack", stack, "area-progress", area_progress, NULL);
}
