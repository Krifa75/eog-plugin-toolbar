/* EogToolbar - Plugin.

   Copyright (C) 2019 Yahiaoui Fakhri

   This file is part of the plugin for Eog.

   EogToolbar is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License as published by the Free
   Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   EogToolbar is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
   for more details.

   You should have received a copy of the GNU General Public License
   along with EogImagesManager; see the file COPYING.  If not, write to the Free
   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   This file implements a toolbar for eog.  */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <eog-3.0/eog/eog-scroll-view.h>
#include <eog-3.0/eog/eog-thumb-view.h>
#include "eog-reduce-toolbar.h"
#include "eog-utils.h"
#include "utils/exif-jpg.h"
#include <glib/gi18n.h>

#define REDUCE_LITTLE 0.25
#define REDUCE_MIDDLE 0.5
#define REDUCE_BIG 0.75

struct _EogReduceToolbarPrivate {
    EogWindow *window;
    GtkStack *stack;
    GtkWidget *area_progress;
    gchar *button_clicked;
};

G_DEFINE_TYPE_WITH_PRIVATE (EogReduceToolbar, eog_reduce_toolbar, GTK_TYPE_TOOLBAR)

enum {
    PROP_0,
    PROP_WINDOW,
    PROP_STACK,
    PROP_AREA,
};

static void
eog_reduce_toolbar_constructed (GObject *object)
{
    if (G_OBJECT_CLASS (eog_reduce_toolbar_parent_class)->constructed)
        G_OBJECT_CLASS (eog_reduce_toolbar_parent_class)->constructed (object);

    return;
}

static void
eog_reduce_toolbar_dispose (GObject *object)
{

    G_OBJECT_CLASS (eog_reduce_toolbar_parent_class)->dispose (object);
    return;
}

static void
eog_reduce_toolbar_get_property (GObject    *object,
                               guint       prop_id,
                               GValue     *value,
                               GParamSpec *pspec)
{
    EogReduceToolbarPrivate *priv = EOG_REDUCE_TOOLBAR(object)->priv;

    switch (prop_id){
        case PROP_WINDOW:
            g_value_set_object(value, priv->window);
            break;
        case PROP_STACK:
            g_value_set_object(value, priv->stack);
            break;
        case PROP_AREA:
            g_value_set_object(value, priv->area_progress);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
eog_reduce_toolbar_set_property (GObject    *object,
                               guint       prop_id,
                               const GValue     *value,
                               GParamSpec *pspec)
{
    EogReduceToolbarPrivate *priv = EOG_REDUCE_TOOLBAR(object)->priv;

    switch (prop_id){
        case PROP_WINDOW:
            priv->window = EOG_WINDOW(g_value_dup_object (value));
            break;
        case PROP_STACK:
            priv->stack = GTK_STACK(g_value_dup_object (value));
            break;
        case PROP_AREA:
            priv->area_progress = GTK_WIDGET(g_value_get_object (value));
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
eog_reduce_toolbar_class_init (EogReduceToolbarClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);

    object_class->constructed = eog_reduce_toolbar_constructed;
    object_class->dispose = eog_reduce_toolbar_dispose;
    object_class->set_property = eog_reduce_toolbar_set_property;
    object_class->get_property = eog_reduce_toolbar_get_property;

    g_object_class_install_property(object_class, PROP_WINDOW, g_param_spec_object("window", "", "", EOG_TYPE_WINDOW, G_PARAM_READWRITE |
                                                                                                                      G_PARAM_CONSTRUCT_ONLY |
                                                                                                                      G_PARAM_STATIC_STRINGS));
    g_object_class_install_property(object_class, PROP_STACK, g_param_spec_object("stack", "", "", GTK_TYPE_STACK, G_PARAM_READWRITE));
    g_object_class_install_property(object_class, PROP_AREA, g_param_spec_object("area-progress", "", "", GTK_TYPE_DRAWING_AREA,
                                                                                 (G_PARAM_WRITABLE |
                                                                                  G_PARAM_CONSTRUCT_ONLY |
                                                                                  G_PARAM_STATIC_STRINGS)));
    bind_textdomain_codeset (ET_PACKAGE, "UTF-8");
    textdomain (ET_PACKAGE);
    return;
}

static void
button_reduce_cancel_cb (GtkToolButton *toolbutton,
                         gpointer       data)
{
    g_return_if_fail(EOG_IS_REDUCE_TOOLBAR(data));

    GtkStack *stack = EOG_REDUCE_TOOLBAR(data)->priv->stack;
    g_return_if_fail(GTK_IS_STACK(stack));

    eog_utils_set_sensitive_widgets (EOG_REDUCE_TOOLBAR(data)->priv->window, TRUE);
    gtk_stack_set_visible_child_full(GTK_STACK(stack), "main-toolbar", GTK_STACK_TRANSITION_TYPE_OVER_RIGHT);
    return;
}

static GtkToolItem *
item_button_reduce_cancel (EogReduceToolbar *toolbar)
{
    g_return_val_if_fail(EOG_IS_REDUCE_TOOLBAR(toolbar), NULL);

    GtkWidget *img_cancel = gtk_image_new_from_icon_name("edit-undo", GTK_ICON_SIZE_BUTTON);
    GtkToolItem *ss = gtk_tool_button_new(img_cancel, _("Cancel"));

    g_signal_connect(GTK_TOOL_BUTTON(ss), "clicked", G_CALLBACK(button_reduce_cancel_cb), toolbar);

    return ss;
}

static gpointer
apply_reduce (gpointer       data)
{
    g_return_val_if_fail(EOG_IS_REDUCE_TOOLBAR(data), NULL);

    EogReduceToolbarPrivate *priv = EOG_REDUCE_TOOLBAR(data)->priv;
    EogWindow *window = priv->window;

    EogImage *image = eog_window_get_image(window);
    if(image == NULL)
        return NULL;

    GError *error = NULL;
    GFile *filename = eog_image_get_file(image);
    GdkPixbuf *pix = gdk_pixbuf_new_from_file(g_file_get_parse_name(filename), &error);
    pix = gdk_pixbuf_apply_embedded_orientation(pix);
    if (error){
        g_critical("%s : Error while loading image %s. \nError : %s\n", __func__, g_file_get_parse_name(filename), error->message);
        g_clear_error(&error);
    }

//    const gchar *label = gtk_tool_button_get_label(priv->button_clicked);

    gint width, height;
    eog_image_get_size(image, &width, &height);

    if (!g_strcmp0(priv->button_clicked, "little")){
        width *= REDUCE_LITTLE;
        height *= REDUCE_LITTLE;
    }
    else if (!g_strcmp0(priv->button_clicked, "average")){
        width *= REDUCE_MIDDLE;
        height *= REDUCE_MIDDLE;
    }
    else if (!g_strcmp0(priv->button_clicked, "big")){
        width *= REDUCE_BIG;
        height *= REDUCE_BIG;
    }

    GdkPixbuf *pix_scaled = gdk_pixbuf_scale_simple(pix, width, height, GDK_INTERP_NEAREST);
    if (pix_scaled){
        eog_utils_save_image (window, pix_scaled, g_file_get_parent(filename), g_file_get_parse_name(filename), g_strdup(priv->button_clicked), TRUE);

//        g_object_unref(image);
        g_object_unref(filename);
    }

    button_reduce_cancel_cb (NULL, data);
    return NULL;
}

static gboolean
set_thread_reduce (gpointer data)
{
    g_return_val_if_fail (EOG_REDUCE_TOOLBAR(data), G_SOURCE_REMOVE);

    GThread *thread = g_thread_new (NULL,
                                    apply_reduce,
                                    data);
    g_thread_join(thread);

    gtk_widget_hide (EOG_REDUCE_TOOLBAR(data)->priv->area_progress);

    return G_SOURCE_REMOVE;
};

static void
button_reduce_cb (GtkToolButton *toolbutton,
                  gpointer       data)
{
    g_return_if_fail(EOG_IS_REDUCE_TOOLBAR(data));

    EogReduceToolbarPrivate *priv = EOG_REDUCE_TOOLBAR(data)->priv;
    EogWindow *window = priv->window;

    EogImage *image = eog_window_get_image(window);
    if (image == NULL)
        return;

    gtk_widget_show_all (priv->area_progress);
    if (gtk_widget_get_visible (priv->area_progress)) {
        if (priv->button_clicked)
            g_free (priv->button_clicked);
        priv->button_clicked = g_strdup(gtk_tool_button_get_label(toolbutton));
        g_timeout_add(500, set_thread_reduce, data);
    }

}
static GtkToolItem *
item_button_reduce_little (EogReduceToolbar *toolbar)
{
    g_return_val_if_fail(EOG_IS_REDUCE_TOOLBAR(toolbar), NULL);

    GtkWidget *img_little = gtk_image_new_from_icon_name("reduce-little", GTK_ICON_SIZE_BUTTON);
    GtkToolItem *ss = gtk_tool_button_new(img_little, _("little"));

    g_signal_connect(GTK_TOOL_BUTTON(ss), "clicked", G_CALLBACK(button_reduce_cb), toolbar);

    return ss;
}

static GtkToolItem *
item_button_reduce_middle (EogReduceToolbar *toolbar)
{
    g_return_val_if_fail(EOG_IS_REDUCE_TOOLBAR(toolbar), NULL);

    GtkWidget *img_middle = gtk_image_new_from_icon_name("reduce-average", GTK_ICON_SIZE_BUTTON);
    GtkToolItem *ss = gtk_tool_button_new(img_middle, _("average"));

    g_signal_connect(GTK_TOOL_BUTTON(ss), "clicked", G_CALLBACK(button_reduce_cb), toolbar);

    return ss;
}

static GtkToolItem *
item_button_reduce_big (EogReduceToolbar *toolbar)
{
    g_return_val_if_fail(EOG_IS_REDUCE_TOOLBAR(toolbar), NULL);

    GtkWidget *img_big = gtk_image_new_from_icon_name("reduce-big", GTK_ICON_SIZE_BUTTON);
    GtkToolItem *ss = gtk_tool_button_new(img_big, _("big"));

    g_signal_connect(GTK_TOOL_BUTTON(ss), "clicked", G_CALLBACK(button_reduce_cb), toolbar);

    return ss;
}

static void
eog_reduce_toolbar_init (EogReduceToolbar *m_toolbars)
{
    m_toolbars->priv = eog_reduce_toolbar_get_instance_private(m_toolbars);
    //EogReduceToolbarPrivate *priv = m_toolbars->priv;
    gtk_toolbar_set_style(GTK_TOOLBAR(m_toolbars), GTK_TOOLBAR_BOTH);
    gtk_toolbar_insert(GTK_TOOLBAR(m_toolbars), item_button_reduce_cancel (m_toolbars), 1);
    gtk_toolbar_insert(GTK_TOOLBAR(m_toolbars), item_button_reduce_little (m_toolbars), 2);
    gtk_toolbar_insert(GTK_TOOLBAR(m_toolbars), item_button_reduce_middle (m_toolbars), 3);
    gtk_toolbar_insert(GTK_TOOLBAR(m_toolbars), item_button_reduce_big (m_toolbars), 4);

    m_toolbars->priv->button_clicked = NULL;
}

GtkWidget *
eog_reduce_toolbar_new (EogWindow *window, GtkStack *stack, GtkWidget *area_progress)
{
    return g_object_new (EOG_TYPE_REDUCE_TOOLBAR, "window", window, "stack", stack, "area-progress", area_progress, NULL);
}
