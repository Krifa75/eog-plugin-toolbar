/* EogToolbar - Plugin.

   Copyright (C) 2019 Yahiaoui Fakhri

   This file is part of the plugin for Eog.

   EogToolbar is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License as published by the Free
   Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   EogToolbar is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
   for more details.

   You should have received a copy of the GNU General Public License
   along with EogImagesManager; see the file COPYING.  If not, write to the Free
   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   This file implements a toolbar for eog.  */

#ifndef EOG_SELECTION_TOOL_H
#define EOG_SELECTION_TOOL_H

#include <gtk/gtk.h>
#include <eog-3.0/eog/eog-image.h>

G_BEGIN_DECLS

/*
 * Type checking and casting macros
 */
#define EOG_TYPE_SELECTION		(eog_selection_get_type ())
#define EOG_SELECTION(o)		(G_TYPE_CHECK_INSTANCE_CAST ((o), EOG_TYPE_SELECTION, EogSelection))
#define EOG_SELECTION_CLASS(k)		G_TYPE_CHECK_CLASS_CAST((k),      EOG_TYPE_SELECTION, EogSelectionClass))
#define EOG_IS_SELECTION(o)	        (G_TYPE_CHECK_INSTANCE_TYPE ((o), EOG_TYPE_SELECTION))
#define EOG_IS_SELECTION_CLASS(k)	(G_TYPE_CHECK_CLASS_TYPE ((k),    EOG_TYPE_SELECTION))
#define EOG_SELECTION_GET_CLASS(o)	(G_TYPE_INSTANCE_GET_CLASS ((o),  EOG_TYPE_SELECTION, EogSelectionClass))

typedef struct _EogSelection EogSelection;
typedef struct _EogSelectionClass _EogSelectionClass;
typedef struct _EogSelectionPrivate EogSelectionPrivate;

struct _EogSelection
{
    GtkDrawingArea parent_instance;

    GSList *l_coords;
    EogSelectionPrivate *priv;
};

/*
 * Class definition
 */
typedef struct _EogSelectionClass	EogSelectionClass;

struct _EogSelectionClass
{
    GtkDrawingAreaClass parent_class;
};

/*
 * Public methods
 */
GType	eog_selection_get_type		(void) G_GNUC_CONST;

GtkWidget *eog_selection_new (GtkWidget *view);
void eog_selection_set_func_filter (GtkWidget *selection, gboolean (*func_filter) (EogSelection *sel, GtkWidget *view, GdkRectangle selection));
G_END_DECLS

#endif //EOG_SELECTION_TOOL_H
