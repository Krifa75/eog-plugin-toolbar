/* EogToolbar - Plugin.

   Copyright (C) 2019 Yahiaoui Fakhri

   This file is part of the plugin for Eog.

   EogToolbar is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License as published by the Free
   Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   EogToolbar is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
   for more details.

   You should have received a copy of the GNU General Public License
   along with EogImagesManager; see the file COPYING.  If not, write to the Free
   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   This file implements a toolbar for eog.  */

#include "eog-selection-tool.h"
#include "utils/redeyes-filter.h"
#include <eog-3.0/eog/eog-scroll-view.h>
//#include <stdlib.h>
#include <math.h>

struct _EogSelectionPrivate {
    GtkWidget *eog_view;
    GtkWidget *eog_nav;

    GtkAllocation alloc_view;
    gint width, height;

    gdouble zoom;
    gint offset_x, offset_y;

    GdkRectangle select_area;
    GdkRectangle select_img;

    gboolean (*func_filter) (EogSelection *sel, GtkWidget *view, GdkRectangle selection);
};

G_DEFINE_TYPE_WITH_PRIVATE (EogSelection, eog_selection, GTK_TYPE_DRAWING_AREA)

enum {
    PROP_0,
    PROP_VIEW,
    PROP_NAV
};

static gboolean clicked_cb(GtkWidget *widget, GdkEventButton *event, gpointer user_data);
static gboolean motion_notify_event_cb (GtkWidget *widget, GdkEventMotion *event, gpointer data);
static gboolean draw_cb (GtkWidget *widget, cairo_t *cr, gpointer data);

static void
eog_selection_constructed (GObject *object)
{
    if (G_OBJECT_CLASS (eog_selection_parent_class)->constructed)
        G_OBJECT_CLASS (eog_selection_parent_class)->constructed (object);

    EogSelection *select = EOG_SELECTION(object);
    EogSelectionPrivate *priv = select->priv;

    priv->zoom = eog_scroll_view_get_zoom(EOG_SCROLL_VIEW(priv->eog_view));
//    g_message ("Init Zoom : %f", eog_scroll_view_get_zoom(EOG_SCROLL_VIEW (priv->eog_view)));

    GtkWidget *overlay = gtk_grid_get_child_at(GTK_GRID(priv->eog_view), 0, 0);
    g_assert(GTK_IS_OVERLAY(overlay));

    gtk_widget_get_allocation (GTK_WIDGET (overlay), &priv->alloc_view);

    gtk_widget_add_events(GTK_WIDGET(select), gtk_widget_get_events (GTK_WIDGET(select))
                                                    | GDK_BUTTON_PRESS_MASK
                                                    | GDK_BUTTON_RELEASE_MASK
                                                    | GDK_POINTER_MOTION_MASK);

    g_signal_connect(GTK_WIDGET(select), "button-press-event",
                     G_CALLBACK(clicked_cb), select);
    g_signal_connect(GTK_WIDGET(select), "button-release-event",
                     G_CALLBACK(clicked_cb), select);
    g_signal_connect (GTK_WIDGET(select), "motion-notify-event",
                      G_CALLBACK (motion_notify_event_cb), select);
    g_signal_connect (G_OBJECT (select), "draw",
                      G_CALLBACK (draw_cb), select);

    return;
}

static void
eog_selection_dispose (GObject *object)
{
    g_return_if_fail(EOG_IS_SELECTION(object));
/*
    EogSelection *selection = EOG_SELECTION(object);
    guint nb_signals = g_signal_handlers_disconnect_by_data(object, selection);
    g_warning("Nb signals : %d", nb_signals);

    if(selection->l_coords != NULL){
        g_slist_foreach(selection->l_coords, (GFunc)g_free, NULL);
        g_slist_free(selection->l_coords);
        selection->l_coords = NULL;
    }
*/
    G_OBJECT_CLASS (eog_selection_parent_class)->dispose (object);

    return;
}

static void
eog_selection_finalize (GObject *object)
{

    EogSelection *selection = EOG_SELECTION(object);
    guint nb_signals = g_signal_handlers_disconnect_by_data(object, selection);
    g_warning("Nb signals : %d", nb_signals);

    G_OBJECT_CLASS (eog_selection_parent_class)->finalize (object);
}

static void
eog_selection_get_property (GObject    *object,
                                   guint       prop_id,
                                   GValue     *value,
                                   GParamSpec *pspec)
{
    EogSelectionPrivate *priv = EOG_SELECTION(object)->priv;
    switch (prop_id){
        case PROP_VIEW:
            g_value_set_object(value, priv->eog_view);
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
eog_selection_set_property (GObject    *object,
                                   guint       prop_id,
                                   const GValue     *value,
                                   GParamSpec *pspec)
{
    EogSelectionPrivate *priv = EOG_SELECTION(object)->priv;

    switch (prop_id){
        case PROP_VIEW:
            priv->eog_view = GTK_WIDGET(g_value_get_object (value));
            break;
        case PROP_NAV:
            priv->eog_nav = GTK_WIDGET (g_value_get_object (value));
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
eog_selection_class_init (EogSelectionClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);

    object_class->constructed = eog_selection_constructed;
    object_class->dispose = eog_selection_dispose;
    object_class->finalize = eog_selection_finalize;
    object_class->set_property = eog_selection_set_property;
    object_class->get_property = eog_selection_get_property;

    g_object_class_install_property(object_class, PROP_VIEW, g_param_spec_object("view", "", "", GTK_TYPE_WIDGET,
                                                                                  (G_PARAM_WRITABLE |
                                                                                   G_PARAM_CONSTRUCT_ONLY |
                                                                                   G_PARAM_STATIC_STRINGS))); //G_PARAM_READWRITE));

    g_object_class_install_property(object_class, PROP_NAV, g_param_spec_object("nav", "", "", GTK_TYPE_WIDGET,
                                                                                 (G_PARAM_WRITABLE |
                                                                                  G_PARAM_CONSTRUCT_ONLY |
                                                                                  G_PARAM_STATIC_STRINGS))); //G_PARAM_READWRITE));

    return;
}

static void
eog_selection_init (EogSelection *select)
{
    select->priv = eog_selection_get_instance_private(select);
    //select->priv->ratio = RATIO_FREE;
    select->priv->func_filter = NULL;
    select->l_coords = NULL;
    select->priv->select_area = (GdkRectangle){-1, -1, -1, -1};
}

void
eog_selection_set_func_filter (GtkWidget *selection, gboolean (*func_filter) (EogSelection *sel, GtkWidget *view, GdkRectangle selection))
{
    g_return_if_fail(EOG_IS_SELECTION(selection));
    EOG_SELECTION(selection)->priv->func_filter = func_filter;

    return;
}

static gboolean
clicked_cb(GtkWidget *widget, GdkEventButton *event,
           gpointer user_data)
{
    g_return_val_if_fail(EOG_IS_SELECTION(user_data), FALSE);
    EogSelectionPrivate *priv = EOG_SELECTION(user_data)->priv;

    /*
     * When the thumb nav is hidden the size of the scrollview and the zoom change
     * TODO better fix
     */
    GtkWidget *overlay = gtk_grid_get_child_at(GTK_GRID(priv->eog_view), 0, 0);
    gtk_widget_get_allocation (GTK_WIDGET (overlay), &priv->alloc_view);
    priv->zoom = eog_scroll_view_get_zoom(EOG_SCROLL_VIEW (priv->eog_view));
    if(event->type == GDK_BUTTON_PRESS){
        eog_scroll_view_get_image_coords (EOG_SCROLL_VIEW(priv->eog_view), &priv->offset_x, &priv->offset_y, &priv->width, &priv->height);

        if(event->x < priv->offset_x)
            priv->select_area.x = priv->offset_x;
        else if(priv->offset_x > 0 && event->x > (priv->width + priv->offset_x))
            priv->select_area.x = priv->width + priv->offset_x;
        else
            priv->select_area.x = event->x;

        if(event->y < priv->offset_y)
            priv->select_area.y = priv->offset_y;
        else if(priv->offset_y > 0 && event->y > (priv->height + priv->offset_y))
            priv->select_area.y = priv->height + priv->offset_y;
        else
            priv->select_area.y = event->y;
    } else if(event->type == GDK_BUTTON_RELEASE) {
        if(priv->select_img.width < 10 && priv->select_img.height < 10) {
            priv->select_area = (GdkRectangle){-1, -1, -1, -1};
            gtk_widget_queue_draw(widget);
            return TRUE;
        }

        priv->select_img.x -= priv->offset_x;
        priv->select_img.y -= priv->offset_y;

        priv->select_img.x /= priv->zoom;
        priv->select_img.y /= priv->zoom;
        priv->select_img.width /= priv->zoom;
        priv->select_img.height /= priv->zoom;

        if(priv->func_filter != NULL){
            gboolean refresh = priv->func_filter(EOG_SELECTION(user_data), priv->eog_view, priv->select_img);
            if(refresh) {
                priv->select_area = (GdkRectangle){-1, -1, -1, -1};
                gtk_widget_queue_draw(widget);
            }
        } else{
            priv->select_area = (GdkRectangle){-1, -1, -1, -1};
            gtk_widget_queue_draw(widget);
        }

        g_warning("%s : (%d, %d), (%d, %d)", "Region selection", priv->select_img.x, priv->select_img.y,
                  priv->select_img.width, priv->select_img.height);

    }
    return TRUE;
}

static gboolean
draw_cb (GtkWidget *widget, cairo_t *cr, gpointer data)
{
    g_return_val_if_fail(EOG_IS_SELECTION(data), FALSE);
    EogSelectionPrivate *priv = EOG_SELECTION(data)->priv;

    if(priv->select_area.x == -1)
        return FALSE;

    gint pw, ph, offset_x, offset_y, tmp;
    priv->select_img.x = priv->select_area.x;
    priv->select_img.y = priv->select_area.y;
    pw = priv->alloc_view.width < priv->width ? priv->alloc_view.width : priv->width;
    ph = priv->alloc_view.height < priv->height ? priv->alloc_view.height : priv->height;
    offset_x = priv->offset_x < 0 ? 0 : priv->offset_x;
    offset_y = priv->offset_y < 0 ? 0 : priv->offset_y;

    if(priv->select_area.width < offset_x){
        priv->select_img.width = offset_x;
    }else if(priv->select_area.width > (pw + offset_x)){
        priv->select_img.width = pw + offset_x ;
    } else
        priv->select_img.width = priv->select_area.width;

    if(priv->select_area.height < offset_y){
        priv->select_img.height = offset_y;
    }else if(priv->select_area.height > (ph + offset_y)){
        priv->select_img.height = ph + offset_y;
    } else
        priv->select_img.height = priv->select_area.height;

    if(priv->select_img.x > priv->select_img.width){
        tmp = priv->select_img.x;
        priv->select_img.x = priv->select_img.width;
        priv->select_img.width = tmp;
    }

    if(priv->select_img.y > priv->select_img.height){
        tmp = priv->select_img.y;
        priv->select_img.y = priv->select_img.height;
        priv->select_img.height = tmp;
    }
    priv->select_img.width -= priv->select_img.x;
    priv->select_img.height -= priv->select_img.y;

    cairo_new_sub_path(cr);
    cairo_set_line_width(cr, 4.5);

    cairo_set_source_rgba(cr, 0, 0, 0, 0.8);
    cairo_rectangle(cr, priv->select_img.x, priv->select_img.y, priv->select_img.width, priv->select_img.height);

    cairo_fill_preserve(cr);
    cairo_set_source_rgb(cr, 1, 1, 1);

    cairo_stroke(cr);

    static double pattern[] = {10.0, 5.0};
    cairo_set_dash (cr, pattern, 2, 0);//    cairo_set_dash (cr, pattern, 0, 0);
    cairo_set_source_rgb(cr, 0, 0, 0);
    cairo_rectangle(cr, priv->select_img.x, priv->select_img.y, priv->select_img.width, priv->select_img.height);

    cairo_stroke(cr);
    return TRUE;
}

static gboolean
motion_notify_event_cb (GtkWidget      *widget,
                        GdkEventMotion *event,
                        gpointer        data)
{
    g_return_val_if_fail(EOG_IS_SELECTION(data), FALSE);
    EogSelectionPrivate *priv = EOG_SELECTION(data)->priv;

    GdkModifierType state;
    gdk_window_get_device_position(event->window, event->device, NULL, NULL, &state);

    if (state & GDK_BUTTON1_MASK) {
        priv->select_area.width = event->x;
        priv->select_area.height = event->y;
        gtk_widget_queue_draw(widget);
    }
    /* We've handled it, stop processing */
    return TRUE;
}

GtkWidget *
eog_selection_new (GtkWidget *view)
{
    return g_object_new (EOG_TYPE_SELECTION, "view", view, NULL);
}