/* EogToolbar - Plugin.

   Copyright (C) 2019 Yahiaoui Fakhri

   This file is part of the plugin for Eog.

   EogToolbar is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License as published by the Free
   Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   EogToolbar is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
   for more details.

   You should have received a copy of the GNU General Public License
   along with EogImagesManager; see the file COPYING.  If not, write to the Free
   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   This file implements a toolbar for eog.  */

#ifndef EOG_TOOLBAR_PLUGIN_H
#define EOG_TOOLBAR_PLUGIN_H

#include <eog-3.0/eog/eog-window.h>
#include <libpeas/peas.h>
#include <gtk/gtk.h>

G_BEGIN_DECLS

/*
 * Type checking and casting macros
 */
#define EOG_TYPE_TOOLBAR_PLUGIN		(eog_toolbar_plugin_get_type ())
#define EOG_TOOLBAR_PLUGIN(o)		(G_TYPE_CHECK_INSTANCE_CAST ((o), EOG_TYPE_TOOLBAR_PLUGIN, EogToolbarPlugin))
#define EOG_TOOLBAR_PLUGIN_CLASS(k)		G_TYPE_CHECK_CLASS_CAST((k),      EOG_TYPE_TOOLBAR_PLUGIN, EogToolbarPluginClass))
#define EOG_IS_TOOLBAR_PLUGIN(o)	        (G_TYPE_CHECK_INSTANCE_TYPE ((o), EOG_TYPE_TOOLBAR_PLUGIN))
#define EOG_IS_TOOLBAR_PLUGIN_CLASS(k)	(G_TYPE_CHECK_CLASS_TYPE ((k),    EOG_TYPE_TOOLBAR_PLUGIN))
#define EOG_TOOLBAR_PLUGIN_GET_CLASS(o)	(G_TYPE_INSTANCE_GET_CLASS ((o),  EOG_TYPE_TOOLBAR_PLUGIN, EogToolbarPluginClass))

typedef struct _EogToolbarPluginPrivate		EogToolbarPluginPrivate;

/*
 * Main object structure
 */
typedef struct _EogToolbarPlugin		EogToolbarPlugin;

struct _EogToolbarPlugin
{
    PeasExtensionBase parent_instance;

    EogWindow *window;

    EogToolbarPluginPrivate *priv;
};

/*
 * Class definition
 */
typedef struct _EogToolbarPluginClass	EogToolbarPluginClass;

struct _EogToolbarPluginClass
{
    PeasExtensionBaseClass parent_class;
};

/*
 * Public methods
 */
GType	eog_toolbar_plugin_get_type		(void) G_GNUC_CONST;

/* All the plugins must implement this function */
G_MODULE_EXPORT void peas_register_types (PeasObjectModule *module);

G_END_DECLS

#endif //EOG_TOOLBAR_PLUGIN_H
