/* EogToolbar - Plugin.

   Copyright (C) 2019 Yahiaoui Fakhri

   This file is part of the plugin for Eog.

   EogToolbar is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License as published by the Free
   Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   EogToolbar is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
   for more details.

   You should have received a copy of the GNU General Public License
   along with EogImagesManager; see the file COPYING.  If not, write to the Free
   Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

   This file implements a toolbar for eog.  */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <eog-3.0/eog/eog-thumb-view.h>
#include <eog-3.0/eog/eog-thumb-nav.h>
#include <eog-3.0/eog/eog-scroll-view.h>
#include "eog-main-toolbar.h"
#include "eog-utils.h"
#include "eog-crop-toolbar.h"
#include "eog-comment-toolbar.h"
#include "eog-reduce-toolbar.h"
#include "eog-brightness-toolbar.h"
#include "utils/exif-jpg.h"
#include "eog-red-eyes-toolbar.h"
#include <glib/gi18n.h>
#include <eog/eog-job-scheduler.h>
#include "eog-jobs-filter.h"

#define EOG_CONF_UI		"org.gnome.eog.ui"
#define GALLERY_SIZE    "image-gallery-thumb-size"

enum {
    THUMBNAILS_SIZE_NORMAL = 90,
    THUMBNAILS_SIZE_MIDDLE = 173,
    THUMBNAILS_SIZE_LARGE = 256,
};

struct _EogMainToolbarPrivate {
    EogWindow *window;
    GtkStack *stack;
    GtkWidget *t_view;
    GtkWidget *eog_nav;
    GtkWidget *manager;
    GtkWidget *area_progress;

    gboolean slideshow, gallery;

    GtkToolItem *item_gallery, *item_rename, *item_crop, *item_rotate, *item_comment;
    guint pos_start_filters, pos_end_filters;

    gboolean filters_is_visible;

    GtkWidget *box_info;
    GtkWidget *label_info;
    gchar *format;

    GSettings *gallery_settings;
    gboolean oinfo_exist;
};

enum {
    PROP_0,
    PROP_WINDOW,
    PROP_STACK
};

G_DEFINE_TYPE_WITH_PRIVATE (EogMainToolbar, eog_main_toolbar, GTK_TYPE_TOOLBAR)

static void
eog_main_toolbar_constructed (GObject *object)
{
    if (G_OBJECT_CLASS (eog_main_toolbar_parent_class)->constructed)
        G_OBJECT_CLASS (eog_main_toolbar_parent_class)->constructed (object);

    EogMainToolbar *toolbar = EOG_MAIN_TOOLBAR(object);
    EogMainToolbarPrivate *priv = toolbar->priv;

    /*
     * We cannot know if the plugin manager is loaded first or not;
     */
    priv->manager = NULL;//get_widget_by_name(GTK_WIDGET (priv->window), "Manager");
    priv->t_view = eog_window_get_thumb_view(priv->window);
    priv->eog_nav = eog_window_get_thumb_nav(priv->window);

    GtkWidget *eog_scrollview = eog_window_get_view (priv->window);
    g_assert (GTK_IS_GRID (eog_scrollview));
    GtkWidget *overlay_sv = gtk_widget_get_parent (eog_scrollview);
    g_assert (GTK_IS_OVERLAY (overlay_sv));
    GList *children = gtk_container_get_children (GTK_CONTAINER (overlay_sv));
    priv->area_progress = g_list_nth_data(children, 1);
    g_assert (GTK_IS_DRAWING_AREA(priv->area_progress));
    g_list_free (children);

    priv->label_info = gtk_label_new(NULL);
    gtk_label_set_line_wrap (GTK_LABEL (priv->label_info), TRUE);
    gtk_label_set_max_width_chars (GTK_LABEL (priv->label_info), 100);
    gtk_label_set_justify(GTK_LABEL (priv->label_info), GTK_JUSTIFY_CENTER);
    priv->format = "<span foreground='white' size='large'><b>%s</b></span>";

    priv->box_info = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 0);
    gtk_widget_set_opacity(priv->box_info, 0.6);
    gtk_widget_set_name(priv->box_info, "box-info");
    gtk_box_pack_start(GTK_BOX (priv->box_info), priv->label_info, FALSE, FALSE, 0);

    gtk_widget_set_valign (priv->box_info, GTK_ALIGN_CENTER);
    gtk_widget_set_halign (priv->box_info, GTK_ALIGN_CENTER);

    gtk_grid_attach_next_to (GTK_GRID (eog_scrollview), priv->box_info, NULL, GTK_POS_BOTTOM, 1, 1);
    gtk_widget_set_visible(priv->box_info, FALSE);
//    priv->box_info = gtk_grid_get_child_at(GTK_GRID (eog_scrollview), 0, 2);


    return;
}

static void
eog_main_toolbar_dispose (GObject *object)
{

    G_OBJECT_CLASS (eog_main_toolbar_parent_class)->dispose (object);
    return;
}

static void
eog_main_toolbar_get_property (GObject    *object,
                               guint       prop_id,
                               GValue     *value,
                               GParamSpec *pspec)
{
    EogMainToolbarPrivate *priv = EOG_MAIN_TOOLBAR(object)->priv;

    switch (prop_id){
        case PROP_WINDOW:
            g_value_set_object(value, priv->window);
            break;
        case PROP_STACK:
            g_value_set_object(value, priv->stack);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
eog_main_toolbar_set_property (GObject    *object,
                               guint       prop_id,
                               const GValue     *value,
                               GParamSpec *pspec)
{
    EogMainToolbarPrivate *priv = EOG_MAIN_TOOLBAR(object)->priv;

    switch (prop_id){
        case PROP_WINDOW:
            priv->window = EOG_WINDOW(g_value_dup_object (value));
            break;
        case PROP_STACK:
            priv->stack = GTK_STACK(g_value_dup_object (value));
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
eog_main_toolbar_class_init (EogMainToolbarClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);

    object_class->constructed = eog_main_toolbar_constructed;
    object_class->dispose = eog_main_toolbar_dispose;
    object_class->set_property = eog_main_toolbar_set_property;
    object_class->get_property = eog_main_toolbar_get_property;

    g_object_class_install_property(object_class, PROP_WINDOW, g_param_spec_object("window", "", "", EOG_TYPE_WINDOW, (G_PARAM_WRITABLE |
                                                                                                                       G_PARAM_CONSTRUCT_ONLY |
                                                                                                                       G_PARAM_STATIC_STRINGS))); //G_PARAM_READWRITE));
    g_object_class_install_property(object_class, PROP_STACK, g_param_spec_object("stack", "", "", GTK_TYPE_STACK, (G_PARAM_WRITABLE |
                                                                                                                    G_PARAM_CONSTRUCT_ONLY |
                                                                                                                    G_PARAM_STATIC_STRINGS))); //G_PARAM_READWRITE));
    bind_textdomain_codeset (ET_PACKAGE, "UTF-8");
    textdomain (ET_PACKAGE);
    return;
}

GtkWidget *
eog_main_toolbar_new (EogWindow *window, GtkStack *stack)
{
    return g_object_new (EOG_TYPE_MAIN_TOOLBAR, "window", window, "stack", stack, NULL);
}

/* * * * * *  * * * * * * * * * * *
 *
 * BUTTON GALLERY
 *
 * * * * * *  * * * * * * * *  * * */

static void
resize_cells (gpointer data, gpointer user_data)
{
    if (GTK_IS_CELL_RENDERER_PIXBUF(data)) {
        guint size = GPOINTER_TO_INT(user_data);

        g_object_set (data, "width", size, "height", size, NULL);
//        gtk_cell_renderer_set_fixed_size(data, size, size);
    }
    return;
}

static void
_resize_thumbnails (EogThumbNav *nav, EogThumbView *thumbview, guint new_size)
{
    EogListStore     *store = EOG_LIST_STORE(gtk_icon_view_get_model (GTK_ICON_VIEW (thumbview)));

    gint size_thumb;
    if (eog_thumb_nav_get_mode (nav) == EOG_THUMB_NAV_MODE_MULTIPLE_ROWS) {
        GtkAllocation alloc_nav;
        gtk_widget_get_allocation(GTK_WIDGET(nav), &alloc_nav);

        alloc_nav.width -= 15;
        gint val_thumbnails = new_size;
        gint val_col = alloc_nav.width / val_thumbnails;
        gtk_icon_view_set_columns(GTK_ICON_VIEW(thumbview), val_col);

        size_thumb = alloc_nav.width / val_col;
        size_thumb -= 2;
    } else {
        gint len = eog_list_store_length (store);
        gtk_icon_view_set_columns(GTK_ICON_VIEW(thumbview), len);
        size_thumb = new_size;
    }

    GList *renderer = gtk_cell_layout_get_cells(GTK_CELL_LAYOUT (thumbview));
    g_list_foreach(renderer, resize_cells, GINT_TO_POINTER(size_thumb));

    GtkTreePath *path1 = gtk_tree_path_new_first ();
    gint n_items = gtk_tree_model_iter_n_children(GTK_TREE_MODEL (store), NULL);
    GtkTreePath *path2 = gtk_tree_path_new_from_indices(n_items - 1, -1);

    GtkTreeIter iter;
    gint thumb = 0;
    gint end_thumb = gtk_tree_path_get_indices(path2)[0];
    gboolean result;
    gboolean thumb_set = FALSE;
    for (result = gtk_tree_model_get_iter(GTK_TREE_MODEL (store), &iter, path1);
         result && thumb <= end_thumb + 1;
         result = gtk_tree_model_iter_next(GTK_TREE_MODEL (store), &iter), thumb++) {
        gtk_tree_model_get (GTK_TREE_MODEL (store), &iter, EOG_LIST_STORE_THUMB_SET, &thumb_set, -1);
        if (thumb_set)
            eog_list_store_thumbnail_refresh(store, &iter);
    }

    gtk_tree_path_free(path1);
    gtk_tree_path_free(path2);

    g_object_set(thumbview,
                 "activate-on-single-click", TRUE, "margin", 10, "item-padding", 1, NULL);
}

static gboolean
key_press_event_cb (GtkWidget *widget,
                    GdkEvent  *event,
                    gpointer   user_data)
{
    g_return_val_if_fail(EOG_IS_MAIN_TOOLBAR(user_data), FALSE);
    EogMainToolbarPrivate *priv = EOG_MAIN_TOOLBAR(user_data)->priv;

    guint new_size;

    switch(event->key.keyval){
        case GDK_KEY_Escape:
            if (priv->slideshow){
                eog_window_set_mode(priv->window, EOG_WINDOW_MODE_NORMAL);
                GtkWidget *boxtool = eog_window_get_boxtool(priv->window);
                if (!gtk_widget_get_visible(priv->manager))
                    gtk_widget_set_visible(priv->manager, TRUE);
                if (!gtk_widget_get_visible(boxtool))
                    gtk_widget_set_visible(boxtool, TRUE);

                gtk_widget_show_all(boxtool);

                GAction *action = g_action_map_lookup_action (G_ACTION_MAP (priv->window), "view-gallery");
                g_action_change_state (action, g_variant_new_boolean (TRUE));

                // Bad practice ?
                g_signal_handlers_disconnect_by_func (priv->window, key_press_event_cb, user_data);
                priv->slideshow = FALSE;
            }
            break;
        case GDK_KEY_KP_Add:
        case GDK_KEY_plus:
            if ( (event->key.state & GDK_CONTROL_MASK) && priv->gallery) {
                new_size = g_settings_get_enum(priv->gallery_settings, GALLERY_SIZE);
                if (new_size < THUMBNAILS_SIZE_LARGE) {
                    new_size = (new_size == THUMBNAILS_SIZE_NORMAL) ? THUMBNAILS_SIZE_MIDDLE : THUMBNAILS_SIZE_LARGE;
                    g_settings_set_enum(priv->gallery_settings, GALLERY_SIZE, new_size);
                    EogListStore *store = eog_window_get_store (priv->window);
                    g_object_set (store, "size-thumbnail", new_size, NULL);
                    _resize_thumbnails(EOG_THUMB_NAV (eog_window_get_thumb_nav (priv->window)), EOG_THUMB_VIEW(priv->t_view), new_size);
                }
            }
            break;
        case GDK_KEY_KP_Subtract:
        case GDK_KEY_minus:
            if ( (event->key.state & GDK_CONTROL_MASK) && priv->gallery) {
                new_size = g_settings_get_enum(priv->gallery_settings, GALLERY_SIZE);
                if (new_size > THUMBNAILS_SIZE_NORMAL) {
                    new_size = (new_size == THUMBNAILS_SIZE_LARGE) ? THUMBNAILS_SIZE_MIDDLE : THUMBNAILS_SIZE_NORMAL;
                    g_settings_set_enum(priv->gallery_settings, GALLERY_SIZE, new_size);
                    EogListStore *store = eog_window_get_store (priv->window);
                    g_object_set (store, "size-thumbnail", new_size, NULL);
                    _resize_thumbnails(EOG_THUMB_NAV (eog_window_get_thumb_nav (priv->window)), EOG_THUMB_VIEW(priv->t_view), new_size);
                }
            }
            break;
        default:
            break;
    }
    return TRUE;
}

static void
tb_on_selection_changed (GtkIconView *thumb_view,
                         gpointer data)
{
    g_return_if_fail(EOG_IS_MAIN_TOOLBAR(data));

    EogMainToolbarPrivate *priv = EOG_MAIN_TOOLBAR(data)->priv;
    EogWindow *window = EOG_MAIN_TOOLBAR(data)->priv->window;
    g_return_if_fail(EOG_IS_WINDOW(window));

    GtkWidget *nav = eog_window_get_thumb_nav(window);


    eog_thumb_nav_set_mode (EOG_THUMB_NAV(nav), EOG_THUMB_NAV_MODE_ONE_ROW);
    eog_utils_set_mode_single_row(window);

    g_object_set (eog_window_get_store (priv->window), "size-thumbnail", 90, NULL);
    _resize_thumbnails(EOG_THUMB_NAV (nav), EOG_THUMB_VIEW(priv->t_view), 90);

    eog_thumb_view_select_single(EOG_THUMB_VIEW(priv->t_view), EOG_THUMB_VIEW_SELECT_CURRENT);

    gtk_stack_set_visible_child_name(GTK_STACK(priv->stack), "main-toolbar");

    priv->gallery = FALSE;

    gtk_widget_hide (priv->box_info);
    // Bad practice ?
    g_signal_handlers_disconnect_by_func (priv->t_view, key_press_event_cb, data);
    g_signal_handlers_disconnect_by_func (priv->t_view, tb_on_selection_changed, data);

    return;
}

static void
button_gallery_cb (GtkToolButton *toolbutton,
                   gpointer       data)
{
    g_return_if_fail(EOG_IS_MAIN_TOOLBAR(data));

    EogMainToolbarPrivate *priv = EOG_MAIN_TOOLBAR(data)->priv;
    EogWindow *window = EOG_MAIN_TOOLBAR(data)->priv->window;
    g_return_if_fail(EOG_IS_WINDOW(window));

    GtkWidget *eog_nav = eog_window_get_thumb_nav(priv->window);

    eog_thumb_nav_set_mode(EOG_THUMB_NAV(eog_nav), EOG_THUMB_NAV_MODE_MULTIPLE_ROWS);

    gint val_thumbnails = g_settings_get_enum(priv->gallery_settings, GALLERY_SIZE);
    g_object_set (eog_window_get_store (priv->window), "size-thumbnail", val_thumbnails, NULL);
    _resize_thumbnails(EOG_THUMB_NAV (eog_nav), EOG_THUMB_VIEW(priv->t_view), val_thumbnails);

    eog_utils_set_mode_multiple_rows(window);

    g_signal_connect(GTK_WIDGET(priv->t_view),
                     "selection_changed",
                     G_CALLBACK(tb_on_selection_changed), data);
    g_signal_connect(GTK_WIDGET(priv->t_view),
                     "key-press-event",
                     G_CALLBACK(key_press_event_cb), data);



    priv->gallery = TRUE;
    return;
}

static GtkToolItem *
item_button_gallery (EogMainToolbar *toolbar)
{
    g_return_val_if_fail(EOG_IS_MAIN_TOOLBAR(toolbar), NULL);

    GtkWidget *img_gallery = gtk_image_new_from_icon_name("input-dialpad", GTK_ICON_SIZE_BUTTON);
    GtkToolItem *ss = gtk_tool_button_new(img_gallery, _("Gallery"));

    g_signal_connect(GTK_TOOL_BUTTON(ss), "clicked", G_CALLBACK(button_gallery_cb), toolbar);

    return ss;
}

/* * * * * *  * * * * * * * * * * *
 *
 * BUTTON SLIDESHOW
 *
 * * * * * *  * * * * * * * *  * * */
static void
default_slideshow (EogMainToolbar *toolbar)
{
    EogMainToolbarPrivate *priv = toolbar->priv;
    if (!priv->manager)
        priv->manager = eog_utils_get_widget_by_name(GTK_WIDGET (priv->window), "Manager");
    gtk_widget_set_visible(priv->manager, FALSE);

    eog_thumb_view_select_single(EOG_THUMB_VIEW(priv->t_view), EOG_THUMB_VIEW_SELECT_FIRST);
    eog_window_set_mode(priv->window, EOG_WINDOW_MODE_SLIDESHOW);

    priv->slideshow = TRUE;

    g_signal_connect(GTK_WIDGET(priv->window),
                     "key-press-event",
                     G_CALLBACK(key_press_event_cb), toolbar);
    return;
}

static void
button_slideshow_cb (GtkToolButton *toolbutton,
                     gpointer       data)
{
    g_return_if_fail(EOG_IS_MAIN_TOOLBAR(data));

    EogMainToolbarPrivate *priv = EOG_MAIN_TOOLBAR(data)->priv;

    if (priv->oinfo_exist) {
        GtkTreeIter iter;
        GError *error = NULL;
        EogImage *image;
        GFile *file;
        gchar *path;
        GString *args = g_string_new ("photossimo ");
        GtkTreeModel *model = GTK_TREE_MODEL (eog_window_get_store (priv->window));
        EogThumbNavMode mode = eog_thumb_nav_get_mode(EOG_THUMB_NAV(priv->eog_nav));
        gboolean valid = gtk_tree_model_get_iter_first(model, &iter);
        if (!valid)
            return;
        do {
            gtk_tree_model_get (model, &iter, EOG_LIST_STORE_EOG_IMAGE, &image, -1);
            file = eog_image_get_file(image);
            path = g_file_get_path(file);
            g_string_append_printf(args, "'%s' ", path);
        } while (gtk_tree_model_iter_next(model, &iter));
        if (path)
            g_free (path);
        if(file)
            g_object_unref (file);
        if (image)
            g_object_unref (G_OBJECT (image));

        gboolean spawned = g_spawn_command_line_async(args->str, &error);
        if (!spawned || error) {
            g_critical("Could not spawn photossimo : %s", error ? error->message : "Unknown error");
            default_slideshow(EOG_MAIN_TOOLBAR(data));
        }
        g_string_free (args, TRUE);
    } else
        default_slideshow(EOG_MAIN_TOOLBAR(data));
    return;
}

static GtkToolItem *
item_button_slideshow (EogMainToolbar *toolbar)
{
    g_return_val_if_fail(EOG_IS_MAIN_TOOLBAR(toolbar), NULL);

    GtkWidget *img_slideshow = gtk_image_new_from_icon_name("view-fullscreen", GTK_ICON_SIZE_BUTTON);
    GtkToolItem *ss = gtk_tool_button_new(img_slideshow, _("Slideshow"));

    g_signal_connect(GTK_TOOL_BUTTON(ss), "clicked", G_CALLBACK(button_slideshow_cb), toolbar);

    return ss;

}

/* * * * * *  * * * * * * * * * * *
 *
 * BUTTON RENAME
 *
 * * * * * *  * * * * * * * *  * * */
static void
rename_image (EogWindow *window, EogImage *image, const gchar *new_name, const gchar *ext)
{
    g_return_if_fail (EOG_IS_WINDOW (window));

    GFile *image_file = eog_image_get_file(image);
    const gchar *path_image = g_strconcat(g_file_get_parse_name(g_file_get_parent(image_file)), "/", new_name, NULL);

    const gchar *renamed_file = eog_utils_get_new_filename(path_image, "");//check_renamed_file(path_image, ext);//
    const gchar *new_caption = g_path_get_basename(renamed_file);

    // Break the jpeg
    //    image_file = g_file_set_display_name (image_file, new_caption, NULL, NULL);

    gchar *orig_path = g_file_get_parse_name(image_file);
    rename(orig_path, renamed_file);

    EogListStore *gallery = eog_window_get_store(window);

    /* We remove the image because after the image renamed we cannot select it */
    eog_list_store_remove_image(gallery, image);

    GFile *new_file = g_file_new_for_path(renamed_file);
    EogImage *new_image = eog_image_new_file(new_file, new_caption);
    eog_list_store_append_image(gallery, new_image);
    eog_image_autorotate(new_image);
    eog_image_modified(new_image);
    eog_thumb_view_set_current_image(EOG_THUMB_VIEW (eog_window_get_thumb_view(window)), new_image, TRUE);

    eog_window_reload_image(window);
    g_object_unref (image_file);
    g_object_unref (new_file);
    return;
}

static void
set_valign_center_label (gpointer widget,
                         gpointer data)
{
    if (GTK_IS_LABEL (widget))
        gtk_widget_set_valign(widget, GTK_ALIGN_CENTER);
    return;
}

static gboolean
rename_on_key_pressed (GtkWidget *widget,
                       GdkEvent  *event,
                       gpointer   user_data)
{
    switch(event->key.keyval) {
        case GDK_KEY_KP_Enter:
        case GDK_KEY_Return:
            gtk_dialog_response(GTK_DIALOG(widget), GTK_RESPONSE_OK);
            break;
        case GDK_KEY_Escape:
            gtk_dialog_response(GTK_DIALOG(widget), GTK_RESPONSE_CANCEL);
            break;
        default:
            break;
    }
    return FALSE;
}

static void
button_rename_cb (GtkToolButton *toolbutton,
                  gpointer       data)
{
    g_return_if_fail(EOG_IS_MAIN_TOOLBAR(data));

    EogMainToolbarPrivate *priv = EOG_MAIN_TOOLBAR(data)->priv;
    EogWindow *window = priv->window;
    g_return_if_fail(EOG_IS_WINDOW(window));

    EogImage *curr_image = eog_window_get_image(window);
    if(curr_image == NULL)
        return;

    GtkWidget *align = NULL;
    GtkWidget *box = NULL;
    GList *box_children;

    GtkWidget *dialog = gtk_message_dialog_new(GTK_WINDOW(window),GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
                                               GTK_MESSAGE_QUESTION, GTK_BUTTONS_NONE,
                                               _("Enter the new image name :"));
    gtk_widget_set_name (dialog, "dialog-rename");
    gtk_window_set_decorated(GTK_WINDOW(dialog), FALSE);

    /* default response doesn't seem to work with add_action_widget */

    GtkWidget *img_close = gtk_image_new_from_icon_name("window-close", priv->oinfo_exist? GTK_ICON_SIZE_LARGE_TOOLBAR : GTK_ICON_SIZE_BUTTON);
    GtkWidget *button_close = gtk_button_new_with_label(_("Cancel"));

    gtk_button_set_image(GTK_BUTTON(button_close), img_close);
    gtk_button_set_always_show_image(GTK_BUTTON(button_close), TRUE);
    gtk_button_set_image_position(GTK_BUTTON(button_close), GTK_POS_LEFT);

    /* Getting label and setting valign before adding the image doesn't apply */
    align = gtk_bin_get_child (GTK_BIN(button_close));
    box = gtk_bin_get_child (GTK_BIN(align));
    box_children = gtk_container_get_children (GTK_CONTAINER (box));
    g_list_foreach(box_children, set_valign_center_label, NULL);
    g_list_free (box_children);

    gtk_dialog_add_action_widget (GTK_DIALOG(dialog), button_close, GTK_RESPONSE_CANCEL);

    GtkWidget *img_ok = gtk_image_new_from_icon_name("gtk-ok", priv->oinfo_exist? GTK_ICON_SIZE_LARGE_TOOLBAR : GTK_ICON_SIZE_BUTTON);
    GtkWidget *button_ok = gtk_button_new_with_label(_("Validate"));

    gtk_button_set_image(GTK_BUTTON(button_ok), img_ok);
    gtk_button_set_always_show_image(GTK_BUTTON(button_ok), TRUE);
    gtk_button_set_image_position(GTK_BUTTON(button_ok), GTK_POS_LEFT);

    /* Getting label and setting valign before adding the image doesn't apply */
    align = gtk_bin_get_child (GTK_BIN(button_ok));
    box = gtk_bin_get_child (GTK_BIN(align));
    box_children = gtk_container_get_children (GTK_CONTAINER (box));
    g_list_foreach(box_children, set_valign_center_label, NULL);
    g_list_free (box_children);

    gtk_dialog_add_action_widget (GTK_DIALOG(dialog), button_ok, GTK_RESPONSE_OK);

    GtkWidget *bbox = gtk_widget_get_parent(button_ok);
    g_assert (GTK_IS_BUTTON_BOX(bbox));
    gtk_widget_set_halign(bbox, GTK_ALIGN_CENTER);

    /* if the buttons label are not of the same size the icons is not at the same position */
    gtk_button_box_set_layout (GTK_BUTTON_BOX (bbox), GTK_BUTTONBOX_CENTER);
    gtk_button_box_set_child_non_homogeneous (GTK_BUTTON_BOX (bbox), button_ok, TRUE);
    gtk_button_box_set_child_non_homogeneous (GTK_BUTTON_BOX (bbox), button_close, TRUE);

    GtkWidget *content_area = gtk_dialog_get_content_area(GTK_DIALOG (dialog));
    GtkWidget *entry = gtk_entry_new();
    gtk_entry_set_activates_default(GTK_ENTRY(entry), TRUE);

//    gtk_dialog_set_default_response(GTK_DIALOG(dialog), GTK_RESPONSE_OK);
    gtk_widget_grab_focus(entry);

    gchar *caption = g_strdup(eog_image_get_caption(curr_image));
    // we hide the extension name
    gchar* ext = g_utf8_strrchr(caption, -1, '.');
    gchar *ext_dup =  NULL;
    if(ext){
        ext_dup = g_strdup(ext);
        *ext = 0;
    }
    gtk_entry_set_text(GTK_ENTRY(entry), caption);
    gtk_box_pack_start(GTK_BOX (content_area), entry, FALSE, FALSE, 0);

    gtk_widget_show_all(dialog);

    g_signal_connect(dialog, "key-press-event", G_CALLBACK(rename_on_key_pressed), NULL);

    int result = gtk_dialog_run(GTK_DIALOG(dialog));
    const gchar *entry_text = NULL;
    switch (result){
        case GTK_RESPONSE_OK:
            entry_text = gtk_entry_get_text(GTK_ENTRY (entry));
            if (strcmp(entry_text, caption) != 0)
                rename_image (EOG_MAIN_TOOLBAR(data)->priv->window, curr_image, entry_text, ext_dup);
            g_signal_handlers_disconnect_by_func(dialog, rename_on_key_pressed, NULL);
            break;
        default:
            break;
    }

    if (caption)
        g_free(caption);
    if (ext)
        g_free (ext_dup);
    gtk_widget_destroy(dialog);
    return;
}

static GtkToolItem *
item_button_rename (EogMainToolbar *toolbar)
{
    g_return_val_if_fail(EOG_IS_MAIN_TOOLBAR(toolbar), NULL);

    GtkWidget *img_rename = gtk_image_new_from_icon_name("format-text-bold", GTK_ICON_SIZE_BUTTON);
    GtkToolItem *rename = gtk_tool_button_new(img_rename, _("Rename"));// ceci est un test pour voir l'ellipsize"));

    g_signal_connect(GTK_TOOL_BUTTON(rename), "clicked", G_CALLBACK(button_rename_cb), toolbar);

    return rename;
}

/* * * * * *  * * * * * * * * * * *
 *
 * BUTTON CROP
 *
 * * * * * *  * * * * * * * *  * * */
static void
button_crop_cb (GtkToolButton *toolbutton,
                gpointer       data)
{
    g_return_if_fail(EOG_IS_MAIN_TOOLBAR(data));

    EogMainToolbarPrivate *priv = EOG_MAIN_TOOLBAR(data)->priv;

    const gchar *info = _("To crop, please select a part of the image.");
    gchar *markup = g_markup_printf_escaped(priv->format, info);
    gtk_label_set_markup(GTK_LABEL (priv->label_info), markup);
    g_free (markup);

    gtk_widget_show_all(priv->box_info);
    gtk_widget_hide (priv->eog_nav);

    if(!gtk_stack_get_child_by_name(GTK_STACK(priv->stack), "crop-toolbar")) {
        GtkWidget *crop_toolbar = eog_crop_toolbar_new(priv->window, priv->stack, priv->area_progress);
        if (priv->oinfo_exist)
            gtk_toolbar_set_icon_size(GTK_TOOLBAR(crop_toolbar), GTK_ICON_SIZE_DIALOG);
        gtk_stack_add_named(GTK_STACK(priv->stack), crop_toolbar, "crop-toolbar");
        gtk_widget_show_all(crop_toolbar);
    } else{
        GtkWidget *crop_toolbar = gtk_stack_get_child_by_name(GTK_STACK(priv->stack), "crop-toolbar");
        eog_crop_toolbar_init_area(crop_toolbar);
    }

    eog_utils_set_sensitive_widgets(priv->window, FALSE);
    gtk_stack_set_visible_child_full(GTK_STACK(priv->stack), "crop-toolbar", GTK_STACK_TRANSITION_TYPE_OVER_LEFT);
    return;
}

static GtkToolItem *
item_button_crop (EogMainToolbar *toolbar)
{
    g_return_val_if_fail(EOG_IS_MAIN_TOOLBAR(toolbar), NULL);

    GtkWidget *img_crop = gtk_image_new_from_icon_name("view-restore", GTK_ICON_SIZE_BUTTON);
    GtkToolItem *crop = gtk_tool_button_new(img_crop, _("Crop"));

    g_signal_connect(GTK_TOOL_BUTTON(crop), "clicked", G_CALLBACK(button_crop_cb), toolbar);

    return crop;
}

/* * * * * *  * * * * * * * * * * *
 *
 * BUTTON ROTATE
 *
 * * * * * *  * * * * * * * *  * * */
static gboolean
timeout_rotate (gpointer data)
{
    g_return_val_if_fail(EOG_IS_MAIN_TOOLBAR(data), G_SOURCE_REMOVE);

    EogMainToolbarPrivate *priv = EOG_MAIN_TOOLBAR(data)->priv;
    GAction *rotate_left = g_action_map_lookup_action (G_ACTION_MAP(priv->window), "rotate-90");
    g_action_activate (rotate_left, NULL);

    gtk_widget_hide (EOG_MAIN_TOOLBAR(data)->priv->area_progress);
    eog_utils_set_sensitive_widgets(priv->window, TRUE);
    return G_SOURCE_REMOVE;
}

static void
button_rotate_cb (GtkToolButton *toolbutton,
                  gpointer       data)
{
    g_return_if_fail(EOG_IS_MAIN_TOOLBAR(data));
    EogMainToolbarPrivate *priv = EOG_MAIN_TOOLBAR(data)->priv;

    gtk_widget_show_all (priv->area_progress);
    if (gtk_widget_get_visible (priv->area_progress)) {
        eog_utils_set_sensitive_widgets(priv->window, FALSE);
        g_timeout_add(500, timeout_rotate, data);
    }
    return;
}

static GtkToolItem *
item_button_rotate (EogMainToolbar *toolbar)
{
    g_return_val_if_fail(EOG_IS_MAIN_TOOLBAR(toolbar), NULL);

    GtkWidget *img_rotate = gtk_image_new_from_icon_name("object-rotate-right", GTK_ICON_SIZE_BUTTON);
    GtkToolItem *rotate = gtk_tool_button_new(img_rotate, _("Rotate"));

    g_signal_connect(GTK_TOOL_BUTTON(rotate), "clicked", G_CALLBACK(button_rotate_cb), toolbar);

    return rotate;
}

/* * * * * *  * * * * * * * * * * *
 *
 * BUTTON COMMENT
 *
 * * * * * *  * * * * * * * *  * * */
static void
button_comment_cb (GtkToolButton *toolbutton,
                   gpointer       data)
{
    g_return_if_fail(EOG_IS_MAIN_TOOLBAR(data));

    EogMainToolbarPrivate *priv = EOG_MAIN_TOOLBAR(data)->priv;

    if(!gtk_stack_get_child_by_name(GTK_STACK(priv->stack), "comment-toolbar")) {
        GtkWidget *comment_toolbar = eog_comment_toolbar_new(priv->window, priv->stack, priv->area_progress);
        if (priv->oinfo_exist)
            gtk_toolbar_set_icon_size(GTK_TOOLBAR(comment_toolbar), GTK_ICON_SIZE_DIALOG);
        gtk_stack_add_named(GTK_STACK(priv->stack), comment_toolbar, "comment-toolbar");
        gtk_widget_show_all(comment_toolbar);
    } else{
        GtkWidget *comment_toolbar = gtk_stack_get_child_by_name(GTK_STACK(priv->stack), "comment-toolbar");
        eog_comment_toolbar_init_area(EOG_COMMENT_TOOLBAR(comment_toolbar));
    }
    eog_utils_set_sensitive_widgets (priv->window, FALSE);
    gtk_stack_set_visible_child_full(GTK_STACK(priv->stack), "comment-toolbar", GTK_STACK_TRANSITION_TYPE_OVER_LEFT);

    const gchar *info = _("Please enter your comment then validate with the menu button or the enter key.");
    gchar *markup = g_markup_printf_escaped(priv->format, info);
    gtk_label_set_markup(GTK_LABEL (priv->label_info), markup);
    g_free (markup);

    gtk_widget_show_all(priv->box_info);
    gtk_widget_hide (priv->eog_nav);
    return;
}

static GtkToolItem *
item_button_comment (EogMainToolbar *toolbar)
{
    g_return_val_if_fail(EOG_IS_MAIN_TOOLBAR(toolbar), NULL);

    GtkWidget *img_comment = gtk_image_new_from_icon_name("format-text-direction-ltr", GTK_ICON_SIZE_BUTTON);
    GtkToolItem *comment = gtk_tool_button_new(img_comment, _("Comment"));

    g_signal_connect(GTK_TOOL_BUTTON(comment), "clicked", G_CALLBACK(button_comment_cb), toolbar);

    return comment;
}

/* * * * * *  * * * * * * * * * * *
 *
 * BUTTON EMAIL
 * Code took from eog-plugin repository
 *
 * * * * * *  * * * * * * * *  * * */
static void
button_email_cb (GtkToolButton *toolbutton,
                 gpointer       data)
{
    g_return_if_fail(EOG_IS_MAIN_TOOLBAR(data));

    EogMainToolbarPrivate *priv = EOG_MAIN_TOOLBAR(data)->priv;
    EogWindow *window = priv->window;
    g_return_if_fail(EOG_IS_WINDOW(window));

    EogImage *curr_image = eog_window_get_image(window);
    if (curr_image == NULL)
        return;

    GList *images = NULL, *image = NULL;
    GString *uri = NULL;
    gboolean first = TRUE;

    images = eog_thumb_view_get_selected_images (EOG_THUMB_VIEW (priv->t_view));

    gboolean is_thunderbird = g_find_program_in_path ("mailissimo") != NULL ? TRUE : FALSE;
    uri = is_thunderbird ? g_string_new ("mailissimo -compose \"attachment=") : g_string_new ("mailto:?attach=");

    for (image = images; image != NULL; image = image->next) {
        EogImage *img = EOG_IMAGE (image->data);
        GFile *file;
        gchar *path;

        file = eog_image_get_file (img);
        if (!file) {
            g_object_unref (img);
            continue;
        }

        path = g_file_get_path (file);
        if (first) {
            if (is_thunderbird)
                g_string_append_printf(uri, "'%s", path);
            else
                uri = g_string_append (uri, path);
            first = FALSE;
        } else {
            if (is_thunderbird)
                g_string_append_printf (uri, ",%s", path);
            else
               g_string_append_printf (uri, "&attach=%s", path);
        }
        g_free (path);
        g_object_unref (file);
        g_object_unref (img);
    }
    g_list_free (images);

    if (is_thunderbird)
        uri = g_string_append(uri, "'\"");
    /* changed gtk_show_uri to  gtk_show_uri_on_window */
    GError *error = NULL;

    if(is_thunderbird) {
        g_spawn_command_line_async(uri->str, &error);
    } else
        gtk_show_uri_on_window(GTK_WINDOW(window), uri->str, GDK_CURRENT_TIME, &error);
    if(error) {
        g_warning ("Error (%s) : %s\n", __func__, error->message);
        g_clear_error(&error);
    }
    g_string_free (uri, TRUE);
}

static GtkToolItem *
item_button_email (EogMainToolbar *toolbar)
{
    g_return_val_if_fail(EOG_IS_MAIN_TOOLBAR(toolbar), NULL);

    GtkWidget *img_email = gtk_image_new_from_icon_name("mail-send", GTK_ICON_SIZE_BUTTON);
    GtkToolItem *send_email = gtk_tool_button_new(img_email, _("Send email"));

    g_signal_connect(GTK_TOOL_BUTTON(send_email), "clicked", G_CALLBACK(button_email_cb), toolbar);

    return send_email;
}

/* * * * * *  * * * * * * * * * * *
 *
 * BUTTON PRINT
 *
 * * * * * *  * * * * * * * *  * * */
static void
button_print_cb (GtkToolButton *toolbutton,
                 gpointer       data)
{
    g_return_if_fail(EOG_IS_MAIN_TOOLBAR(data));

    EogMainToolbarPrivate *priv = EOG_MAIN_TOOLBAR(data)->priv;

    /*
     * This is here where we check the environment (Or before)
     */

    GAction *act = g_action_map_lookup_action (G_ACTION_MAP(priv->window), "print");

    if (priv->oinfo_exist) {
        GString *args = g_string_new ("impressimo -G ");
        GtkTreeModel *model = GTK_TREE_MODEL (eog_window_get_store (priv->window));
        EogThumbNavMode mode = eog_thumb_nav_get_mode(EOG_THUMB_NAV(priv->eog_nav));
        GError *error = NULL;
        EogImage *image;
        GFile *file;
        gchar *path;
        if (mode == EOG_THUMB_NAV_MODE_MULTIPLE_ROWS) {
            GtkTreeIter iter;
            gboolean valid = gtk_tree_model_get_iter_first(model, &iter);
            if (!valid)
                return;
            do {
                gtk_tree_model_get (model, &iter, EOG_LIST_STORE_EOG_IMAGE, &image, -1);
                file = eog_image_get_file(image);
                path = g_file_get_path(file);
                g_string_append_printf(args, "'%s' ", path);
            } while (gtk_tree_model_iter_next(model, &iter));
        } else if (mode == EOG_THUMB_NAV_MODE_ONE_ROW) {
            GtkWidget *eog_view = eog_window_get_thumb_view(priv->window);
            image = eog_thumb_view_get_first_selected_image(EOG_THUMB_VIEW (eog_view));
            file = eog_image_get_file(image);
            path = g_file_get_path (file);
            g_string_append_printf(args, "'%s'", path);
        }
        if (path)
            g_free (path);
        if(file)
            g_object_unref (file);
        if (image)
            g_object_unref (G_OBJECT (image));

        gboolean spawned = g_spawn_command_line_async(args->str, &error);
        if (!spawned || error) {
            g_critical("Could not spawn impressimo : %s", error ? error->message : "Unknown error");
            g_action_activate (act, NULL);
        }
        g_string_free (args, TRUE);
    }else
        g_action_activate (act, NULL);
    return;
}

static GtkToolItem *
item_button_print (EogMainToolbar *toolbar)
{
    g_return_val_if_fail(EOG_IS_MAIN_TOOLBAR(toolbar), NULL);

    GtkWidget *img_print = gtk_image_new_from_icon_name("printer", GTK_ICON_SIZE_BUTTON);
    GtkToolItem *print = gtk_tool_button_new(img_print, _("Print"));


    g_signal_connect(GTK_TOOL_BUTTON(print), "clicked", G_CALLBACK(button_print_cb), toolbar);

    return print;
}

/* * * * * *  * * * * * * * * * * *
 *
 * BUTTON REDUCE
 *
 * * * * * *  * * * * * * * *  * * */
static void
button_reduce_cb (GtkToolButton *toolbutton,
                  gpointer       data)
{
    g_return_if_fail(EOG_IS_MAIN_TOOLBAR(data));

    EogMainToolbarPrivate *priv = EOG_MAIN_TOOLBAR(data)->priv;

    if(!gtk_stack_get_child_by_name(GTK_STACK(priv->stack), "reduce-toolbar")) {
        GtkWidget *reduce_toolbar = eog_reduce_toolbar_new(priv->window, priv->stack, priv->area_progress);
        if (priv->oinfo_exist)
            gtk_toolbar_set_icon_size(GTK_TOOLBAR(reduce_toolbar), GTK_ICON_SIZE_DIALOG);
        gtk_stack_add_named(GTK_STACK(priv->stack), reduce_toolbar, "reduce-toolbar");
        gtk_widget_show_all(reduce_toolbar);
    }
    eog_utils_set_sensitive_widgets (priv->window, FALSE);
    gtk_stack_set_visible_child_full(GTK_STACK(priv->stack), "reduce-toolbar", GTK_STACK_TRANSITION_TYPE_OVER_LEFT);

    const gchar *info = _("Please choose the desired image size from the menu.");
    gchar *markup = g_markup_printf_escaped(priv->format, info);
    gtk_label_set_markup(GTK_LABEL (priv->label_info), markup);
    g_free (markup);

    gtk_widget_show_all(priv->box_info);
    gtk_widget_hide (priv->eog_nav);
    return;
}


static GtkToolItem *
item_button_reduce (EogMainToolbar *toolbar)
{
    g_return_val_if_fail(EOG_IS_MAIN_TOOLBAR(toolbar), NULL);

    GtkWidget *img_reduce = gtk_image_new_from_icon_name("edit-cut", GTK_ICON_SIZE_BUTTON);
    GtkToolItem *reduce = gtk_tool_button_new(img_reduce, _("Reduce"));

    g_signal_connect(GTK_TOOL_BUTTON(reduce), "clicked", G_CALLBACK(button_reduce_cb), toolbar);

    return reduce;
}

/* * * * * *  * * * * * * * * * * *
 *
 * BUTTON BRIGHTNESS
 *
 * * * * * *  * * * * * * * *  * * */
static void
button_brightness_cb (GtkToolButton *toolbutton,
                      gpointer       data)
{
    g_return_if_fail(EOG_IS_MAIN_TOOLBAR(data));

    EogMainToolbarPrivate *priv = EOG_MAIN_TOOLBAR(data)->priv;

    const gchar *info = _("Please decrease or increase the brightness of the image using the menu buttons.");
    gchar *markup = g_markup_printf_escaped(priv->format, info);
    gtk_label_set_markup(GTK_LABEL (priv->label_info), markup);
    g_free (markup);

    gtk_widget_show_all(priv->box_info);
    gtk_widget_hide (priv->eog_nav);

    if(!gtk_stack_get_child_by_name(GTK_STACK(priv->stack), "brightness-toolbar")) {
        GtkWidget *brightness_toolbar = eog_brightness_toolbar_new(priv->window, priv->stack, priv->area_progress);
        if (priv->oinfo_exist)
            gtk_toolbar_set_icon_size(GTK_TOOLBAR(brightness_toolbar), GTK_ICON_SIZE_DIALOG);
        gtk_stack_add_named(GTK_STACK(priv->stack), brightness_toolbar, "brightness-toolbar");
        gtk_widget_show_all(brightness_toolbar);
    } else {
        GtkWidget *brightness_toolbar = gtk_stack_get_child_by_name(GTK_STACK(priv->stack), "brightness-toolbar");
        eog_brightness_set_current_image(EOG_BRIGHTNESS_TOOLBAR(brightness_toolbar), eog_window_get_image(priv->window));
    }
    eog_utils_set_sensitive_widgets (priv->window, FALSE);
    gtk_stack_set_visible_child_full(GTK_STACK(priv->stack), "brightness-toolbar", GTK_STACK_TRANSITION_TYPE_OVER_LEFT);

    return;
}

static GtkToolItem *
item_button_brightness (EogMainToolbar *toolbar)
{
    g_return_val_if_fail(EOG_IS_MAIN_TOOLBAR(toolbar), NULL);

    GtkWidget *img_brightness = gtk_image_new_from_icon_name("emblem-system", GTK_ICON_SIZE_BUTTON);
    GtkToolItem *brightness = gtk_tool_button_new(img_brightness, _("Brightness"));

    g_signal_connect(GTK_TOOL_BUTTON(brightness), "clicked", G_CALLBACK(button_brightness_cb), toolbar);

    return brightness;
}

/* * * * * *  * * * * * * * * * * *
 *
 * BUTTON CONTRAST
 *
 * * * * * *  * * * * * * * *  * * */
static void job_filter_contrast_finished_cb (EogJobContrastFilter *job, gpointer data)
{
    GFile *file = eog_image_get_file (job->image);
    const gchar *path_file = g_file_get_parse_name(file);
    eog_utils_save_image (EOG_MAIN_TOOLBAR(data)->priv->window, job->pixbuf_output, g_file_get_parent(file), path_file, "Contrast", TRUE);

    gtk_widget_hide (EOG_MAIN_TOOLBAR(data)->priv->area_progress);
    gtk_widget_set_sensitive(GTK_WIDGET(EOG_MAIN_TOOLBAR(data)->priv->stack), TRUE);
    eog_utils_set_sensitive_widgets (EOG_MAIN_TOOLBAR(data)->priv->window, TRUE);

    return;
}

static void
button_contrast_cb (GtkToolButton *toolbutton,
                    gpointer       data)
{
    g_return_if_fail(EOG_IS_MAIN_TOOLBAR(data));

    EogMainToolbarPrivate *priv = EOG_MAIN_TOOLBAR(data)->priv;
    EogImage *image = eog_window_get_image(priv->window);
    if (image == NULL)
        return;

    gtk_widget_show_all (priv->area_progress);
    if (gtk_widget_get_visible (priv->area_progress)) {
        gtk_widget_set_sensitive(GTK_WIDGET(priv->stack), FALSE);
        eog_utils_set_sensitive_widgets (priv->window, FALSE);
    }

    EogJob *job = eog_job_contrast_filter_new(image);
    g_signal_connect (job, "finished", G_CALLBACK (job_filter_contrast_finished_cb), data);
    eog_job_scheduler_add_job(job);
    g_object_unref (job);
}

static GtkToolItem *
item_button_contrast (EogMainToolbar *toolbar)
{
    g_return_val_if_fail(EOG_IS_MAIN_TOOLBAR(toolbar), NULL);

    GtkWidget *img_contrast = gtk_image_new_from_icon_name("weather-clear", GTK_ICON_SIZE_BUTTON);
    GtkToolItem *contrast = gtk_tool_button_new(img_contrast, _("Contrast"));

    g_signal_connect(GTK_TOOL_BUTTON(contrast), "clicked", G_CALLBACK(button_contrast_cb), toolbar);

    return contrast;
}

/* * * * * *  * * * * * * * * * * *
 *
 * BUTTON RED EYES
 *
 * * * * * *  * * * * * * * *  * * */
static void
button_red_eyes_cb (GtkToolButton *toolbutton,
                    gpointer       data)
{
    g_return_if_fail(EOG_IS_MAIN_TOOLBAR(data));

    EogMainToolbarPrivate *priv = EOG_MAIN_TOOLBAR(data)->priv;

    const gchar *info = _("Select the area around the eye's pupil.");
    gchar *markup = g_markup_printf_escaped(priv->format, info);
    gtk_label_set_markup(GTK_LABEL (priv->label_info), markup);
    g_free (markup);

    gtk_widget_show_all(priv->box_info);
    gtk_widget_hide (priv->eog_nav);

    if(!gtk_stack_get_child_by_name(GTK_STACK(priv->stack), "red-eyes-toolbar")) {
        GtkWidget *red_eyes_toolbar = eog_red_eyes_toolbar_new(priv->window, priv->stack, priv->area_progress);
        if (priv->oinfo_exist)
            gtk_toolbar_set_icon_size(GTK_TOOLBAR(red_eyes_toolbar), GTK_ICON_SIZE_DIALOG);
        gtk_stack_add_named(GTK_STACK(priv->stack), red_eyes_toolbar, "red-eyes-toolbar");
        gtk_widget_show_all(red_eyes_toolbar);
    } else{
        GtkWidget *red_eyes_toolbar = gtk_stack_get_child_by_name(GTK_STACK(priv->stack), "red-eyes-toolbar");
        eog_red_eyes_toolbar_init_area(red_eyes_toolbar);
    }
    eog_utils_set_sensitive_widgets (priv->window, FALSE);
    gtk_stack_set_visible_child_full(GTK_STACK(priv->stack), "red-eyes-toolbar", GTK_STACK_TRANSITION_TYPE_OVER_LEFT);
    return;
}

static GtkToolItem *
item_button_red_eyes (EogMainToolbar *toolbar)
{
    g_return_val_if_fail(EOG_IS_MAIN_TOOLBAR(toolbar), NULL);

    GtkWidget *img_red_eyes = gtk_image_new_from_icon_name("process-stop", GTK_ICON_SIZE_BUTTON);
    GtkToolItem *red_eyes = gtk_tool_button_new(img_red_eyes, _("Red eyes"));

    g_signal_connect(GTK_TOOL_BUTTON(red_eyes), "clicked", G_CALLBACK(button_red_eyes_cb), toolbar);

    return red_eyes;
}

/* * * * * *  * * * * * * * * * * *
 *
 * BUTTON GRAYSCALE
 *
 * * * * * *  * * * * * * * *  * * */
static void job_filter_grayscale_finished_cb (EogJobGrayscaleFilter *job, gpointer data)
{
    GFile *file = eog_image_get_file (job->image);
    const gchar *path_file = g_file_get_parse_name(file);
    eog_utils_save_image (EOG_MAIN_TOOLBAR(data)->priv->window, job->pixbuf_output, g_file_get_parent(file), path_file, "Grayscale", TRUE);

    gtk_widget_hide (EOG_MAIN_TOOLBAR(data)->priv->area_progress);
    gtk_widget_set_sensitive(GTK_WIDGET(EOG_MAIN_TOOLBAR(data)->priv->stack), TRUE);
    eog_utils_set_sensitive_widgets (EOG_MAIN_TOOLBAR(data)->priv->window, TRUE);

    return;
}

static void
button_grayscale_cb (GtkToolButton *toolbutton,
                     gpointer       data)
{
    g_return_if_fail(EOG_IS_MAIN_TOOLBAR(data));

    EogMainToolbarPrivate *priv = EOG_MAIN_TOOLBAR(data)->priv;
    EogImage *image = eog_window_get_image(priv->window);
    if (image == NULL)
        return;

    gtk_widget_show_all (priv->area_progress);
    if (gtk_widget_get_visible (priv->area_progress)) {
        gtk_widget_set_sensitive(GTK_WIDGET(priv->stack), FALSE);
        eog_utils_set_sensitive_widgets (priv->window, FALSE);
    }

    EogJob *job = eog_job_grayscale_filter_new(image);
    g_signal_connect (job, "finished", G_CALLBACK (job_filter_grayscale_finished_cb), data);
    eog_job_scheduler_add_job(job);
    g_object_unref (job);
}

static GtkToolItem *
item_button_grayscale (EogMainToolbar *toolbar)
{
    g_return_val_if_fail(EOG_IS_MAIN_TOOLBAR(toolbar), NULL);

    GtkWidget *img_grayscale = gtk_image_new_from_icon_name("weather-clear-night", GTK_ICON_SIZE_BUTTON);
    GtkToolItem *grayscale = gtk_tool_button_new(img_grayscale, _("Grayscale"));

    g_signal_connect(GTK_TOOL_BUTTON(grayscale), "clicked", G_CALLBACK(button_grayscale_cb), toolbar);

    return grayscale;
}

/* * * * * *  * * * * * * * * * * *
 *
 * BUTTON SEPIA
 *
 * * * * * *  * * * * * * * *  * * */
static void job_filter_sepia_finished_cb (EogJobSepiaFilter *job, gpointer data)
{
    GFile *file = eog_image_get_file (job->image);
    const gchar *path_file = g_file_get_parse_name(file);
    eog_utils_save_image (EOG_MAIN_TOOLBAR(data)->priv->window, job->pixbuf_output, g_file_get_parent(file), path_file, "Sepia", TRUE);

    gtk_widget_hide (EOG_MAIN_TOOLBAR(data)->priv->area_progress);
    gtk_widget_set_sensitive(GTK_WIDGET(EOG_MAIN_TOOLBAR(data)->priv->stack), TRUE);
    eog_utils_set_sensitive_widgets (EOG_MAIN_TOOLBAR(data)->priv->window, TRUE);

    return;
}

static void
button_sepia_cb (GtkToolButton *toolbutton,
                     gpointer       data)
{
    g_return_if_fail(EOG_IS_MAIN_TOOLBAR(data));

    EogMainToolbarPrivate *priv = EOG_MAIN_TOOLBAR(data)->priv;

    EogImage *image = eog_window_get_image(priv->window);
    if (!image)
        return;

    gtk_widget_show_all (priv->area_progress);

    if (gtk_widget_get_visible(priv->area_progress)) {
        gtk_widget_set_sensitive(GTK_WIDGET(priv->stack), FALSE);
        eog_utils_set_sensitive_widgets(priv->window, FALSE);
    }
    EogJob *job = eog_job_sepia_filter_new(image);
    g_signal_connect (job, "finished", G_CALLBACK (job_filter_sepia_finished_cb), data);
    eog_job_scheduler_add_job(job);
    g_object_unref (job);

    return;
}

static GtkToolItem *
item_button_sepia (EogMainToolbar *toolbar)
{
    g_return_val_if_fail(EOG_IS_MAIN_TOOLBAR(toolbar), NULL);

    GtkWidget *img_sepia = gtk_image_new_from_icon_name("applications-graphics", GTK_ICON_SIZE_BUTTON);
    GtkToolItem *sepia = gtk_tool_button_new(img_sepia, _("Sepia"));

    g_signal_connect(GTK_TOOL_BUTTON(sepia), "clicked", G_CALLBACK(button_sepia_cb), toolbar);

    return sepia;
}

static void
eog_main_toolbar_init (EogMainToolbar *m_toolbars)
{
    m_toolbars->priv = eog_main_toolbar_get_instance_private(m_toolbars);
    EogMainToolbarPrivate *priv = m_toolbars->priv;
    priv->slideshow = FALSE;
    priv->gallery = FALSE;
//    gtk_stack_set_transition_type(GTK_STACK(stack_toolbars), GTK_STACK_TRANSITION_TYPE_SLIDE_UP_DOWN);

/*
        if (eog_list_store_length(eog_window_get_store(window)) == 1) {
            GtkToolItem *item = gtk_toolbar_get_nth_item(GTK_TOOLBAR())
            gtk_widget_set_visible(GTK_WIDGET())
        }
*/
    priv->filters_is_visible = TRUE;

    priv->item_gallery = item_button_gallery (m_toolbars);
    priv->item_rename = item_button_rename (m_toolbars);
    priv->item_crop = item_button_crop (m_toolbars);
    priv->item_rotate = item_button_rotate (m_toolbars);
    priv->item_comment = item_button_comment (m_toolbars);

    priv->pos_start_filters = 8;
    priv->pos_end_filters = 13;

    gtk_toolbar_set_style(GTK_TOOLBAR(m_toolbars), GTK_TOOLBAR_BOTH);
    gtk_toolbar_insert(GTK_TOOLBAR(m_toolbars), priv->item_gallery, 0);
    gtk_toolbar_insert(GTK_TOOLBAR(m_toolbars), item_button_slideshow (m_toolbars), 1);
    gtk_toolbar_insert(GTK_TOOLBAR(m_toolbars), priv->item_rename, 2);
    gtk_toolbar_insert(GTK_TOOLBAR(m_toolbars), priv->item_crop, 3);
    gtk_toolbar_insert(GTK_TOOLBAR(m_toolbars), priv->item_rotate, 4);
    gtk_toolbar_insert(GTK_TOOLBAR(m_toolbars), priv->item_comment, 5);
    gtk_toolbar_insert(GTK_TOOLBAR(m_toolbars), item_button_email (m_toolbars), 6);
    gtk_toolbar_insert(GTK_TOOLBAR(m_toolbars), item_button_print (m_toolbars), 7);
//    gtk_toolbar_insert(GTK_TOOLBAR(priv->default_toolbar), item_button_remove (stack_toolbars), 1);
    gtk_toolbar_insert(GTK_TOOLBAR(m_toolbars), item_button_reduce (m_toolbars), priv->pos_start_filters);//7);
    gtk_toolbar_insert(GTK_TOOLBAR(m_toolbars), item_button_brightness (m_toolbars), 9);
    gtk_toolbar_insert(GTK_TOOLBAR(m_toolbars), item_button_contrast (m_toolbars), 10);
    gtk_toolbar_insert(GTK_TOOLBAR(m_toolbars), item_button_red_eyes (m_toolbars), 11);
    gtk_toolbar_insert(GTK_TOOLBAR(m_toolbars), item_button_grayscale (m_toolbars), 12);
    gtk_toolbar_insert(GTK_TOOLBAR(m_toolbars), item_button_sepia (m_toolbars), priv->pos_end_filters);//12);

    priv->gallery_settings = g_settings_new(EOG_CONF_UI);
    priv->oinfo_exist = g_find_program_in_path ("oinfo") != NULL ? TRUE : FALSE;

    if (priv->oinfo_exist)
        gtk_toolbar_set_icon_size(GTK_TOOLBAR(m_toolbars), GTK_ICON_SIZE_DIALOG);
}

void
eog_main_toolbar_hide_gallery (EogMainToolbar *toolbar, gboolean visible)
{
    g_return_if_fail(EOG_IS_MAIN_TOOLBAR(toolbar));

    gboolean is_visible = gtk_widget_get_visible(GTK_WIDGET(toolbar->priv->item_gallery));
    if ( (!is_visible) == visible)
        gtk_widget_set_visible(GTK_WIDGET(toolbar->priv->item_gallery), visible);

}

void
eog_main_toolbar_hide_filters (EogMainToolbar *toolbar, gboolean hide)
{
    g_return_if_fail(EOG_IS_MAIN_TOOLBAR(toolbar));

    EogMainToolbarPrivate *priv = toolbar->priv;

    gtk_widget_set_visible(GTK_WIDGET(priv->item_rename), hide);
    gtk_widget_set_visible(GTK_WIDGET(priv->item_crop), hide);
    gtk_widget_set_visible(GTK_WIDGET(priv->item_rotate), hide);
    gtk_widget_set_visible(GTK_WIDGET(priv->item_comment), hide);

    guint i;
    for (i = priv->pos_start_filters; i <= priv->pos_end_filters; i++) {
        GtkToolItem *item = gtk_toolbar_get_nth_item (GTK_TOOLBAR(toolbar), i);
        gtk_widget_set_visible(GTK_WIDGET(item), hide);
    }

    priv->filters_is_visible = !hide;
}

gboolean
eog_main_toolbar_filters_is_hidden (EogMainToolbar *toolbar)
{
    g_return_val_if_fail(EOG_IS_MAIN_TOOLBAR(toolbar), FALSE);
    return toolbar->priv->filters_is_visible;
}