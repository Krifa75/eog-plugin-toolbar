Eog plugin toolbar<br>
<br>
Description :<br>
This plugin display in EOG a toolbar that contains several buttons to interact with the displayed image (such as filters, renaming, etc ...) <br>
                 <br>

```
Note :
To make this plugin work, you shloud download our fork of Eog (until Eog accept our modifications). 
```

```
Requirements :
    For the execution:
         All requirements from EOG.
        
    For the build:
         meson
         libexif-dev
         libpeas-dev
         eog-dev
```


Install :<br>
To install this plugin :<br>
<br>
```
    meson build 
    cd build
    sudo ninja install
```


License :<br>
This program is released under the terms of the GNU General Public License. <br>
Please see the file LICENSE for details.<br>
<br>